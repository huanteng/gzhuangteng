<?php
require dirname( __FILE__ ) . '/library/controller.php';

class frontend extends controller
{
	// 模板对象
	var $template = null;

	/** 定义模板缓存时间
	 * @return array
	 * 	如： 'list' => 300，表示该方法的访问将会有300秒的缓存；（不定义表示不缓存）
	 */
	function cache_rule()
	{
		return array();
	}

	// 防注入
	function clean( $in )
	{
		return load('http')->clean( $in );
	}

	function xss_clean( $in )
	{
		return load('ci_security')->xss_clean( $in );
	}

	function _method( $in )
	{
        $url = config('url');

		$out = '未定义方法，method=' . $url[ 'method' ];
		return $out;
	}

	/** 传递变量给模板
	 * @param $key
	 * @param $value
	 */
	function assign( $key, $value )
	{
		if( !$this->template )
		{
			$this->template = load('template');
		}
		$this->template->assign( $key, $value );
	}

	/** 转换url中的字符串为对应的数字值，如 module/method/expert => module/method/1
	 * @param $in：url转化后的数组
	 * @param $name：要转换的变量名
	 * @param $config：转换配置
	 * @param null $default：如果不存在，使用的默认值，默认为不设置
	 * @return mixed：修改后的值
	 */
	function convert_url( &$in, $name, $config, $default = null )
	{
		if( isset( $in[ $name ] ) && isset( $config[ $in[ $name ] ] ) )
		{
			$in[ $name ] = $config[ $in[ $name ] ];
		}
		else
		{
			if( !isset( $in[ $name ] ) && $default !== null )
			{
				$in[ $name ] = $default;
			}
		}

		return $in;
	}

	/** 将url中的sport值由文字改为数字。原值以sport_name赋给模板。因较常用，特抽取。详见例子
	 */
	function convert_url_from_sport( $url )
	{
		$get = &$url[ 'get' ];

        if(empty($get[ 'sport' ]))  $get[ 'sport' ]=1;
//		$this->assign( 'sport_name', $get[ 'sport' ] );
		$get[ 'sport_name' ] = $get[ 'sport' ];
		$get[ 'sport' ] = $get[ 'sport' ] == 'basketball' ? '2' : '1';
		$get[ 'r_uri' ]=$_SERVER["REQUEST_URI"];

//        $this->assign( 'sport', $get[ 'sport' ] );

		return $url;
	}

	// 通用列表
	function hoxme( $in )
	{
		return $this->render( $in );
	}
	

	/** 检查是否登录，如无则ajax返回提醒。如有，返回uid。（逻辑上不太合理，但可节省代码。）
	 * 参数：
	 * 	ajax：true（默认）使用ajax式返回，false，页面式跳转返回
	 * @return string
	 */
	function check_login( $ajax = true )
	{
		//发布推介
		$uid = $this->login_uid();

		if( $uid == 0 )
		{
			if( $ajax )
			{
				return $this->ajax_out( -100, '您还没有登录' );
			}
			else
			{
				echo $this->render( ["title"=>config( 'site.name' ), "content"=>"请先登录"], "prompt" );
				die;
			}
		}

		return $uid;
	}

	// 定义路由规则，按需重载。这会组成url_route中的一部分
	function route_rule()
	{
		return [];
	}

	/** 处理url参数，重载一般要继承
	 * @param $url
	 * @return mixed
	 */
	function url_route( $url )
	{
		$route = [];
		$route = array_merge( $route, $this->route_rule() );

		$uri = &$url[ 'uri' ];

		foreach ($route as $k => $v)
		{
			$pattern = '/' . $k . '/';
			if( preg_match( $pattern, $uri, $matches, PREG_OFFSET_CAPTURE ) === 1 )
			{
				if( is_array( $v ) )
				{
					$uri = preg_replace( $pattern, $v[ 0 ], $uri );
				}
				else
				{
					$uri = preg_replace( $pattern, $v, $uri );
					break;
				}
			}
		}

        $pos = strpos($uri, '?');
        if($pos === false) {
            $pos = strpos($uri, '/');

            if($pos === false) {
                $pos = strlen($uri);
            }
        }

        $method = substr($uri, 0, $pos);
        $uri = substr($uri, $pos + 1);

        $get = [];
        if ($uri != '') {
            $uri = str_replace(['&', '='], ['/', '/'], $uri);
            $uri = explode('/', $uri);
        } else {
            $uri = [];
        }

        for( $i = 0, $length = count( $uri ); $i < $length; $i += 2 )
        {
            $name = $uri[ $i ];
            // 参数名为空，这是错误，忽略本组参数
            if( $name == '') continue;

            $value = value( $uri, $i + 1, '' );

            // 变量名重复问题，以数组形式保存
            if( isset( $get[ $name ] ) )
            {
                if( is_array( $get[ $name ] ) )
                {
                    $get[ $name ][] = $value;
                }
                else
                {
                    $get[ $name ] = [ $get[ $name ], $value ];
                }
            }
            else
            {
                $get[ $name ] = $value;
            }
        }

		if(isset($_POST['method'])) {
			$method = $_POST['method'];
		}elseif (isset($get['method'])) {
            $method = $get['method'];
            unset($get['method']);
        }

        if( $method == '' )
        {
            $method = 'home';
        }

		// 方法不存在
		if( !method_exists( $this, $method ) )
		{
			// 使用默认方法
			if( method_exists( $this, 'home' ) )
			{
				$method = 'home';
			}
			// 方法不存在，并将原method值作为参数压入栈
			else
			{
				$method = 'not_found_method';
			}
			array_unshift($uri, '');
		}
		$url[ 'method' ] = $method;
		$url[ 'get' ] = $get;

		unset( $url[ 'uri' ] );

		return $url;
	}

	/**检查sign是否正确，如无则结束。（逻辑上不太合理，但可节省代码。）
	 * @param $in：
	 */
	function check_sign( $in )
	{
		$site = biz( 'site' );

		$check = $site->check_sign( $in );

		if( $check[ 'code' ] < 0 )
		{
			$site->log( 0, 0, $check[ 'memo' ], array( 'data' => $in ) );
			echo $check[ 'memo' ];
			die;
		}
	}
	


	function jump( $text='请不要乱打链接访问哦', $url='/' )
	{
		return $this->render(array("title"=>config( 'site.name' ),"content"=>$text,'url'=>$url),"prompt");
		exit();
	}



	function render( $data = array(), $tpl = '')
	{
		$dir = config('dir');
		$path = $dir['project'] . 'template/';

		if( $dir[ 'base' ] != '/' )
		{
			$path .= $dir[ 'base' ];
		}

		if ($tpl == '') {
			$url = config('url');
			$tpl = $url[ 'module' ];
			$method = $url[ 'method' ];
			if ($method != 'home')
			{
				$tpl .= '_' . $method;
			}
		}

		return load('twig')->render( $path, $tpl . '.php', $data );
	}

	function login_uid()
	{

		$account = load('cookie')->get('account', true);
		return value($account, 'uid', 0);
	}

	//检查 $key是否存在并且是数字，如存在则返回，否则返回$default
	function number($data, $key, $default = 0)
	{

		return isset($data[$key]) && is_numeric($data[$key]) ? $data[$key] : $default;
	}

	function pagebar($data, $style = 1)
	{

		$template = load('template', array('style' => $style));
		$template->appoint($data);
		return $template->parse('pagebar.php');
	}

	function substr($text, $start = 0, $length = 4)
	{

		if (strlen($text) <= $length * 2) {
			return $text;
		} else {
			if (preg_match('/^[a-zA-Z0-9_]+$/', $text)) {
				$length *= 2;
			}
			return mb_substr($text, $start, $length, 'utf-8');
		}
	}

	function title( $text )
	{
		if( $text != '' ){
			$arr = array(
				'index'=>'',
				'search'=>'搜索',
				'magic'=>'优惠套餐',
				'news_index'=>'体育新闻',
				'news_more'=>'新闻',
				'news'=>'',
				'about'=>'关于网站',
				'links'=>'友情链接',
				'contact'=>'联系我们',
				'help'=>'帮助中心',
				'map'=>'网站地图',
				'duty'=>'本站声明',
				);
			return isset( $arr[$text] ) && $arr[$text] != '' ? $arr[$text] . '' : '';
		} else {
			return '欢腾网络';
		}
	}

	function dplus_encode( $data )
	{
		$out = array();
		foreach( $data as $k => $v )
		{
			$out[] = "'" . $k . "':'" . $v . "'";
		}

		return '{' . implode( ',', $out ) . '}';
	}

    function checkParameter($in, $config)
    {
        return biz('base')->checkParameter($in, $config);
    }
}
