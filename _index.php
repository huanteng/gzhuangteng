<?
define('MVC', true);
require dirname(__FILE__) . '/frontend.php';

// 统计页面运行时间,在必要时启用
$showRuntime = false;

//if(load('cookie')->get('cache')=='new') {
//    $showRuntime = true;
//    $starttime = load('time')->microtime_float();
//}

controller::initCli();

$url = urldecode($_SERVER["REQUEST_URI"]);
//$url = controller::removePhpFromUrl($url);
//if ( $url == '/index' )
//{
//	go_301( '' );
//}

//controller::checkDenyIp();

$url = controller::getModuleFromUrl($url);
$action = controller::getAction($url);
$url = $action->url_route($url);
config('url', $url);

$action->run( $_POST + $url['get'] );

if($showRuntime) {
    $runtime = number_format((load('time')->microtime_float()-$starttime), 4).'s';
    echo '本页面运行时间:' . $runtime;
}