<?php
// 处理box缓存和页面缓存

require_once 'redisDB.php';

class box extends redisDB
{
    public function __construct($param = NULL) {
        $this->is_refresh = load('cookie')->get( 'cache' ) == 'new';
    }

    function name()
    {
        $out = [];
        foreach( func_get_args() as $v )
        {
            $out[] = is_array( $v ) ? md5( json_encode( $v ) ) : str_replace('.', '/', $v);
        }

        return config( 'dir.cache' ) . implode( '/', $out ) . '.php';
    }

    function del( $key )
    {
        $file = $this->name( $key );
        return load('file')->del( $file );
    }

    function get( $key, $default = false )
    {
        if( $this->is_refresh )
        {
            $this->del( $key );
            return $default;
        }

        $file = $this->name( $key );

        if( !is_file( $file ) )
        {
            return $default;
        }

        $handle=fopen($file,"r");
        if ($handle) {
            flock($handle,LOCK_EX);
            $data = require $file;
            flock($handle,LOCK_UN);
            fclose($handle);
        }

        if ($data[0] < time()) {
//            20170810 暂不用删除
//            unlink( $file );
            return $default;
        }

        return $data[1];
    }

    function set( $key, $value, $expire = 900 )
    {
        $file = $this->name( $key );

        $info = pathinfo( $file );
        if( !is_dir( $info[ 'dirname' ] ) )
        {
            mkdir( $info[ 'dirname' ], 0755, true );
        }

        $content = '<?php return [' . ( time()+$expire ) . ',' . var_export( $value, true ) . '];';
        return file_put_contents( $file, $content ,LOCK_EX);
    }
}
