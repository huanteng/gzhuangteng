<?php
/*
 * 结合以下文章:
 * http://www.tuicool.com/articles/iUVvq2r
 * http://www.cnblogs.com/it-cen/p/4984272.html
 */
/**
 *在redis上操作类
 */
class redisDB {
    private $redis;

    // 是否刷新缓存
    var $is_refresh = false;

	public function __construct($param = NULL) {
        $this->redis = new Redis();
        $this->is_refresh = load('cookie')->get( 'cache' ) == 'new';
        $config = load_config( 'redis' );
        
        if($config == '') {
            return false;
        }
        
        $result = $this->redis->connect( $config['server'], $config['port']);
        if (!$result) {
            biz('log')->error('redis无法连接');
            return;
        }
        if( isset($config['auth']) ) {
            $this->redis->auth($config['auth']);
        }
        return $this->redis;
	}

    /** 当调用不存在的方法时,将key转码,并直接给内部函数实现
     * @param $method
     * @param $args
     *
     * @return mixed
     */
    // lPop, rPush
    public function __call( $method, $args )
    {
        $args[0] = $this->name($args[0]);
        return call_user_func_array([ $this->redis, $method ], $args);
    }

    /** 传入多参数，每个参数为字符串或数组，生成名字，以便于程序简化和统一
     * @return mixed：名字
     */
    function name()
    {
        $out = [ load_config('redis')['pre'] ];
        foreach( func_get_args() as $v )
        {
            $out[] = is_array( $v ) ? md5( json_encode( $v ) ) : $v;
        }
        return implode( '.', $out );
    }

	/**
	 * 加锁
	 * @param  [type]  $name           锁的标识名
	 * @param  integer $timeout        循环获取锁的等待超时时间，在此时间内会一直尝试获取锁直到超时，为0表示失败后直接返回不等待
	 * @param  integer $expire         当前锁的最大生存时间(秒)，必须大于0，如果超过生存时间锁仍未被释放，则系统会自动强制释放
	 * @param  integer $waitIntervalUs 获取锁失败后挂起再试的时间间隔(微秒)
	 * @return [type]                  [description]
	 */
	public function lock($name, $timeout = 0, $expire = 15, $waitIntervalUs = 100000) {
		if(!$this->redis) return false;

		//取得当前时间
		$now = time();
		//获取锁失败时的等待超时时刻
		$timeoutAt = $now + $timeout;
		//锁的最大生存时刻,rem by dda
//		$expireAt = $now + $expire;

		$redisKey = $this->name('lock', $name);
		while (true) {
			//将rediskey的最大生存时刻存到redis里，过了这个时刻该锁会被自动释放
			$result = $this->redis->incr($redisKey);

            // edit by dda
//			if ($result != false) {
			if ($result == 1) {
				//设置key的失效时间
				$this->redis->expire($redisKey, $expire);
				//将锁标志放到lockedNames数组里
//				$this->lockedNames[$name] = $expireAt;
				return true;
			}

			//以秒为单位，返回给定key的剩余生存时间
			$ttl = $this->redis->ttl($redisKey);

			//ttl小于0 表示key上没有设置生存时间（key是不会不存在的，因为前面setnx会自动创建）
			//如果出现这种状况，那就是进程的某个实例setnx成功后 crash 导致紧跟着的expire没有被调用
			//这时可以直接设置expire并把锁纳为己用
			if ($ttl < 0) {
//				$this->redis->set($redisKey, $expireAt);
//				$this->lockedNames[$name] = $expireAt;

                // add by dda
                //设置key的失效时间
                $this->redis->expire($redisKey, 1);

                // rem by dda
//				return true;
			}

//			/*****循环请求锁部分*****/
//			//如果没设置锁失败的等待时间 或者 已超过最大等待时间了，那就退出
			if ($timeout <= 0 || $timeoutAt < microtime(true)) break;
//
//			//隔 $waitIntervalUs 后继续 请求
			usleep($waitIntervalUs);

		}

		return false;
	}

    public function del( $key )
    {
        if( !$this->redis )
        {
            return false;
        }

        $key = $this->name($key);

        return $this->redis->del( $key );
    }

    public function unlock($name)
    {
        return $this->redis->del( $this->name('lock', $name) ) >= 1;
    }

    /** 抄自 http://www.jb51.net/article/34636.htm
     * @param string $namespace
     *
     * @return string
     */
    public function createGUID($namespace = '') {
        static $guid = '';
        $uid = uniqid("", true);
        $data = $namespace;
        $data .= $_SERVER['REQUEST_TIME'];
        $data .= $_SERVER['HTTP_USER_AGENT'];
//        $data .= $_SERVER['LOCAL_ADDR'];
//        $data .= $_SERVER['LOCAL_PORT'];
        $data .= $_SERVER['REMOTE_ADDR'];
        $data .= $_SERVER['REMOTE_PORT'];
        $hash = strtoupper(hash('ripemd128', $uid . $guid . md5($data)));
        $guid = '{' .
            substr($hash,  0,  8) .
            '-' .
            substr($hash,  8,  4) .
            '-' .
            substr($hash, 12,  4) .
            '-' .
            substr($hash, 16,  4) .
            '-' .
            substr($hash, 20, 12) .
            '}';
        return $guid;
    }

    function setJson( $key, $value, $expire = 900 )
    {
        return $this->set($key, json_encode($value), $expire);
    }

    function set( $key, $value, $expire = 900 )
    {
        if (!$this->redis) {
            return false;
        }

        $key = $this->name($key);
        $this->redis->set($key, $value);
        $this->redis->expire($key, $expire);

        return true;
    }

    function get( $key, $default = false )
    {
        if ($this->ttl($key) > -1 && $this->is_refresh)
        {
            $this->del( $key );
            return $default;
        }

        $key = $this->name($key);

        if (!$this->redis) {
            return $default;
        }
        
        $data = $this->redis->get( $key );

        if ($data === false) {
            $data = $default;
        }

        return $data;
    }

    function getJson($key, $default = [])
    {
        $out = $this->get($key);
        return json_decode($out, true);
    }
    
    /** 检查是否存在缓存，如不存在，则执行相应逻辑，并更新缓存。
     * 
     * @param $class:所调用的对象
     * @param $method:方法名
     * @param $in:参数
     * @param $tail   : 有些缓存要带上额外的标识,在此实现,有可能是是字符串或数组
     * 
     * @return mixed：缓存或执行结果
     */
    function call_or_cache( $class, $method, $in, $tail = null )
    {
        $second = $in[ 0 ];
        unset( $in[ 0 ] );

        $key = get_class( $class ) . '.' . $method . '.' . md5( json_encode( [$tail, $in ] ) );

        $out = $this->get( $key );
        if( !$out )
        {
            $out = call_user_func_array([ $class, $method ], $in);
            $this->set( $key, $out, $second );
        }

        return $out;
    }

    /**压栈,支持JSON
     * @param $key
     * @param $value
     *
     * @return int
     */
    public function pushList($key, $value)
    {
        return $this->redis->rPush($this->name($key), json_encode($value));
    }

    /**出栈
     * @param $key
     *
     * @return mixed
     *  json或null
     */
    public function popList($key)
    {
        $out = $this->redis->lPop($this->name($key));
        return json_decode($out, true);
    }

    /** 获取List完整值
     * @param $key
     *
     * @return array: {len, data}
     */
    public function getList($key)
    {
        $key = $this->name($key);
        $len = $this->redis->llen($key);
        if( $len == 0 ) {
            $data = [];
        } else {
            $data = $this->redis->lRange($key, 0, $len - 1);

            foreach ($data as &$v) {
                $v = json_decode( $v, true);
            }
        }
        
        return ['len' => $len, 'data' => $data];
    }

    /** 获取zset中单页数据
     * @param $key
     * @param $in
     *  [page]:默认为1
     *  [pagesize]:默认为10
     *
     * @return array
     */
    public function getPage($key, $in)
    {
        $in += [
            'page' => 1,
            'pagesize' => 10
        ];

        $out = ['nav' => [
            'total' => 0,	// 总记录数
            'pagesize' => $in['pagesize'],	// 页宽
            'page' => $in['page'],			// 当前页
            'pagecount' => 0	// 总页码
        ], 'data' => []
        ];
        $out['data'] = $this->redis->zRevRange($this->name($key),
            ($in['page']-1)*$in['pagesize'],
            $in['page']*$in['pagesize'] - 1
        );

        if ($out['data']) {
            $out['nav']['total'] = $this->zCard($key);
            $out['nav']['pagecount'] = ceil( $out['nav']['total'] / $out['nav']['pagesize'] );
        }

        return $out;
    }

    public function clear()
    {
        $this->redis->flushAll();
    }

    /**
     * @param $oldName , 原来待改的名字
     * @param $newName , 新的名字,不需要加上前缀
     * list改名,适合 用错 key 值,重新改名 压入新名字的 list
     */
    public function renameList( $oldName, $newName )
    {
        $len = $this->redis->llen($oldName);
        $data = $this->redis->lRange($oldName, 0, $len - 1);

        foreach ($data as &$v) {
            $this->redis->rPush($this->name($newName), $v);//压入新的list, 名字按规范加上前缀
            $this->redis->lPop($oldName);//旧的list 出栈
        }
        return $this->redis->del( $oldName );
    }
}
