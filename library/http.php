<?php
class http
{
	function format( $value )
	{
		return addslashes( $value );
	}

	function clean( $array )
	{
		foreach( $array	as $key	=> $value ) $array[$key] = is_array( $array[$key] ) ? $this->clean( $value ) : $this->format( $value );
		return $array;
	}

	function get_or_post( $url, $post = array(), $referer = '', $cookie = '', $port = 80, $useragent = 'Mozilla/4.0 (compatible; MSIE 5.00; Windows 98)' )
	{
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
		//curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 1 );
		curl_setopt( $ch, CURLOPT_PORT, $port );
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_HEADER, 0 );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1 );
		curl_setopt( $ch, CURLOPT_USERAGENT, $useragent );
		// dda, 20151104，原值为0，但我觉得不合理
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 60 );
		// dda, 20151104，原值为0，但我觉得不合理
		curl_setopt( $ch, CURLOPT_TIMEOUT, 60 );

		if ( $referer != '' ) curl_setopt( $ch, CURLOPT_REFERER, $referer );
		if ( $cookie != '' ) curl_setopt( $ch, CURLOPT_COOKIE, $cookie );

		if ( count( $post ) > 0 )
		{
			$field = '';
			foreach( $post as $key => $value ) $field .= $key . '=' . urlencode( $value ) . '&';
			$field = substr( $field, 0, -1 );
			curl_setopt( $ch, CURLOPT_POST, 1 );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, $field );
		}

		$data = curl_exec( $ch );
		curl_close( $ch );
		return $data;
	}

	/** 从远程获得json内容，并转化为json返回，如访问出错或不是json内容，将返回null
	 * @param $url
	 * @return json
	 */
	function get_json( $url )
	{
		$log = biz('log');
		$content = $this->get_or_post( $url );
		$data = json_decode( $content, true );

		if( empty( $data ) )
		{
			$log->warning_log('获取json内容时出错，url：' . $url, 0, $content);
		}

	 	return $data;
	}

	/** 从远程获得内容
	 * @param $url
	 * @return
	 */
	function get( $url )
	{
		return file_get_contents( $url );
	}

	/** 获得ip所在地区
	 * @param $ip
	 * 返回值：
	 * 	{country,region，city,isp}
	 */
	function ipinfo( $ip )
	{
		$url = 'http://ip.taobao.com/service/getIpInfo.php?ip=' . $ip;
		$json = $this->get_json( $url );

		$json = $json[ 'data' ];
		return array(
			'country' => $json[ 'country' ],
			'region' => $json[ 'region' ],
			'city' => $json[ 'city' ],
			'isp' => $json[ 'isp' ]
		);
	}

	/** 获得来源ip
	 * @return string
	 */
	function get_ip()
	{
		$ip = '';
		$data = array( 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR' );

		foreach( $data as $v )
		{
			if ( isset( $_SERVER[ $v ] ) )
			{
				return $_SERVER[ $v ];
				break;
			}
		}

		return $ip;
	}

	/** 云盾扫描云服务器的的IP段
	 * @return bool
	 */
	function isAliIP($my_ip){
		$ip_list = array('42.120.145',
						 '110.75.105',
						 '110.75.185',
						 '110.75.186',
						 '112.125.32',
						 '112.124.36.187',
						 '112.124.36.32',
						 '121.0.19',
						 '121.0.30',
						 '121.42.0',
						 '42.120.142',
						 '42.156.250',
						 );

		foreach($ip_list as $v){
			//三位的IP段,都为0-24
			if($my_ip==$v || (substr($my_ip,0,strrpos($my_ip,'.'))==$v && (int)(substr($my_ip,strrpos($my_ip,'.')+1,strlen($my_ip)))<25)){
				return true;
			}
		}

		return false;
	}

	/**
	 * 记录注入式攻击
	 * @param $str
	 */
	function logHack($str){

		$log = biz('log');
		$deny = biz('deny');
		$now = time();
		$ip = $this->get_ip();

		if ($log->count(['module' => '注入式', 'time>' . ($now - 60)]) >= 5 ) {
			$deny->add( [ 'uid' => 0, 'ip' => $ip, 'start' => $now, 'end' => ( $now + 300 ),
			              'reason' => '注入式攻击' ] );
		} else {
			$log->info_log($str, 0, [$_SERVER], ['module' => '注入式']);
		}
	}
}