<?php
class data
{
	var $cache = null;
	var $pre = '999';//前缀，区分不同项目
	var $server = '7f410eabab924169.m.cnhzaliqshpub001.ocs.aliyuncs.com';

	/**
	 * 本地内存存储数据，以免频繁读写同一名的缓存。
	 * 因为是内存变量，所以不设过期。一般情况下不会有问题。
	 */
	var $data = array();

	function __construct()
	{
		if( class_exists('Memcached') )
		{
			$this->cache = new Memcached();
			$this->cache->addServer( $this->server, 11211 );
			if( config('develop') ) $this->pre .= $_SERVER['HTTP_HOST'];//区分开发模式的缓存
			register_shutdown_function( array( & $this, 'disconnect' ) );
		}
	}

	/** 传入字符串或数组，生成名字，以便于程序简化和统一
	 * @param $key：字符串或数组
	 * @return mixed：名字
	 */
	function name( $key )
	{
		return $this->pre . ( is_array( $key ) ? md5( json_encode( $key ) ) : $key );
	}

	function set( $key, $data, $expire = 900 )
	{
		$key = $this->name( $key );
		$this->data[ $key ] = $data;

		if( !$this->cache )
		{
			return false;
		}

		return $this->cache->set( $key, $data, $expire );
	}

	function get( $key )
	{
		$key = $this->name( $key );

		if( isset( $this->data[ $key ] ) )
		{
			return $this->data[ $key ];
		}

		if( !$this->cache )
		{
			return false;
		}

		$data = $this->cache->get( $key );
		// 如果$data为false时，此处有bug，但问题不大。
		if( !$data )
		{
			$this->data[ $key ] = $data;
		}

		return $data;
	}

	function del( $key )
	{
		$key = $this->name( $key );

		unset( $this->data[ $key ] );

		if( !$this->cache )
		{
			return false;
		}

		return $this->cache->delete( $key );
	}

	function clear()
	{
		$this->data = array();

		if( !$this->cache )
		{
			return false;
		}

		return $this->cache->flush();
	}

	function disconnect()
	{
		if( $this->cache )
		{
			$this->cache->quit();
		}
	}
}
?>