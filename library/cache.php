<?php
// 20161110,已经弃用ocs,改用redis

class cache
{
	var $cache = null;
	var $pre = '999';//前缀，区分不同项目
	var $server = '7f410eabab924169.m.cnhzaliqshpub001.ocs.aliyuncs.com';

	/**
	 * 本地内存存储数据，以免频繁读写同一名的缓存。
	 * 因为是内存变量，所以不设过期。一般情况下不会有问题。
	 */
	var $data = array();

	// 是否刷新缓存
	var $is_refresh = false;

	function __construct()
	{
		if( class_exists('Memcached') )
		{
			$this->cache = new Memcached();
			$this->cache->addServer( $this->server, 11211 );
			if( config('develop') ) $this->pre .= $_SERVER['HTTP_HOST'];//区分开发模式的缓存
			register_shutdown_function( array( & $this, 'disconnect' ) );
			$this->is_refresh = load('cookie')->get( 'cache' ) == 'new';
		}
	}

	/** 传入多参数，每个参数为字符串或数组，生成名字，以便于程序简化和统一
	 * @return mixed：名字
	 */
	function name()
	{
		$out = array();
		foreach( func_get_args() as $v )
		{
			$out[] = is_array( $v ) ? md5( json_encode( $v ) ) : $v;
		}
		return implode( '.', $out );
	}

	function set( $key, $data, $expire = 900 )
	{
		$key = $this->pre . $key;
		$this->data[ $key ] = $data;

		if( !$this->cache )
		{
			return false;
		}

		return $this->cache->set( $key, $data, $expire );
	}

	function get( $key )
	{
		if( $this->is_refresh )
		{
			$this->del( $key );
			return false;
		}

		$key = $this->pre . $key;

		if( isset( $this->data[ $key ] ) )
		{
			return $this->data[ $key ];
		}

		if( !$this->cache )
		{
			return false;
		}

		$data = $this->cache->get( $key );
		// 规定$data不允许为false，否则此处有bug。
		if( !$data )
		{
			$this->data[ $key ] = $data;
		}

		return $data;
	}

	function del( $key )
	{
		$key = $this->pre . $key;

		unset( $this->data[ $key ] );

		if( !$this->cache )
		{
			return false;
		}

		return $this->cache->delete( $key );
	}

	function clear()
	{
		$this->data = array();

		if( !$this->cache )
		{
			return false;
		}

		return $this->cache->flush();
	}

	function disconnect()
	{
		if( $this->cache )
		{
			$this->cache->quit();
		}
	}

	/** 检查是否存在缓存，如不存在，则执行相应逻辑，并更新缓存。
	 * @param $key
	 * @param $class
	 * @param $function
	 * @param $second
	 * 。。。（后续为多参数，给真正逻辑调用）
	 * @return mixed：缓存或执行结果
	 */
	function call_or_cache( $key, $class, $function, $second )
	{
		$out = $this->get( $key );
		if( !$out )
		{
			$argv = func_get_args();
			$out = call_user_func_array(array( $class, $function ), $argv[ 4 ]);
			$this->set( $key, $out, $second );
		}

		return $out;
	}
}
?>