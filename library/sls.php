<?php
// 阿里云日志服务 https://help.aliyun.com/document_detail/29074.html
require_once config( 'dir.root' ) . 'library/aliyun/Log/Log_Autoload.php';

class sls
{
    const endpoint = 'cn-hangzhou.log.aliyuncs.com';
    const accessKeyId = 'CBpAFp9JeOZSqSgR';
    const accessKey = '40BnYfSUnGo8g7V7AuO63v6CDDD4VN';

    private $project = 'tjw';
    private $logstore = 'log';
    private $client;

    public function __construct(){
        $this->client   = new Aliyun_Log_Client(self::endpoint, self::accessKeyId, self::accessKey);
    }

   function putLogs(Aliyun_Log_Client $client, $project, $logstore) {
    $topic = 'TestTopic';
    
    $contents = array( // key-value pair
        'TestKey'=>'TestContent'
    );
    $logItem = new Aliyun_Log_Models_LogItem();
    $logItem->setTime(time());
    $logItem->setContents($contents);
    $logitems = array($logItem);
    $request = new Aliyun_Log_Models_PutLogsRequest($project, $logstore, 
            $topic, null, $logitems);
    
    try {
        $response = $client->putLogs($request);
        var_dump($response);
    } catch (Aliyun_Log_Exception $ex) {
        var_dump($ex);
    } catch (Exception $ex) {
        var_dump($ex);
    }
}

function listLogstores(Aliyun_Log_Client $client, $project) {
    try{
        $request = new Aliyun_Log_Models_ListLogstoresRequest($project);
        $response = $client->listLogstores($request);
        var_dump($response);
    } catch (Aliyun_Log_Exception $ex) {
        var_dump($ex);
    } catch (Exception $ex) {
        var_dump($ex);
    }
}


function listTopics(Aliyun_Log_Client $client, $project, $logstore) {
    $request = new Aliyun_Log_Models_ListTopicsRequest($project, $logstore);
    
    try {
        $response = $client->listTopics($request);
        var_dump($response);
    } catch (Aliyun_Log_Exception $ex) {
        var_dump($ex);
    } catch (Exception $ex) {
        var_dump($ex);
    }
}

    function getLogs( $in = [] ) {
        $in += [
            'from' => time()-900,
            'to' => time(),
            'topic' => '',
            'query' => '',
            'pagesize' => 10,
            'page' => 1
        ];

         $request = new Aliyun_Log_Models_GetLogsRequest($this->project,
            $this->logstore, $in[ 'from' ], $in[ 'to' ], $in[ 'topic' ],
             $in[ 'query' ], $in[ 'pagesize' ], $in[ 'page' ] - 1, False);

        $out = [];

        try {
            $response = $this->client->getLogs($request);

            foreach($response -> getLogs() as $log)
            {
                $info = [ 'time' => $log -> getTime() ];
                foreach($log -> getContents() as $k => $v){
                    $info[ $k ] = $v;
                }
                $out[] = $info;
            }

        } catch (Aliyun_Log_Exception $ex) {
            var_dump($ex);
        } catch (Exception $ex) {
            var_dump($ex);
        }

        return $out;
    }

function getHistograms(Aliyun_Log_Client $client, $project, $logstore) {
    $topic = 'TestTopic';
    $from = time()-3600;
    $to = time();
    $request = new Aliyun_Log_Models_GetHistogramsRequest($project, $logstore, $from, $to, $topic, '');
    
    try {
        $response = $client->getHistograms($request);
        var_dump($response);
    } catch (Aliyun_Log_Exception $ex) {
        var_dump($ex);
    } catch (Exception $ex) {
        var_dump($ex);
    }
}
function listShard(Aliyun_Log_Client $client,$project,$logstore){
    $request = new Aliyun_Log_Models_ListShardsRequest($project,$logstore);
    try
    {
        $response = $client -> listShards($request);
        var_dump($response);
    } catch (Aliyun_Log_Exception $ex) {
        var_dump($ex);
    } catch (Exception $ex) {
        var_dump($ex);
    }
}
function batchGetLogs(Aliyun_Log_Client $client,$project,$logstore)
{
    $listShardRequest = new Aliyun_Log_Models_ListShardsRequest($project,$logstore);
    $listShardResponse = $client -> listShards($listShardRequest);
    foreach($listShardResponse-> getShardIds()  as $shardId)
    {
        $getCursorRequest = new Aliyun_Log_Models_GetCursorRequest($project,$logstore,$shardId,null, time() - 60);
        $response = $client -> getCursor($getCursorRequest);
        $cursor = $response-> getCursor();
        $count = 100;
        while(true)
        {
            $batchGetDataRequest = new Aliyun_Log_Models_BatchGetLogsRequest($project,$logstore,$shardId,$count,$cursor);
            var_dump($batchGetDataRequest);
            $response = $client -> batchGetLogs($batchGetDataRequest);
            if($cursor == $response -> getNextCursor())
            {
                break;
            }
            $logGroupList = $response -> getLogGroupList();
            foreach($logGroupList as $logGroup)
            {
                print ($logGroup->getCategory());

                foreach($logGroup -> getLogsArray() as $log)
                {
                    foreach($log -> getContentsArray() as $content)
                    {
                        print($content-> getKey().":".$content->getValue()."\t");
                    }
                    print("\n");
                }
            }
            $cursor = $response -> getNextCursor();
        }
    }
}
function deleteShard(Aliyun_Log_Client $client,$project,$logstore,$shardId)
{
    $request = new Aliyun_Log_Models_DeleteShardRequest($project,$logstore,$shardId);
    try
    {
        $response = $client -> deleteShard($request);
        var_dump($response);
    }catch (Aliyun_Log_Exception $ex) {
        var_dump($ex);
    } catch (Exception $ex) {
        var_dump($ex);
    }
}
function mergeShard(Aliyun_Log_Client $client,$project,$logstore,$shardId)
{
    $request = new Aliyun_Log_Models_MergeShardsRequest($project,$logstore,$shardId);
    try
    {
        $response = $client -> mergeShards($request);
        var_dump($response);
    }catch (Aliyun_Log_Exception $ex) {
        var_dump($ex);
    } catch (Exception $ex) {
        var_dump($ex);
    }
}
function splitShard(Aliyun_Log_Client $client,$project,$logstore,$shardId,$midHash)
{
    $request = new Aliyun_Log_Models_SplitShardRequest($project,$logstore,$shardId,$midHash);
    try
    {
        $response = $client -> splitShard($request);
        var_dump($response);
    }catch (Aliyun_Log_Exception $ex) {
        var_dump($ex);
    } catch (Exception $ex) {
        var_dump($ex);
    }
}
//$endpoint = 'cn-hangzhou.log.aliyuncs.com';
//$accessKeyId = 'CBpAFp9JeOZSqSgR';
//$accessKey = '40BnYfSUnGo8g7V7AuO63v6CDDD4VN';
//$project = '999';
//$logstore = 'log';
//$token = "";

//$client = new Aliyun_Log_Client($endpoint, $accessKeyId, $accessKey,$token);
    //listShard($client,$project,$logstore);
    //mergeShard($client,$project,$logstore,82);
    //deleteShard($client,$project,$logstore,21);
    //splitShard($client,$project,$logstore,84,"0e000000000000000000000000000000");
    //putLogs($client, $project, $logstore);
    //listShard($client,$project,$logstore);
    //batchGetLogs($client,$project,$logstore);
    //listLogstores($client, $project);
    //listTopics($client, $project, $logstore);
    //getHistograms($client, $project, $logstore);
//getLogs($client, $project, $logstore);
}

