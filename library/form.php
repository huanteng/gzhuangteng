<?php
class form
{
	/** 返回单选框内容
	 * @param $name：控件名字
	 * @param $value：当前选中控件件
	 * @param array $list：控件值列表，默认是或否
	 * @return string：适合显示的html
	 */
	function radio( $name, $value, $list = array( 1 => '是', 0 => '否' ) )
	{
		$out = '';
		foreach( $list as $key => $text )
		{
			$out .= '&nbsp;&nbsp;<label for="' . $name.$key . '"  class="radio"><input type="radio" id="'.$name.$key.'" name="' . $name . '" value="'. $key . '"';
			if( $key == $value )
			{
				$out .= ' checked="checked"';
			}
			$out .= '>' . $text . '</label> ';
		}

		return $out;
	}
	
	/*
	 * 返回多选框内容,name 传入值的集合字符串
	 */
	function checkbox( $name, $value, $list )
	{
		$out = '';
		foreach( $list as $key => $text )
		{
			$out .= '&nbsp;&nbsp;<label class="checkbox inline"><input type="checkbox" id="'.$name.$key.'" name="' . $name . '[]" value="'. $key . '"';
			if( stristr("$value","$key" )  )
			{
				$out .= ' checked';
			}
			$out .= '>' . $text . '</label> ';
		}

		return $out;
	}
	
	/*
	 * 返回下拉单选框内容
	 * option:
		 * empty：多一个空白的选项
		 * attr：附加字符串，如style=""
		 * title：控件首项名字，value为空。（本属性和empty很接近，推荐使用本属性）
	 */
	function select( $name, $value, $list, $option = array() )
	{
		if( !is_array( $option ) )
		{
			$option = array( 'empty' => $option );
		}
		$option += array(
			'empty' => false,
			'attr' => '',
			'title' => '',
		);

		$out = '<select id="' . $name . '" name="' . $name . '"';
		if( $option[ 'attr' ] != '' )
		{
			$out .= ' ' . $option[ 'attr' ];
		}
		$out .= '>';
		if( $option[ 'empty' ] )
		{
			$out .= '<option value="">-</option>';
		}
		if( $option[ 'title' ] != '' )
		{
			$out .= '<option value="">-' . $option[ 'title' ] . '-</option>';
		}
		foreach( $list as $key => $text )
		{
			$out .= '<option value="'. $key . '"';
			if( $key === $value )
			{
				$out .= ' selected';
			}
			$out .= '>' . $text . '['.$key.'] </option> ';
		}

		return $out.'</select>';
	}
	/** 返回数字下拉表
	 * @param $name：名
	 * @param $value：选中值
	 * @param int $begin：开始数字，默认为0
	 * @param int $end：结束数字，默认为10
	 * @param array $option：其它参数
	 * @return string
	 *
	 * 注：最终转为select实现
	 */
	function number_select( $name, $value, $begin = 0, $end = 10, $option = array() )
	{
		$list = array();
		for( $i = $begin; $i <= $end; ++$i )
		{
			$list[ $i ] = $i;
		}

		return $this->select( $name, $value, $list, $option);
	}

}
?>