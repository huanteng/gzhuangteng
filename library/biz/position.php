<?php
require_once dirname( __FILE__ ) . '/base.php';

class position extends base
{
	function __construct()
	{
		parent::__construct();
		$this->table = __CLASS__;
		$this->field = 'id,name,width,height,target,follow';
		$this->row_cache_second = 86400;
	}

	function follow_dict( $i = '' )
	{
		$data = array( 0 => '否', 1 => '是' );
		return $this->dict( $data, $i, '错误', '未定义position::follow' );
	}
}
?>