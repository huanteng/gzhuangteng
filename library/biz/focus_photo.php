<?php
require_once dirname( __FILE__ ) . '/base.php';

class focus_photo extends base
{
    function __construct()
    {
        parent::__construct();
        $this->table = __CLASS__;
        $this->field = 'id,file,url,seq,type,title,content';
    }
    function type_dict( $id = '' )
    {
        $data = array( '1' => '公司环境','2'=> '家庭成员','3'=>'聚餐旅游');
        return $this->dict( $data, $id, '错误', '无法识别的id' );
    }

}
?>