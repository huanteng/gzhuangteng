<?php
require_once dirname( __FILE__ ) .'/base.php';

class log extends base
{
	var $level = 0;

	function __construct()
	{
		parent::__construct();
		$this->table = __CLASS__;
		$this->field = 'id,module,type,relate_id,code,data,content,ip,url,time';
		$this->time_field = 'time';
	}
	function type_dict( $key = '' )
	{
		$data = array(
			0 => '信息',
			1 => '警告',
			2 => '错误',
		);

		if( $key != '' )
		{
			if( isset( $data[ $key ] ) )
			{
				$data = $data[ $key ];
			}
			else
			{
				$this->create( array( 'type' => 2, 'content' => '类型不存在，type=' . $key ) );

				$data = '错误';
			}
		}

		return $data;
	}

	/** 增加记录
	 * @param $info
	 */
	function create( $info )
	{
		if( isset( $info[ 'data' ] ) )
		{
			$info[ 'data' ] = load('arr')->php_json_encode( $info[ 'data' ] );
		}

		if( !isset( $info[ 'ip' ] ) )
		{
			$info[ 'ip' ] = load('http')->get_ip();
		}

		//阿里过来的IP只做普通的LOG
		if(load('http')->isAliIP($info[ 'ip' ]))
		{
			$info['type']=0;
		}

        // 写文件log
        $info += [
            'type' => 0,
            'relate_id' => '',
            'code' => '',
            'data' => '',
            'content' => ''
        ];
        $now = time();
        $filename = sprintf( '%scache/%s.log', config('dir.root'),
            date('YmdH', $now ));
        $content = sprintf( "%s\t%s\t%s\t%s\t%s\t%s\n", $info[ 'type' ],
            $info[ 'ip' ], $info[ 'module' ],
            $info[ 'relate_id' ], $info[ 'content' ], $info[ 'data' ] );
        file_put_contents( $filename, $content, FILE_APPEND );

		return $this->add( $info );
	}

	// 返回模块列表
	function module_dict()
	{
		$data = $this->get( 'distinct module', array('module !=""') );

		$out = array();
		foreach( $data as $v )
		{
			$out[ $v[ 'module' ] ] = $v[ 'module' ];
		}

		return $out;
	}

	// 设置调试级别
	function set_level( $level )
	{
		$this->level = $level;
	}


	function check_email($email)
	{
		if (!preg_match('/^[a-zA-Z0-9._]+@(([0-9a-zA-Z]+)[.])+[a-zA-Z]{2,5}/', $email)) {
			return false;
		} else {
			return true;
		}
	}
}
?>