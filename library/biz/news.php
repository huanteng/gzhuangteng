<?php
require_once dirname( __FILE__ ) . '/base.php';

class news extends base
{
	function __construct()
	{
		parent::__construct();
		$this->table = __CLASS__;
		$this->field = 'id,time,title,hot,from,type';
//		$this->time_field = 'time';
		$this->row_cache_second = 86400;
	}	
	function type_dict( $i = '' )
	{
		$data = array(1=>'新闻', 2=>'成功案例');
		return $this->dict( $data, $i, '错误', '未定义体育类型' );
	}

	function about_us_type()
	{
		return array(1=>'新手上路', 2=>'特色服务', 3=>'竞彩擂台', 4=>'客服中心');
	}

	function add_keyword($content)
	{
		$league = biz('league');
		$team = biz('team');
		$vs = biz('vs');

		$league_id_list = "";
		$team_id_list = "";
		$now = time();

		$data = $vs->get( 'distinct league_id', array( 'status=1', ' vs_time>'.(time()-7*24*60*60) ));
		$league_id_list = $vs->implode2($data);

		$data = $vs->get( 'team_id1,team_id2', array( 'status=1', ' vs_time>'.(time()-7*24*60*60) ));
		$team_id_list = $vs->implode2($data);

		$league_info = $league_id_list == '' ? array() :
			$league->get("short_hk,short_sc,short_en", array('id in ('.$league_id_list.')'));

		$team_info = $team_id_list == '' ? array() :
			$team->get("short_hk,short_sc,short_en",array('id in ('.$team_id_list.')'));

		$kw = array();
		$rp_kw = array();

		//联赛
		foreach($league_info as $val)
		{
			$kw[$val['short_hk']]=array('name'=>$val['short_hk'],'style'=>'hk','c1'=>array($val['short_sc'],'sc'),'c2'=>array($val['short_en'],'en'));
			$kw[$val['short_sc']]=array('name'=>$val['short_sc'],'style'=>'sc','c1'=>array($val['short_hk'],'hk'),'c2'=>array($val['short_en'],'en'));
			$kw[$val['short_en']]=array('name'=>$val['short_en'],'style'=>'en','c1'=>array($val['short_hk'],'hk'),'c2'=>array($val['short_sc'],'sc'));
		}

		//队
		foreach($team_info as $val)
		{
			$kw[$val['short_hk']]=array('name'=>$val['short_hk'],'style'=>'hk','c1'=>array($val['short_sc'],'sc'),'c2'=>array($val['short_en'],'en'));
			$kw[$val['short_sc']]=array('name'=>$val['short_sc'],'style'=>'sc','c1'=>array($val['short_hk'],'hk'),'c2'=>array($val['short_en'],'en'));
			$kw[$val['short_en']]=array('name'=>$val['short_en'],'style'=>'en','c1'=>array($val['short_hk'],'hk'),'c2'=>array($val['short_sc'],'sc'));
		}

		$i=0;
		foreach($kw as $key=>$value)
		{
			$key_n = strpos($content,$kw[$key]['name']);

			if($key_n>-1)
			{ //存在这个关键字
				if($i<4)
				{
					$i=$i+1;
					$c = strlen(urlencode($kw[$key]['name']))*3+80;

					$c1=$key_n-$c;
					if($key_n-$c<0) $c1=0;

					$cut_content = substr($content,$c1,$c);
					$has_link = strpos($cut_content,"href");

					if($has_link===false)
					{
						$rp_kw = $this->add_link($kw[$key]['style'],$kw[$key]['name']).'('.$this->add_link($kw[$key]['c1'][1],$kw[$key]['c1'][0]).','.$this->add_link($kw[$key]['c2'][1],$kw[$key]['c2'][0]).')</a>';;

						$content = $this->str_replace_once($kw[$key]['name'], $rp_kw, $content);
					}
				}

			}
		}

		return $content;
	}

	/** 替换第一个词
	 * @param $needle
	 * @param $replace
	 * @param $haystack
	 * @return mixed
	 */
	private function str_replace_once($needle, $replace, $haystack)
	{
		$pos = strpos($haystack, $needle);
		if ($pos === false)
		{
			return $haystack;
		}
		return substr_replace($haystack, $replace, $pos, strlen($needle));
	}

	private function add_link($ln,$kw)
	{
		switch($ln)
		{
			case "sc":
				return '<a href="/sc/league/'.urlencode(str_replace("-","－",$kw)).'" target="_blank">'.$kw."</a>";
				break;
			case "en":
				return '<a href="/en/league/'.urlencode(str_replace("-","－",$kw)).'" target="_blank">'.$kw."</a>";
				break;
			default:
				return '<a href="/league/'.urlencode(str_replace("-","－",$kw)).'" target="_blank">'.$kw."</a>";
			break;
		}

	}

	function get_content_imgs($content,$order='ALL'){
		$pattern="/<img.*?src=[\'|\"](.*?(?:[\.gif|\.jpg]))[\'|\"].*?[\/]?>/";
		preg_match_all($pattern,$content,$match);
		if(isset($match[1])&&!empty($match[1])){
			if($order==='ALL'){
				return $match[1];
			}
			if(is_numeric($order)&&isset($match[1][$order])){
				return $match[1][$order];
			}
		}
		return '';
	}

	function info( $id )
	{
		$info = $this->get_from_id( $id );

		$info += biz('news_content')->get_from_id( $id );

		return $info;
	}

}
?>