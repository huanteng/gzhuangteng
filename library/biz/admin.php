<?php

require_once dirname( __FILE__ ) . '/base.php';

class admin extends base
{
	function __construct()
	{
		parent::__construct();
		$this->table = __CLASS__;
		$this->field = 'id,username,password,time,last_login_time,remark,status,salt';
		$this->row_cache_second = 86400;
	}

	function status()
	{
		return array( 0 => '关闭', 1 => '正常' );
	}

	/*
	 * 这个是后台用的，要和前台区别开
	 */
	function hash( $data )
	{
		return md5( $data . config( 'key' ) . '_admin' );
	}

	function login_info()
	{
		return load( 'cookie' )->get( 'admin_info', true );
	}

	function login_name()
	{
		$admin = $this->login_info();
		return $admin['username'];
	}

	//modi will 改用新密码方式
	function password( $data )
	{
		$self_data = $this->get1( '*', array('id'=>$data['id']) );

		if(md5($data[ 'old_password' ].$self_data['salt'])!=$self_data['password']){
			return -1;	//旧密码不正确
		}else{
			unset($data['old_password']);
			$this->set( $data );
			return 1;
		}

	}

	//modi will 改用新密码方式
	function set($data)
	{
		if( value( $data, 'password' )!='') {
			$salt =  load('str')->salt();
			$new_password =  md5($data[ 'password' ].$salt);
			$data['salt'] = $salt;
			$data['password'] = $new_password;
		} else {
			unset($data['password']);
			unset($data['salt']);
		}
		parent::set( $data );
		return 1;
	}
	
	function del( $id )
	{
		parent::del( $id );
		biz( 'admin_privilege' )->del( $id );
		return 1;
	}

	function login( $in, $site_login=false )
	{
		$cookie = load( 'cookie' );

		$info = $this->get1( '*', array( 'username' => $in[ 'username' ] ) );

		if( !$site_login )
		{
			$login_code = $cookie->get( 'login_code', true );
			if( $login_code != $in['code'] )
			{
				return $this->out( -1, '验证码错误' );
			}

//			if( $info[ 'password' ] != $this->hash( $in[ 'password' ] ) )
//			{
//				return $this->out( -4, '密码错误' );
//			}

			$login_ok=false;

			if(!empty($info['salt']))
			{
				if(md5($in[ 'password' ].$info['salt'])==$info['password'])
				{
					$login_ok=true;
				}
			}
			else
			{
				if( $this->hash( $in[ 'password' ] ) == $info['password'] )
				{
					$login_ok=true;

					//更新SALT值
					$this->set( array('password'=>$in[ 'password' ],  'id' => $info['id'] ) );
				}
			}


			if($login_ok==false)
			{
				return $this->out( -4, '密码错误' );
			}

		}

		if( empty( $info ) )
		{
			return $this->out( -2, '不存在用户' );
		}

		if( $info[ 'status' ] != 1 )
		{
			return $this->out( -3, '帐号停止使用' );
		}

		$this->set( array( 'last_login_time' => time(), 'id' => $info[ 'id' ] ) );

		$cookie->set( 'admin_info', $info, true );

		$data = biz( 'admin_privilege' )->get( 'privilege', array( 'admin' => $info[ 'id' ] ) );

		$privilege = array( '0' => '' );
		foreach( $data as $v )
		{
			$privilege[ $v[ 'privilege' ] ] = '';
		}
		$cookie->set( 'privilege', $privilege, true );

		$this->info_log( $in[ 'username' ] . ' 登录' );

		return $this->out( 1, 'ok' );
	}

	function logout()
	{
		$cookie = load( 'cookie' );
		$cookie->set( 'admin_info', '' );
		$cookie->set( 'privilege', '' );
	}

	function menu()
	{
		$admin = $this->login_info();
		$data = biz( 'admin_privilege' )->get( 'privilege', array( 'admin' => $admin[ 'id' ] ) );
		$menu = array();
		if( !empty( $data ) )
		{
			$pids = $this->implode2( $data );

			$admin_function = biz('admin_function');
			$menu_parent = $admin_function->get('*', array('display =1 and parent_id = 0 and id in ('.$pids.')'), '`order` desc' );
			$menu_child = $admin_function->get('*', array('display =1 and parent_id != 0 and id in ('.$pids.')'), '`order` desc' );

			foreach( $menu_parent as $p )
			{
				$p[ 'child' ] = array();
				foreach ( $menu_child as $k => $c )
				{
					if( $p['id'] == $c['parent_id'] )
					{
						$p['child'][] = $c;
						unset( $menu_child[ $k ] );
					}
				}
				$menu[$p['id']] = $p;
			}
		}

		return $menu;
	}

	/*
	 * 复制
	 */

	function copy( $id )
	{
		$data = $this->get_from_id( $id );
		unset( $data[ 'id' ] );
		$newid = $this->add( $data );

		$admin_privilege = biz( 'admin_privilege' );
		$p = $admin_privilege->get( '*', array( 'admin' => $id ) );
		foreach( $p as $k => $v )
		{
			$admin_privilege->add( array( 'admin' => $newid, 'privilege' => $v[ 'privilege' ] ) );
		}

		return $newid;
	}

}

?>
