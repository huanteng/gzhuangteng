<?php
require_once dirname( __FILE__ ) . '/base.php';

class ad extends base
{
	function __construct()
	{
		parent::__construct();
		$this->table = __CLASS__;
		$this->field = 'id,position_id,filename,url,title,remark,level,expires,time';
		$this->time_field = 'time';
		$this->row_cache_second = 86400;
	}

	function del( $id )
	{
		parent::del( $id );
	}

	/**
	 * 清除过期广告，供任务系统调用
	 */
	function clear_expires()
	{
		$term = array( 'expires<>0 and expires < '. time() );
		$data = $this->get( 'id', $term );
		foreach( $data as $v )
		{
			$this->del( $v[ 'id' ] );
		}
	}

	function info( $pos_id )
	{
		$out = biz('position')->get_from_id( $pos_id );
		if( empty( $out ) )
		{
			return array();
		}

		$term = array( 'position_id=' . $pos_id, 'expires=0 or expires>=' . time() );
		$info = $this->get('*', $term);

		if( count($info) == 1 )
			return $info[0];
		else
			return $info;
	}

	/**
	 * 创建周榜图片
	 */
	function createWeekImage($sport,$device_type='pc')
	{

		$info = biz('rank')->get_rank($sport, 1);

		$font_data = [];

		switch ($device_type) {
			case 'pc':
				$font_data = [[11, 0, 20, 60,date("m月d日", $info[0]['begin']).'-'.date("m月d日", $info[0]['end'])],
							  [13, 0, 110, 245 ,$info[0]['username']]
				];

				break;

			case 'mobile':
				$font_data = [[11, 0, 414, 120,date("m月d日", $info[0]['begin']).'-'.date("m月d日", $info[0]['end'])],
							  [11, 0, 448, 180 , $info[0]['username']]
				];

				break;
		}

		$file_name = 'export_week_'.$sport.'_'.$device_type.'_'.date("Ymd").'.jpg';

		$this->createImg(
			'img/week_' . $sport . '_' . $device_type . '.jpg',
			'banner/'.$file_name,
			$font_data
		);


		//对应广告图的位置更新
		switch ($sport.'_'.$device_type) {
			case '1_pc':
				$in['id'] = 36;
				break;
			case '2_pc':
				$in['id'] = 37;
				break;
			case '1_mobile':
				$in['id'] = 77;
				break;
			case '2_mobile':
				$in['id'] = 78;
				break;
		}

		if (isset($in['id'])) {
			$in['task'] = 'logo';
			$in['filename'] = $file_name;
			biz('ad')->set($in);
		}

	}

	/*
	 * 创建月榜图片
	 */
	function createMonthImage($device_type = 'pc')
	{

		$info = biz('rank')->get_rank(1, 2);

		$font_data = [];

		switch ($device_type) {
			case 'pc':

				$font_data = [[24, 0, 70, 240, date("Y年m月", $info[0]['end'])],];

				break;

			case 'mobile':

				$font_data = [[24, 0, 240, 220, date("Y年m月", $info[0]['end'])],];

				break;
		}

		$file_name = 'export_month_' . $device_type.'_'.date("Ymd") . '.jpg';

		$this->createImg(
			'img/month_' . $device_type . '.jpg',
			'banner/' . $file_name,
			$font_data
		);

		//对应广告图的位置更新
		switch ($device_type) {
			case 'pc':
				$in['id'] = 35;
				break;
			case 'mobile':
				$in['id'] = 68;
				break;
		}

		if (isset($in['id'])) {
			$in['task'] = 'logo';
			$in['filename'] = $file_name;
			biz('ad')->set($in);
		}

	}

	/*
	 * 创建图片
	 */
	function createImg($back_img, $out_img, $font_data)
	{

		$img = imagecreatefromjpeg($back_img);
		$fgColor = imagecolorallocate($img, 255, 255, 255);
		foreach ($font_data as $v) {
			imagettftext($img, $v[0], $v[1], $v[2], $v[3], $fgColor, 'fonts/MicrosoftYahei.ttf', $v[4]);
		}
		imagejpeg($img, $out_img, 200);
		imagedestroy($img);
	}
}
?>