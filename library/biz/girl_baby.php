<?php
require_once dirname( __FILE__ ) . '/base.php';

class girl_baby extends base
{
	function __construct()
	{
		parent::__construct();
		$this->table = __CLASS__;
		$this->field = 'id,name,small_pic,big_pic,desc,from_id,top';
		$this->row_cache_second = 86400;
	}
	
	//取多少张美女图
	function get_girl_baby($limit)
	{
		$girl_baby = $this->get( '*', array(),'id desc',$limit, 86400 );
		return $girl_baby;
	}

}
?>