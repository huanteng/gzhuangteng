<?php
//业务逻辑基类
class base
{
	var $db = null;
	var $table = null;	// 表名，以方便create、remove、modify、search、info方法使用
	var $id = 'id';		// 表的键字段名

	// 字段列表，用逗号隔开，不要出现空格。保留字也不必用"`"包含
	// 主要被add、set、del中实现通用。
	// 调用add、set、del时，不在field中定义的值，将被忽略
	var $field = 'field1,field2';

	/*
	 * 设置时间字段，当add/set时，本字段将自动取当前时间time()。按需重载。
	 * 注：
	 * 	默认值表示没这样的字段；
	 *  add/set中如果不希望本字段取当前时间，可显式传入值以覆盖。
	*/
	var $time_field = null;

	/** 设置行info缓存时间。默认为null（不缓存），单位为秒
	 *  当设置时，set/set_by_term会更新缓存，而get_from_id、get_field_from_id均首先从缓存获取。del时删除相应缓存。
	 */
	var $row_cache_second = null;

	function __construct()
	{
		$this->db = load('db');
	}

	public function __call($method, $args)
	{
		if( $method[ 0 ] == '_' )
		{
			return load('redisDB')->call_or_cache($this, substr( $method, 1 ), $args);
		}
		else
		{
			$this->error_log( $method . '()不存在' );
			die();
		}
	}

	//返回哈希值
	function hash($data)
	{
		return md5($data . config('key'));
	}

	/*
	 * 获得数组形式的字段列表，供内部使用，不必重载
	 */
	function _field()
	{
		return array_flip( explode( ',', $this->field ) );
	}

	/*
	 * 增加记录
	 * 参数：
	 * 	in：一维数组，包含以下对应串 字段名=>值
	 * 返回值：
	 * 	自增id
	 */
	function add($in)
	{
		$field = self::_field();

		$time_field = $this->time_field;
		if( !is_null( $time_field ) )
		{
			$in[ $time_field ] = time();
		}

		foreach( $in as $k => $v )
		{
			if (!isset($field[$k]))
			{
				unset($in[$k]);
			}
		}

		$id = $this->db->add($this->table, $in);

        if ($this->row_cache_second) {
            $this->get_from_id($id);
        }

        return $id;
	}

	/* 返回本数据表中符合条件的数量
	 * 参数：
	 * 	term：条件数组
	 */

	function count($term=array(), $second = 0 )
	{
		$data = $this->get1('count(*) as c', $term, '', $second);
		return $data[ 'c' ];
	}
	
	/* 返回本数据表中符合条件的字段总和
	 * 参数：
	 *  $field : 统计的字段
	 * 	term：条件数组
	 */
	function sum( $field, $term )
	{
		$data = $this->get1('sum(`'.$field.'`) as s', $term);
		return value( $data, 's', 0 );
	}

	// 修改资料，返回影响条数
	function set($in)
	{
		$id_field = $this->id;
		$id = $in[ $id_field ];

		if( !is_array( $in ) )
		{
			$this->error( 'set传入错误的参数，', $in );
		}

		unset($in[ $id_field ]);

		return $this->set_by_term( $in, [ $id_field => $id] );
	}

	/** 按条件更新
	 * @param $in：更新项
	 * @param $term：条件
	 * @return 影响条数
	 */
	function set_by_term( $in, $term )
	{
		$field = self::_field();

		$in = $this->to_array( $in );

		$time_field = $this->time_field;
		if( !is_null( $time_field ) )
		{
			$in[ $time_field ] = time();
		}

		foreach( $in as $k => $v )
		{
			if (!is_numeric($k) && !isset($field[$k]))
			{
				unset($in[$k]);
			}
		}

		$term = $this->to_array( $term );

		$out = $this->db->set($this->table, $in, $term );

		// 处理缓存
		if( $this->row_cache_second )
		{
			if( isset( $term[ $this->id ] ) )
			{
				$info = $this->get1('*', [$this->id => $term[ $this->id ]]);

                $key = $this->idCacheKey($term[ $this->id ]);
                $cache = load('redisDB');
				load('__')->each($info, function($v, $k) use($cache, $key){
					$cache->hSet( $key, $k, $v );
				});
				$cache->expire($key, $this->row_cache_second);
			}
		}

		return $out;
	}

	// 删除记录，$id是单个数字，或用逗号隔开的多个关键字
	// 返回值：true|false
	function del($id)
	{
		$out = ( $this->db->del($this->table, array( $this->id . ' in(' . $id . ')' ) ) > 0 );

		// 处理缓存
		if( $this->row_cache_second )
		{
			foreach( explode( ',', $id ) as $v )
			{
				load('redisDB')->del( $this->idCacheKey($v) );
			}
		}

		return $out;
	}

	/*
	 * 按条件删除，返回true|false
	 */
	function del_by_term( $term )
	{
		$term = $this->to_array( $term );

		$out = ( $this->db->del($this->table, $term ) > 0 );

		return $out;
	}

	/** 查询，可用于复杂sql语句
	 * @param $field：查询的字段项
	 * @param $other：from {table} 后面的sql表达
	 * @param $table：数据表，如需指定，请用本参数
	 * @param $second：缓存时间
	 *
	 * @return 记录集
	 */
	function select( $field, $other, $table = '', $second = 0 )
	{
		if( $table == '' )
		{
			$table = $this->table;
		}

		$sql = 'select ' . $field . ' from ' . $table . ' ' . $other;
		return $this->db->select( $sql, $second );
	}

	// 通用的查询函数，返回二维参数：
	// $in：数据组，部分特殊变量说明如下
	//	orderby：排序，默认为空
	//	pagesize：页宽，默认为10
	// 		0表示不限，将返回全部数据，即相当于所有的$result['data']
	// 		负数时，返回结果中的-pagesize的数据，即相当于的$result['data']的最新的-pagesize条
	//	 	正数时，返回page页数据及返回数据，其中 nav 翻页数据，data 数据
	//	page：当前页，仅当pagesize>0时才有用
	//	pagecount：最多可查询多少页数据，如不传入，默认可查询全部
	// $equal：$in中，使用＝作为比较符的字段名列表，分别和form及数据表的字段对应
	// $like：同理，只是和like作为比较符
	// $q：q关键字要检查的字段名列表，均使用like为比较符，字段间用or作为处理
	// $option：额外参数，便于扩充
	//	in：$in中，使用 in(...)来比较
	// 	term：额外的where条件，数组形式，每个元素是一个条件，默认为空。
	//		如果是单个条件，也可以只是一个字符串
	// 	join：条件间关系，默认为and
	//	field：要返回的字段，默认为*
	//	groupby：分组条件，默认为空
	//	table：从哪个表取？默认为空，会自动从table获得，但在join table时，可以通过本参数传入
	//	second：缓存时间
	// $result['nav'] 翻页数据
	// $result['data'] 数据
	function search( $condition, $equal, $like = array(), $q = array(), $option = array() )
	{
		$option += array( 'term' => array(), 'join' => 'and', 'groupby' => '' , 'field' => '*', 'table' => $this->table,
			'in' => array(), 'second' => 0 );

		$term = $option[ 'term' ];
		$term = $this->to_array( $term );

		foreach( $equal as $v )
		{
			if( value( $condition, $v ) != '' )
			{
				$term[] = "$v = '" . $condition[$v] . "'";
			}
		}

		foreach( $like as $v )
		{
			if( value( $condition, $v ) != '' )
			{
				$term[] = "$v like '%" . $condition[$v] . "%'";
			}
		}
		if( value($condition, 'q') != '' && count( $q ) > 0 )
		{
			$s = array();
			foreach ($q as $v)
			{
				$s[] = "$v like '%" . $condition['q'] . "%'";
			}
			$term[] = implode( ' or ', $s );
		}

		$option[ 'in' ] = $this->to_array( $option[ 'in' ] );
		foreach( $option[ 'in' ] as $v )
		{
			if( value( $condition, $v ) != '' )
			{
				$term[] = "$v in(" . $condition[$v] . ")";
			}
		}

		$where = $this->db->term($term, $option[ 'join' ]);

		$count = ' from ' . $option[ 'table' ];
		if( $where != '' )
		{
			$count .= ' where ' . $where;
		}

		if( $option[ 'groupby' ] != '' )
		{
			$count .= ' group by ' . $option[ 'groupby' ];
		}

		$orderby = value($condition, 'orderby');
		if( $orderby != '' )
		{
			$orderby = ' order by ' . $orderby;
		}

		$sql = 'select ' . $option[ 'field' ] . $count . $orderby;

		$result = array();
		$pagesize = int_value( $condition, 'pagesize', 10);
		if ($pagesize == 0) {
			$result['data'] = $this->db->select($sql, $option[ 'second' ] );
		} elseif ($pagesize < 0) {
			$result = $this->db->select($sql . ' limit ' . (-$pagesize), $option[ 'second' ] );
		} else {

			if (!isset($option['not_count'])) {
				if ($option['groupby'] != '') {
					$base = biz('base');
					$base->table
						= "(select  distinct({$option[ 'groupby' ]}) $count ) as t";
					$total = $base->count(array(), $option['second']);
				} else {
					$total = $this->db->total($count, $option['second']);
				}
			}else{
				$total = 20000;
			}

			$pagecount = ceil( $total / $pagesize );
			if( isset($condition['pagecount' ] ) && $pagecount > $condition['pagecount' ] )
			{
				$pagecount = $condition['pagecount' ];
			}

			$page = int_value( $condition, 'page', 1 );
			$page = is_numeric( $page ) && $page >= 1 ? $page : 1;
			if( $page > $pagecount )
			{
				$page = $pagecount;
			}

			$result = array();
			$result['nav'] = array( 'total' => $total,	// 总记录数
				'pagesize' => $pagesize,	// 页宽
				'page' => $page,			// 当前页
				'pagecount' => $pagecount	// 总页码
			);

			if( $total == 0 )
			{
				$result['data'] = array();
			}
			else
			{
				$sql .= ' limit ' . (( $page - 1 ) * $pagesize) . ', ' . $pagesize;
				$result['data'] = $this->db->select($sql);
			}
		}
		return $result;
	}

	//检查 $key是否存在并且是数字，如存在则返回，否则返回$default
	function number($in, $key, $default = 0)
	{
		$value = value( $in, $key, $default );
		return is_numeric( $value ) ? $value : $default;
	}

	//数组$data，一般只包含一列，最多两列 将数组转为字符串返回，主要应用于in查询
	// 参数说明：
	// pre：前后辍
	// join：用什么符号串接
	function implode2($data, $pre = '', $join = ',')
	{
		$s = '';
		foreach ($data as $row) {
			if (is_array($row)) {
				foreach ($row as $v)
					$s .= $pre . $v . $pre . $join;
			}else
				$s .= $pre . $row . $pre . $join;
		}
		if( $s != '' && $join != '' )
		{
			$s = substr($s, 0, -strlen( $join ) );
		}

		return $s;
	}

	/** 保证参数为数组形式。大部分db操作参数要求为数组，但为了方便调用，也允许仅传入单个字符串，此时，表示单个元素的数组。
	 * @param $str
	 * @return array
	 */
	function to_array( $str )
	{
		if( !is_array( $str ) && strlen($str)>0)
		{
			$str = array( $str );
		}

		return $str;
	}

	function get1( $field, $term = [], $orderby = '', $second = 0 )
	{
		$data = $this->get( $field, $term, $orderby, 1, $second );
		return value( $data, '0', array() );
	}

	/** 获得数据
	 * @param $field：字段列表
	 * @param array $term：条件，有两种形式
	 * 	形式一：字符串
	 * 	形式二：多条件形式的数组。
	 * 	注：传入前请自行防注入
	 * @param string $orderby
	 * @param string $limit
	 * @return mixed
	 */
	function get( $field, $term = array(), $orderby = '', $limit = '', $second = 0 )
	{
		$term = $this->to_array( $term );
		return $this->db->get( $field, $this->table, $term, $orderby, $limit, $second );
	}

	/** 获得符合条件的id，并用半角逗号串接，本函数用于简化代码
	 * @param $term：条件
	 * @param int $mode：返回模式，如下：
	 * 	1：（默认）字符串形式，用逗号隔开
	 * 	2：数组形式，每个id是一个元素
	 * @param $id_field: key字段名字
	 */
	function get_ids( $term = array(), $mode = 1, $id_field='' )
	{
		$id = $id_field == '' ? $this->id : $id_field;
		$data = $this->get( $id, $term );

		$out = array();

		foreach( $data as $v )
		{
			$out[] = $v[ $id ];
		}

		if( $mode == 1 )
		{
			$out = implode( ',', $out );
		}

		return $out;
	}

	// 是否存在符合条件的数据，如存在返回true
	function exists($term = array())
	{
		$term = $this->to_array( $term );
		return $this->db->exists($this->table, $term);
	}

	/* 主要为了便于使用
	 * 参数：
	 * 	name
	 * 	default，可选，如不存在时，默认返回
	 * 	key：可选，查询的name条件字段，默认值为name
	 * 返回值：用户id字段，如不存在返回default
	 * 常用调用：return $user->get_name_from_id( 'dda' )
	 */
	function get_id_from_name($name, $default = '', $key = 'name')
	{
		$id_field = $this->id;
		$info = $this->get1( $id_field, array($key => $name));
		return value($info, $id_field, $default);
	}

	/* 主要为了便于使用
	 * 参数：
	 * 	id
	 * 	default，可选，如不存在时，默认返回
	 * 	name，可选，查询的name字段，默认值为name
	 * 返回值：用户name字段，如不存在返回default
	 */
	function get_name_from_id($id, $default = '', $name = 'name')
	{
		$id_field = $this->id;

		$info = $this->get1($name, array($id_field => $id));
		return value($info, $name, $default);
	}

    /** 根据id,获得缓存key名.主要是便于通用缓存处理
     * @param $id
     *
     * @return string:
     */
    public function idCacheKey($id)
    {
        return 't.' . $this->table . '.id.' . $id;
    }

	/* 主要为了便于使用
	 * 参数：
	 * 	id
	 * 返回值：数组，如无对应值，返回空数组
	 */
	function get_from_id($id)
	{
		if( $this->row_cache_second )
		{
			$key = $this->idCacheKey($id);
			$cache = load('redisDB');
			$info = $cache->hGetAll( $key );

			if( $info )
			{
				return $info;
			}
		}
		$info = $this->get1('*', [$this->id => $id]);

		if( $this->row_cache_second )
		{
			load('__')->each($info, function($v, $k) use($cache, $key){
                $cache->hSet( $key, $k, $v );
            });
            $cache->expire($key, $this->row_cache_second);
		}

		return $info;
	}

	/* 作用：通过id，返回对应的单个字段的值
	 * 参数：
	 * 	id
	 * 	field：要查询的字段，只支持单字段
	 * 	default：可选，默认为空，如果不存在，返回的默认值
	 * 返回值：该字段的值，或不存在时返回默认值
	 */
	function get_field_from_id($id, $field, $default = '')
	{
		$data = $this->get1($field, array($this->id => $id));
		return value($data, $field, $default);
	}

	/*
	 * 从一个字段条件中，获得记录集，相当于get( '*', array( key => value ) )
	 * 注意：仅为了程序简化，这和get_from_id不同，前者只返回一条数据，而本函数返回记录集
	 */
	function get_from_key( $key, $value )
	{
		return $this->get( '*', array( $key => $value ) );
	}

	/* 记录信息于错误文件中
	 * 不定参数，可以为字符串或对象
	 */
	function error()
	{
		$data = array();

		$list=func_get_args();
		foreach ( $list as $v )
		{
			$data = array_merge( $data, $this->to_array( $v ) );
		}

		$data['sql']  = load('db')->sql;
		$data['debug_backtrace'] = debug_backtrace();
		$data['server'] = $_SERVER;
		$data['request'] = $_REQUEST;

		biz('log')->log( 'error', 0, config( 'server.HTTP_HOST' ) . config( 'server.REQUEST_URI' ),
			array( 'type' => 2, 'data' => $data ) );
	}


	/*
	 * 联赛名字和球队名字
	 * 参数：
	 * $data : 数组
	 * [34993] => Array
	  (
	  [sc] => 新潟天鵝乙队
	  [hk] => 新潟天鵝乙隊
	  [en] => Albirex Niigata FC
	  )
	 * $id : 球队ID
	 * $language : 语言，简繁英
	 */

	function index_name($data, $id, $language = 'sc')
	{
		if (isset($data[$id][$language]) && $data[$id][$language] != '')
			$return = $data[$id][$language];
		elseif (isset($data[$id]['hk']) && $data[$id]['hk'] != '')
			$return = $data[$id]['hk'];
		elseif (isset($data[$id]['en']) && $data[$id]['en'] != '')
			$return = $data[$id]['en'];
		else
			$return = ' - ';
		return $return;
	}

	/*
	 * 格式化胜率
	 */

	function rate($rate)
	{
		if ($rate >= 1)
			return '100%';
		elseif ($rate > 0 && $rate < 1)
			return (number_format($rate * 100, 1) . '%');
		else
			return '0%';
	}

	/*
	 * 返回正常输出数组，用于对象间返回时简短写法。
	 * 注意：这和controller::out()有区别
	 *
	 * 有三种参数调用形式
	 * 形式一，只有一个参数：
	 *	data：一般是json格式，用于正常的返回记录集，此时，code=1, memo='成功'
	 *
	 * 形式二，有两个参数：
	 *	code：输出代码，一般来说，负数表示不同的出错代码，正数表示正确
	 *	memo：描述，一般用于表达出错描述。
	 *
	 * 形式三,有三个参数
	 * code：输出代码，一般来说，负数表示不同的出错代码，正数表示正确
	 * memo：描述，一般用于表达出错描述。
	 * data:一般是json格式
	 *
	 * 返回值：json{code, memo ,data }
	 */
	function out($s1, $s2 = '', $s3 = [])
	{
		if( $s2 == '' )
			return array( 'code' => 1, 'data' => $s1, 'memo' => '成功' );

		if( is_array( $s2 ) )
			return array( 'code' => $s1, 'data' => $s2, 'memo' => '' );

		return array('code' => $s1, 'memo' => $s2, 'data' => $s3);
	}

	/*
	 * 取两个值中的另一个值
	 * 参数：
	 * 	value1：值1
	 * 	value2：值2
	 * 	current：当前值
	 */
	function other( $value1, $value2, $current )
	{
		return $current == $value1 ? $value2 : $value1;
	}

	/** 通用字典定义
	 * @param $data：字典定义
	 * @param $key：关键字
	 * @param string $default：不存在时返回值
	 * @param string $alert_text：不存在时，报警提示文字，空表示不提示
	 * @return mixed
	 */
	function dict( $data, $key = '', $default = '错误', $alert_text = '未定义' )
	{
		if( $key === '' )
		{
			return $data;
		}

		if( !isset( $data[ $key ] ) )
		{
			if( $alert_text != '' )
			{
				$url = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
				$this->log( 'dict', 0, $alert_text . ', ' . $key . ', ' . $url  );
			}

			$data[ $key ] = $default;
		}

		return $data[ $key ];
	}

	/** 记录log，放在此便于程序简化调用
	 * @param $module：模块，一般是数据表名，或功能名。如果是空字符串，则自动取当前对象名
	 * @param $relate_id：相关id，如无，建议填入0
	 * @param $content：内容
	 * @param array $data：其它参数，参数 log::create
	 *
	 * 注：module参数为数字时的特殊用法：
	 * 	此时，表示log调试级别，将用来和当前全局的log调试级别作比较，如符合条件，才记录。（这时，module采用当前对象名，或在data参数中传入）
	 */
	function log( $module, $relate_id, $content, $data = array() )
	{
		$log = biz( 'log' );

		if( is_int( $module ) )
		{
			if( $log->level < $module )
			{
				return;
			}

			$module = value( $data, 'module' );
		}

		if( $module == '' )
		{
			$module = $this->table;
		}

		$data[ 'module' ] = $module;
		$data[ 'relate_id' ] = $relate_id;
		$data[ 'content' ] = $content;

		$log->create( $data );
	}

	/**
	 * 普通log，永远记录
	 * @param $content
	 * @param int $relate_id
	 * @param array $data
	 */
	function info_log( $content, $relate_id=0, $data = [], $info = [] )
	{
		$add = [];
		$add[ 'module' ] = $this->table;
		$add[ 'relate_id' ] = $relate_id;
		$add[ 'content' ] = $content;
		$add[ 'data' ] = $data;

		foreach($info as $k=>$v)
		{
			$add[ $k ] = $v;
		}

		biz('log')->create( $add );
	}

	/**
	 * 警告的log
	 * @param $content
	 * @param int $relate_id
	 * @param array $data
	 */
	function warning_log( $content, $relate_id=0, $data = array() )
	{
		$this->info_log( $content, $relate_id, ['data'=>$data, 'server'=>$_SERVER], ['type'=>1] );
	}

	/**
	 * 严重错误的log
	 * @param $content
	 * @param int $relate_id
	 * @param array $data
	 */
	function error_log( $content, $relate_id=0, $data = array() )
	{
		$this->info_log( $content, $relate_id, ['data'=>$data, 'server'=>$_SERVER], ['type'=>2] );
	}

	/**
	 * 按日志级别记录log，1~9级
	 * @param $content
	 * @param int $relate_id
	 * @param array $data
	 */
	function log1( $content, $relate_id=0, $data = array() )
	{
		if( biz('log')->level >= 1 )
		{
			$this->info_log( $content, $relate_id, $data );
		}
	}

	function log2( $content, $relate_id=0, $data = array() )
	{
		if( biz('log')->level >= 2 )
		{
			$this->info_log( $content, $relate_id, $data );
		}
	}

	function log3( $content, $relate_id=0, $data = array() )
	{
		if( biz('log')->level >= 3 )
		{
			$this->info_log( $content, $relate_id, $data );
		}
	}

	function log4( $content, $relate_id=0, $data = array() )
	{
		if( biz('log')->level >= 4 )
		{
			$this->info_log( $content, $relate_id, $data );
		}
	}

	function log5( $content, $relate_id=0, $data = array() )
	{
		if( biz('log')->level >= 5 )
		{
			$this->info_log( $content, $relate_id, $data );
		}
	}

	function log6( $content, $relate_id=0, $data = array() )
	{
		if( biz('log')->level >= 6 )
		{
			$this->info_log( $content, $relate_id, $data );
		}
	}

	function log7( $content, $relate_id=0, $data = array() )
	{
		if( biz('log')->level >= 7 )
		{
			$this->info_log( $content, $relate_id, $data );
		}
	}

	function log8( $content, $relate_id=0, $data = array() )
	{
		if( biz('log')->level >= 8 )
		{
			$this->info_log( $content, $relate_id, $data );
		}
	}

	function log9( $content, $relate_id=0, $data = array() )
	{
		if( biz('log')->level == 9 )
		{
			$this->info_log( $content, $relate_id, $data );
		}
	}

	/*
	 * 合表查询
	 * 参数：
	 * $in['table1'] , $in['table2']
	 * $equal, $like = array(), $q = array(), $option = array() 参考 search()
	 */
	function union($in, $equal, $like = [], $q = [], $option = [])
	{


		$option += array(
			'term' => array(),
			'join' => 'and',
			'groupby' => '',
			'field' => '*',
			'table1' => $in['table1'],
			'table2' => $in['table2'],
			'in' => array(),
			'second' => 0
		);

		$term = $option['term'];

		foreach ($term as $key => $value)
		{
			if (empty($value))
			{
				unset($term[$key]);
			}
		}

		$term = $this->to_array($term);

		foreach ($equal as $v)
		{
			if (value($in, $v) != '')
			{
				$term[] = "$v = '" . $in[$v] . "'";
			}
		}


		foreach ($like as $v)
		{
			if (value($in, $v) != '')
			{
				$term[] = "$v like '%" . $in[$v] . "%'";
			}
		}

		if (value($in, 'q') != '' && count($q) > 0)
		{
			$s = array();
			foreach ($q as $v)
			{
				$s[] = "$v like '%" . $in['q'] . "%'";
			}
			$term[] = implode(' or ', $s);
		}

		$option['in'] = $this->to_array($option['in']);
		foreach ($option['in'] as $v)
		{
			if (value($in, $v) != '')
			{
				$term[] = "$v in(" . $in[$v] . ")";
			}
		}

		$where = $this->db->term($term, $option['join']);

//		$count = ' from ' . $option[ 'table' ];
		$count = '';
		if ($where != '')
		{
			$count .= ' where ' . $where;
		}


		$orderby = value($in, 'orderby');
		if ($orderby != '')
		{
			$orderby = ' order by ' . $orderby;
		}

		$sql_1 = 'select ' . $option['field'] . ' from ' . $option['table1'] . $count . $orderby;
		$sql_2 = 'select ' . $option['field'] . ' from ' . $option['table2'] . $count . $orderby;


		$result = array();
		$pagesize = value($in, 'pagesize', 10);


		if ($pagesize == 0)
		{

			$rs1 = $this->db->select($sql_1, $option['second']);
			$rs2 = $this->db->select($sql_2, $option['second']);

			$result['data'] = array_merge($rs1, $rs2);

		}
		elseif ($pagesize < 0)
		{


			$rs1 = $this->db->select($sql_1 . ' limit ' . (-$pagesize), $option['second']);
			$rs2 = $this->db->select($sql_2 . ' limit ' . (-$pagesize), $option['second']);
			$result = array_merge($rs1, $rs2);

		}
		else
		{


			$count_1 = $this->db->total('from ' . $option['table1'] . $count, $option['second']);
			$count_2 = $this->db->total('from ' . $option['table2'] . $count, $option['second']);


			$total = $count_1 + $count_2;
			$cut = $count_1 % $pagesize;

			$total_page_1 = ceil($count_1 / $pagesize);
			$total_page_2 = ceil($count_2 / $pagesize);
			$pagecount = ceil(($count_1 + $count_2) / $pagesize);

//			$pagecount = ceil( $total / $pagesize );
			if (isset($in['pagecount']) && $pagecount > $in['pagecount'])
			{
				$pagecount = $in['pagecount'];
			}

			$page = value($in, 'page', 1);
			$page = is_numeric($page) && $page >= 1 ? $page : 1;
			if ($page > $pagecount)
			{
				$page = $pagecount;
			}

			$result = array();
			$result['nav'] = array(
				'total' => $total, // 总记录数
				'pagesize' => $pagesize, // 页宽
				'page' => $page, // 当前页
				'pagecount' => $pagecount // 总页码
			);

			if ($total == 0) {
				$result['data'] = array();
			}
			else
			{


				$rs1 = array();
				$rs2 = array();

				if ($page <= $total_page_1)
				{

					if ($page == $total_page_1 && $cut > 0)
					{
						$sql_1 .= ' limit ' . (($page - 1) * $pagesize) . "," . $cut;
					}
					else
					{
						$sql_1 .= ' limit ' . (($page - 1) * $pagesize) . "," . $pagesize;
					}

					$rs1 = $this->db->select($sql_1);
				}


				if ($page >= $total_page_1 && $page <= $total_page_1 + $total_page_2)
				{

					if ($page == $total_page_1 && $cut > 0)
					{
						$sql_2 .= ' limit ' . (($page - $total_page_1) * $pagesize) . "," . ($pagesize - $cut);
					}
					else
					{

						$sql_2 .= ' limit ' . ((($page - $total_page_1) * $pagesize) - $cut) . "," . $pagesize;

					}

					$rs2 = $this->db->select($sql_2);
				}


				$result['data'] = array_merge($rs1, $rs2);

			}
		}
		//d();
		return $result;

	}

    /** 检查输入的参数,主要用于防注入
     * @param $in:原参数集
     * @param $config:设置方案,可能有以下的属性
     *      default:默认值,格式为['field1' => 'val1'...]
     *      number:必须为数字型的字段名,格式为['number_field1' => 'tips1', 'number_field2'...]
     *
     *      must:必填字段,格式为['must_field1' => 'tips1', 'must_field2'...]
     *               如果参数中没填入该字段,则返回对应的tips,或die
     * @return mixed
     *  如果通过,则返回处理后的参数,否则,对应的出错信息
     */
	function checkParameter($in, $config)
	{
        $config += [
            'default' => [],
            'number' => [],
            'numbers' => [],
            'must' => []
        ];

		$http = load('http');

        foreach ($config['must'] as $k => $v) {
            if (is_int($k)) {
                if (!isset($in[$v])) {
                    die();
                }
            } else {
                if (!isset($in[$k])) {
                    return $v;
                }
            }
        }

        foreach ($config['number'] as $v) {
            if (isset($in[$v]) && !is_numeric($in[$v])) {

                $http->logHack($v . '=' . $in[$v]);
                die();
            }
        }

		foreach ($config['numbers'] as $v) {
			if (isset($in[$v])) {
				$temp = explode(',', $in[$v]);
				foreach($temp as $v2){
					if ( !is_numeric($v2) ) {

						$http->logHack($v . '=' . $in[$v]);
						die();
					}
				}
			}
		}

        $in += $config['default'];

		return $in;
	}

	/**
	 * 增强型查询函数，返回二维参数：
	 *
	 * @param array $condition:条件,可能包含不同的参数
	 *                        部分特殊变量说明如下
	 * orderby：排序，默认为空
	 * asc: 是否正序,默认为true或1
	 * pagesize：页宽，默认为10
	 * 0表示不限，将返回全部数据，即相当于所有的$result['data']
	 * 负数时，返回结果中的-pagesize的数据，即相当于的$result['data']的最新的-pagesize条
	 * 正数时，返回page页数据及返回数据，其中 nav 翻页数据，data 数据
	 * page：当前页，仅当pagesize>0时才有用
	 * pagecount：最多可查询多少页数据，如不传入，默认可查询全部
	 * @param array $in: $condition中，使用in()作为比较符的字段名列表，分别和form及数据表的字段对应
	 * @param array $like：同理，只是和like作为比较符
	 * @param array $q：q关键字要检查的字段名列表，均使用like为比较符，字段间用or作为处理
	 * @param array $option：额外参数，便于扩充
	 * term：额外的where条件，数组形式，每个元素是一个条件，默认为空。
	 * 如果是单个条件，也可以只是一个字符串
	 * join：条件间关系，默认为and
	 * field：要返回的字段，默认为*
	 * groupby：分组条件，默认为空
	 * table：从哪个表取？默认为空，会自动从table获得，但在join table时，可以通过本参数传入
	 * bigPage: 数字,如有则为大页码模式,当查询大数据表时,本模式可优化查询效率.
	 *                     本模式下,orderby值为必填. 本字段在数据表中各值必须唯一,否则不准确
	 * 返回值
	 * {nav: 翻页数据, data: 数据}
	 *
	 * 注:和search()有细节区别
	 */
	function exSearch($condition, $in = [], $like = [], $q = [], $option = [] )
	{
		$option += [ 'term' => [], 'join' => 'and', 'groupby' => '' , 'field' => '*',
					 'table' => $this->table,
		];

		$term = $option[ 'term' ];
		$term = $this->to_array( $term );

		$__ = load('__');

		$__->each($in, function($v) use($term, $condition) {
			if( value( $condition, $v ) != '' )
			{
				$term[] = sprintf( '%s in(%s)', $v, $condition[$v]);
			}
		});

		$__->each($like, function($v) use($term, $condition) {
			if( value( $condition, $v ) != '' )
			{
				$term[] = sprintf( "%s like '%%%s%%'", $v, $condition[$v]);
			}
		});

		if( value($condition, 'q') != '' && count( $q ) > 0 )
		{
			$s = [];
			foreach ($q as $v)
			{
				$s[] = "$v like '%" . $condition['q'] . "%'";
			}
			$term[] = implode( ' or ', $s );
		}

		// sql结构,便于构造sql语句
		$sql = [
			'table' => $option[ 'table' ],
			'where' => $this->db->term($term, $option[ 'join' ]),
			'groupby' => $option[ 'groupby' ],
			'orderby' => value($condition, 'orderby'),
			'asc' => value($condition, 'asc', true),
		];

		$result = [];
		$pageSize = int_value( $condition, 'pagesize', 10);
		if ($pageSize < 0) {
			$sql['limit'] = -$pageSize;
		} else {
			if (!isset($option['not_count'])) {
				if ($option['groupby'] != '') {
					$base = biz('base');
					$base->table
						= "(select  distinct({$option[ 'groupby' ]}) $count ) as t";
					$total = $base->count(array(), $option['second']);
				} else {
					$total = $this->db->run($sql, ['field' => 'count( 1 ) as total']);
                    $total = int_value( $total, '0.total', 0);
				}
			}else{
				$total = 20000;
			}

			$pagecount = ceil( $total / $pageSize );
			if( isset($condition['pagecount' ] ) && $pagecount > $condition['pagecount' ] )
			{
				$pagecount = $condition['pagecount' ];
			}

			$page = int_value( $condition, 'page', 1 );
			if( $page > $pagecount )
			{
				$page = $pagecount;
			}

			$result = [];
			$result['nav'] = [ 'total' => $total,	// 总记录数
							   'pagesize' => $pageSize,	// 页宽
							   'page' => $page,			// 当前页
							   'pagecount' => $pagecount	// 总页码
			];

			if( $total == 0 )
			{
				$result['data'] = [];
			}
			else
			{
				$bigPage = int_value( $option, 'bigPage');
				if ($bigPage > 1 && $page >= $bigPage) {
					$temp_result = $this->db->run($sql, [
						'field' => $condition['orderby'] . ' as a',
						'limit' => sprintf( '%s, 1', ($page-1) * $pageSize )
					]);

					//20161227 因强制设置total=20000 则暂时判断temp_result
					if(!empty($temp_result)){
						$idWhere = sprintf('%s%s%s',
							$condition['orderby'],
							value($condition, 'asc', true) ? '>=' : '<=',
							$temp_result[0]['a']);
						if($sql['where'] =='')
						{
							$sql['where'] = $idWhere;

						} else{
							$sql['where'] = implode(' and ', [$sql['where'], $idWhere]);
						}
					}else{
						$sql['where'] = ' 1!=1 ';

					}


					$sql['limit'] = $pageSize;
				} else {
					$sql['limit'] = ( ( $page - 1 ) * $pageSize) . ', ' . $pageSize;
				}
			}
		}
		$result['data'] = $this->db->run($sql);

		//修正not count 模式 没有数据时, nav 数据不正确
		if( isset($option['not_count']) && count($result['data']) == 0 ) {
			$result['nav'] = [ 'total' => 0, 'pagesize' => 0, 'page' => 0, 'pagecount' => 0 ];
		}

		return $result;
	}

    public function rds()
    {
        return load('rds')->init()->table( $this->table );
    }
}
