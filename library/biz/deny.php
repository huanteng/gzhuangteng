<?php
require_once dirname( __FILE__ ) . '/base.php';

class deny extends base
{
	function __construct()
	{
		parent::__construct();
		$this->table = __CLASS__;
		$this->field = 'id,uid,ip,start,end,reason';
	}

	/** 检查ip是否允许登录，返回空字符串或出错原因
	 * */
	function check_ip( $ip )
	{
		$info = $this->get1( 'reason', array( 'ip' => $ip, 'end>' . time() ), 'id desc' );
		return value( $info, 'reason' );
	}

	/** 检查uid是否允许登录，返回空字符串或出错原因
	 * */
	function check_uid( $uid )
	{
		$info = $this->get1( 'reason', array( 'uid' => $uid, 'end>' . time() ), 'id desc' );
		return value( $info, 'reason' );
	}

	function add_ip( $ip, $expire=86400, $reason='' )
	{
		$start = time();
		$end = $start + $expire;
		$this->add( ['ip'=>$ip, 'start'=>$start, 'end'=>$end, 'reason'=>$reason] );

		$this->warning_log('封ip: ' . $ip);
		return true;
	}
}
