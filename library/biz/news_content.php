<?php
require_once dirname( __FILE__ ) .'/base.php';

class news_content extends base
{
	function __construct()
	{
		parent::__construct();
		$this->table = __CLASS__;
		$this->field = 'id,content';
	}
}
?>