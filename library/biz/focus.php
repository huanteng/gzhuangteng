<?php
require_once dirname( __FILE__ ) . '/base.php';

class focus extends base
{
	function __construct()
	{
		parent::__construct();
		$this->table = __CLASS__;
		$this->field = 'id,time,title,content,url,path,filename,filetype,type,remark';
		$this->time_field = 'time';
	}
}
?>