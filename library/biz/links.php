<?php
require_once dirname( __FILE__ ) . '/base.php';

class links extends base
{
	function __construct()
	{
		parent::__construct();
		$this->table = __CLASS__;
		$this->field = 'id,time,title,url,logo,contact,type,status';
		$this->time_field = 'time';
	}
	function status_dict()
	{
		return array(0=>'关闭',1=>'正常');
	}
}
?>