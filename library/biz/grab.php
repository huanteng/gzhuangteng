<?php
require_once dirname( __FILE__ ) . '/base.php';

class grab extends base
{
	function save_album( $in = array('url','title','note','type'=>0) )
	{
		$in['title'] = trim($in['title']);
		$in['note'] = trim($in['note']);
		if( $in['url'] != '' && $in['title'] != '' && is_numeric($in['type']) )
		{
			$album = biz('album');
			$temp = $album->get1( 'id', array('title'=>$in['title']) );

			if( isset($temp['id']) ) 
				return false;
			else 
				return $album->add( array('title'=>$in['title'],'from'=>$in['url'], 'type'=>$in['type'], 'note'=>$in['note']) );
		}
	}
	
	function save_pic( $in = array('url','title','note','album_id'=>0,'top'=>0) )
	{
		$base = '../album/';
		$path = date('Y') . '/' . date('m') . '/' . date('d') . '/' . date('h') . '/';
		$dir = $base . $path;
		if (!is_dir($dir))
			mkdir($dir, 0755, true);

		$array = array('jpg', 'gif', 'png', 'jpeg');
		$img = strtolower($in['url']);
		$type = substr($img, strrpos($img, '.') + 1);

		if (!in_array($type, $array))
			return false;
		
		$uniqid = uniqid();
		$filename = $uniqid. '.' . $type;
		$smallname = $uniqid. '_s.' . $type;

		$http = load('http');
		$c = $http->get_or_post($in['url']);

		if ($handle = fopen($dir . $filename, 'a')) {
			if (fwrite($handle, $c) == TRUE) {

				echo $dir . $filename . PHP_EOL;

				$http->resize_image($dir . $filename, 135, 175, 1, $dir . $smallname );
				
				$album_id = isset( $in['album_id'] ) && is_numeric( $in['album_id'] ) ? $in['album_id'] : 0;
				$this->db->add('picture', array('time'=>time(), 'path'=>$path, 'filename'=>$uniqid, 'filetype'=>$type, 'album_id'=>$album_id, 'title'=>$in['title'], 'note'=>$in['note'], 'top'=>$in['top']));
			}
		}
		fclose($handle);	
		
	}
	
	/*
	 * 网易
	 * 致命诱惑
	 */
	function netease()
	{
		$url = "http://sports.163.com/special/00051K89/tybbtpklm.html";
		$http = load('http');
		$c = $http->get_or_post($url);
		
		preg_match_all( '%http://sports.163.com/photoview/.*.html%', $c, $match);
		
		if( isset($match[0]) )
		{
			$result = array_unique($match[0]);
			
			foreach($result as $url)
			{
				$this->netease_album($url, 3);
			}	
		}
	}
	
	function netease_album($url, $type=1)
	{
		$http = load('http');
		$c = $http->get_or_post($url);
		preg_match_all( '/<textarea name="gallery-data" style="display:none;">(.*?)<\/textarea>/is', $c, $match);
		if( isset($match[1][0]) )
		{
			$m = mb_convert_encoding ( $match[1][0], 'UTF-8', 'GBK' );
			
			$pic_data = json_decode($m, true);

			$title = isset($pic_data['info']['setname']) ? $pic_data['info']['setname'] : '';
			
			if( isset($pic_data['list']) && is_array($pic_data['list']) )
			{				
				$album = biz('album');
				$temp = $album->get1( 'id', array('title'=>$title) );
				if( isset($temp['id']) ) 
					return false;
				else 
					$album_id = $album->add( array('title'=>$title,'from'=>$url, 'type'=>$type) );

				foreach( $pic_data['list'] as $k => $v )
				{
					$top = $k==0 ? 1 : 0;
					$this->save_pic( array('url'=>$v['img'], 'title'=>$v['title'], 'note'=>$v['note'], 'album_id'=>$album_id, 'top'=>$top) );
				}
			}
		}
	}
	/*
	 * 网易 结束
	 */
	
	/*
	 * 7m 足球宝贝,篮球宝贝
	 */
	function seven_m()
	{
		//"http://photo.7m.cn/newlist/lace_s/index.shtml","http://photo.7m.cn/newlist/lace_b/index.shtml"
		$http = load('http');
		
		foreach( array("http://photo.7m.cn/newlist/lace_s/index.shtml","http://photo.7m.cn/newlist/lace_b/index.shtml") as $key => $url )
		{
			$type = $key + 1;
			
			$c = $http->get_or_post($url);
			preg_match_all( '/<div class="c_list_box">(.*?)<div class="bottom">/is', $c, $match);

			if( isset($match[1][0]) )
			{
				preg_match_all( '/<a href="(.*?)" target="_blank">(.*?)<\/a>/is', $match[1][0], $a);
				//http://photo.7m.cn/news/20130724/13622.shtml
				//print_r($a);
				foreach($a[0] as $k=>$v)
				{
					preg_match('/href="(.*?)".*alt="(.*?)"/', $v, $m);
					if( isset($m[1]) && isset($m[2]) )
					{
						$this->seven_m_album('http://photo.7m.cn'.$m[1], $type, $m[2]);
					}
				}
			}
		}
	}
	
	function seven_m_album( $url, $type=1, $title='' )
	{
		$album = biz('album');
		
		if( $title != '' ) $temp = $album->get1( 'id', array('title'=>$title) );
		else $temp = $album->get1( 'id', array('from'=>$url) );
		
		if( isset($temp['id']) ) 
			return false;
		else 
		{
			$http = load('http');
			$c = $http->get_or_post($url);

			preg_match_all( '/var content = \'(.*)\'/', $c, $match);
			$note = isset($match[1][0]) ? trim(strip_tags($match[1][0])) : '';

			preg_match_all( '/var imgArr = (.*)/', $c, $match);
			//print_r($match);
			if( isset($match[1][0]) )
			{
				$data = json_decode($match[1][0], true);
				//print_r($data);
				$temp = $album->get1( 'id', array('title'=>$title) );
				if( isset($temp['id']) ) 
					return false;
				else 				
					$album_id = $album->add( array('title'=>$title, 'from'=>$url, 'type'=>$type, 'note'=>$note) );
				
				foreach( $data as $k => $v)
				{
					$top = $k==0 ? 1 : 0;
					$this->save_pic( array('url'=>'http://photo.7m.cn'.$v['file_path'], 'title'=>trim($v['ptitle']), 'note'=>trim(strip_tags($v['text'])), 'album_id'=>$album_id, 'top'=>$top) );
				}
			}
		}
	}
	/*
	 * 7m 足球宝贝 结束
	 */
	 
	/*
	 * sina
	 */ 
	function sina()
	{
		$url = 'http://slide.eladies.sina.com.cn/index.php?page=1&dpc=1';
		$http = load('http');
		$album = biz('album');
		$c = $http->get_or_post($url);
				
		preg_match_all( '%<td><a href="(http://slide.eladies.sina.com.cn/news/slide_.+.html)"%', $c, $match);
		
		if( isset($match[1]) ){
			
			foreach($match[1] as $u)
			{
				$temp = $album->get1( 'id', array('from'=>$u) );
				if( !isset($temp['id']) ) $this->sina_album($u, 4);
			}
		}
	}
	
	function sina_album($url, $type=4)
	{
		/* 测试例子
		$url = 'http://slide.eladies.sina.com.cn/news/slide_3_54171_21888.html';
		*/
		if( strpos($url, 'slide') > 0 )
		{
			$http = load('http');
			$c = $http->get_or_post($url);
			$c = mb_convert_encoding ( $c, 'UTF-8', 'GBK' );
			preg_match( '%<meta name="Description" content="(.+)" />%', $c, $t);
			$title = isset($t[1]) && !empty($t[1]) ? $t[1] : '';
			
			preg_match_all( '/<div id="eData" style="display:none;">(.*)<div class="footer">/is', $c, $match);

			if( isset($match[1][0]) )
			{
				preg_match_all( '/<dl>(.*?)<\/dl>/is', $match[1][0], $dl);
				if( $dl[1] )
				{
					$album_id = 0;
					foreach($dl[1] as $key => $d)
					{
						preg_match_all( '/<dt>(.*?)<\/dt>|<dd>(.*?)<\/dd>/is', $d, $dd);
						
						if( isset($dd[1][0]) && isset($dd[2][1]) )
						{
							$sub_title = $dd[1][0];
							$pic_url = $dd[2][1];
							$note = $dd[2][5];

							if( $key == 0 ) $album_id =	$this->save_album( array('title'=>$title, 'url'=>$url, 'note'=>$note, 'type'=>$type) );
							if( $album_id != 0 )
							{
								$top = $key==0 ? 1 : 0;
								$this->save_pic( array('url'=>$pic_url, 'title'=>trim($sub_title), 'note'=>trim(strip_tags($note)), 'album_id'=>$album_id, 'top'=>$top) );
							}
						}
					}
				}
			}
		}
	}
	/*
	 * sina 结束
	 */
}
?>