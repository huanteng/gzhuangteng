<?php
require_once dirname( __FILE__ ) . '/function.php';

class controller
{
	var $error = [];
	var $input = [];
	var $output_handler = 'ob_gzhandler';
	var $content_type = 'content-type: text/html; charset=utf-8';
	var $content = '';	// 要输出的内容

	function __construct()
	{
		if( !isset( $GLOBALS[ 'mvc_init' ] ) )
		{
			$GLOBALS[ 'mvc_init' ] = true;

			if( !config( 'develop' ) )
			{
				error_reporting (0);
				set_error_handler( array( & $this, 'mark' ) );
			}

			register_shutdown_function( array( & $this, '_finish' ) );
			mb_internal_encoding( 'UTF-8' );
			date_default_timezone_set('Asia/Shanghai');

			// 保证$_POST等为未转义的原文，如果要在数据库中使用，要编程防注入，而->input是已经防注入的，可在数据库中使用
            // 防注入，子函数中可安全使用。而$_POST等未防注入，在数据库中使用时，要手工写好防注入
            // 20161227,放弃->input概念,观察一个月
            //$this->input = load('http')->clean( array_merge( $_POST , config( 'get' ) ) );

			if ( $this->output_handler != '' ) ob_start( $this->output_handler );
			if ( $this->content_type != '' ) header( $this->content_type );

		}
	}

	function run( $in )
	{
        $url = config('url');

		$method = $url[ 'method' ];
		if( !method_exists( $this, $method ) )
		{
            go_404();
		}

		if( config( 'develop' ) )
		{
			$cachetime = 0;
		}
		else {
			$cachetime = value($this->cache_rule(), $method, 0);
		}

		$in = $this->clean( $in );
		$in = $this->xss_clean( $in );

        $this->content = $cachetime > 0 ?
            load('box')->call_or_cache($this, $method,
                [$cachetime, $in],
                [config('dir.base' ), $url[ 'language' ], $url[ 'module' ] ]) :
            $this->content = $this->$method( $in );
	}

	/*
	 * ajax请求时的输出
	 * 三种调用方式，
	 * 方式一：3 参数
	 * 	code：代码，一般来说，正数表示正确，负数代表不同的出错信息
	 * 	memo：提示信息，字符串
	 * 	data：数组形式的数据
	 *
	 * 方式二：2 参数
	 * 	code
	 * 	memo
	 *
	 * 方式三：1参数，此时认作code=1
	 * 	memo或data
	 */
	function ajax_out( $code, $memo = '', $data = array() )
	{
		$out = [];

		if( is_array( $code ) )
		{
			$out = array( 'code' => 1, 'data' => $code );
		}
		elseif( is_string( $code ) )
		{
			$out = array( 'code' => 1, 'memo' => $code );
		}
		else
		{
			$out = array( 'code' => $code, 'memo' => $memo );

			if( count( $data ) > 0 )
			{
				$out[ 'data' ] = $data;
			}
		}

		return json_encode( $out );
	}

	function mark( $no, $str, $file, $line )
	{
		$EXIT = FALSE;

		switch ( $no ) {
			//提醒级别
			case E_NOTICE:
			case E_USER_NOTICE:
				$error_type = 'Notice';
				break;
			//警告级别
			case E_WARNING:
			case E_USER_WARNING:
				$error_type = 'Warning';
				break;

			//错误级别
			case E_ERROR:
			case E_USER_ERROR:
				$error_type = 'Fatal Error';
				$EXIT = TRUE;
				break;

			//其他未知错误
			default:
				$error_type = 'Unknown';
				$EXIT = TRUE;
				break;
		}
		$content = "{$error_type}：$file, line=$line, no=$no,<br>$str<br><br>";
		if( config( 'develop' ) )
		{
			echo $content;
		}
		else
		{
			if( $EXIT )
			{
				//因为重复插入的问题,产生过多的LOG.暂时先过滤掉
				if ($no == 256 && strpos($content, 'PRIMARY') !== false && (strpos($content, 'tips_archive') !== false || strpos($content, 'vs_archive') !== false || strpos($content, 'vs') !== false)) {
					biz('base')->warning_log('插入主键重复',0,$content);
				} else {
					biz('base')->error($content);
				}

				die;
			}
		}
	}

	function _finish()
	{
		// 有些错误要在此捕捉
		$error = error_get_last();

		if ( $error !== NULL )
		{
			$this->mark( $error['type'], $error['message'], $error['file'], $error['line'] );
		}

		echo $this->content;
	}

	// 初始化cli模式
	static function initCli()
	{
		global $argv;

		if ($argv)
		{
			// 上调内存变量。一些复杂程序如：对阵计算，需要用到很多内存变量
			ini_set('memory_limit','1280M');

			if( isset( $argv[ 1 ] ) )
			{
				// 第一个参数为文件名及参数，以"/"开始
				$_SERVER["REQUEST_URI"] = $argv[ 1 ];
				$_SERVER['HTTP_HOST'] = '';
				$_SERVER['REMOTE_ADDR'] = '.';
			}
		}
	}

    /** 分析url，返回相关结构体
     * @param string $url ：要分析的url，默认值为$_SERVER["REDIRECT_URL"]
     * @param string $root ：统一的根，默认为 "/"
     * @return array：结构如下：
     *    language：语言，sc|hk|en，默认为hk
     *    module：模块名，默认为index
     *    uri：其余的字符串，便于后续分析具体参数对，并得到类似_GET的结构
     */
    static function getModuleFromUrl($url)
    {
		global $SITE;

        $dir = config('dir');

        $out = [
            'root'     => '/',
            'language' => 'hk',
            'module'   => 'index',
            'uri'      => '',
        ];

		//后台不作-号处理转换
		if($SITE['path']!='back') {
			$url = str_replace('-', '/', $url);//转换-
		}

        // 前两个有可能是语言标识:hk/sc/en
        $language = substr($url, 1, 3);
        if ($language == 'sc/' || $language == 'en/') {
            $out['language'] = substr($language, 0, 2);
            $url = substr($url, 4);
        } else {
            $url = substr($url, 1);
        }

        $begin = 0;
        $len = strlen($url);
        while ($begin <= $len) {
            $pos = strpos($url, '?', $begin);

            if ($pos === false) {
                $url1 = $url;
                $url2 = '';
            } else {
                // 以 ? 为分隔,分为前后
                $url1 = substr($url, 0, $pos);
                $url2 = substr($url, $pos); // 含?
            }

            $pos = strpos($url1, '/', $begin);

            if ($pos === false) {
                $pos = $len + 1;
            }

            $module = substr($url1, $begin, $pos - $begin);
            $begin = $pos + 1;

            requireIfExists($dir['module'] . $dir['base'] . 'base.php');

            if ($module != '') {
                $filename = $dir['module'] . $dir['base'] . $module;
                if (is_file($filename)) { // 存在文件
                    $out['module'] = substr($module, 0, -4);
                    break;
                } else if (is_file($filename . '.php')) { // 存在对应 .php
                    $out['module'] = $module;
                    break;
                }
                elseif (is_dir($filename)) { // 存在目录
                    $dir['base'] .= $module . '/';
                    config('dir', $dir);
                } else {
                    // module 不存在，部分是因友好化url，其余的才是真实不存在
                    if ($dir['base'] == '')
                    {
                        global $SITE;
                        $isFound = false;
                        foreach( value( $SITE, 'url_route', [] ) as $k2 => $v2 )
                        {
                            // 为了规则简写，如果 t123 由 tips.php处理，那 t123/... 也由tips.php处理
                            if( preg_match( '/' . $k2 . '\/?/', $module, $temp, PREG_OFFSET_CAPTURE) === 1)
                            {
                                $out['module'] = $v2;
                                $isFound = true;
                                break;
                            }
                        }

                        if ($isFound) {
                            $begin = 0;
                            $len = -1;
                            break;
                        }else{
							//20170310 找不到则跳出
							go_404();
						}
                    } else {
                        go_404();
                    }
                }
            }
        }

        $out['uri'] = substr($url1, $begin) . $url2;
        
        return $out;
    }

    static function getAction($url)
    {
        $dir = config('dir');

        require $dir['module'] . $dir['base'] . $url['module'] . '.php';
        return new action();
    }

    // 检查ip是否被封
    static function checkDenyIp()
    {
		global $SITE;

		//后台不封ip
		if($SITE['path']=='back') {
			return;
		}

        $ip = load('http')->get_ip();
        if( biz('deny')->exists( ['uid' => 0, 'ip' => $ip, 'end>' . time() ] ) )
        {
            die;
        }
    }
}
