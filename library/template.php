<?php
class template
{
	var $data = array();
	var $path = '';

	function __construct()
	{
		$this->path = config('dir.project') . 'template/';
		$this->appoint( $_GET );
	}

	function assign( $key, $value )
	{
		$this->data[$key] = $value;
	}

	function appoint( $array )
	{
		foreach( $array as $k => $v ) $this->assign( $k, $v );
	}

	function parse( $tpl )
	{
		$_IN = &$this->data;
		extract( $this->data );
		ob_start();
		require $this->path . $tpl ;
		$_reserve_contents_reserve_ = ob_get_contents();
		ob_end_clean();
		return $_reserve_contents_reserve_;
	}

	// 20170109,临时放,如果使用twig就不必要了
	public function box($file, $data)
	{
		$dir = config('dir');
		
		$temp = $dir[ 'module' ] . $dir[ 'base' ] . 'box.php';
		include_once is_file( $temp ) ? $temp :
			$dir[ 'module' ] . 'box.php';

		$action = new action_box();

		if( method_exists( $action, $file ) )
		{
			$data = $action->{$file}( $data );
		}


		return $action->out( $data, 'box_' . $file );
	}
}
?>