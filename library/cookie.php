<?php
class cookie
{
	var $key = '';
	var $data = '';
	var $domain = '';

	function __construct( $data = array() )
	{
		if( empty( $data ) )
		{
			$data = array( 'data' => config( 'cookie' ), 'key' => config( 'key' ), 'domain' => config( 'domain.root' ) );
		}

		$this->data = $data[ 'data' ];
		$this->key = md5( $data[ 'key'] . config( 'server.HTTP_USER_AGENT' ) );
		//$domain = explode( ':', $data[ 'domain'] );
		//$this->domain = $domain[0];
	}

	function get( $name, $code = false )
	{
		$value = isset( $this->data[$name] ) ? ( $code ? $this->code( $this->data[$name], 'decode' ) : $this->data[$name] ) : '';
		return unserialize( $value );
	}

	function set( $name, $value, $code = false, $lifetime = 0, $secure = false, $httponly = true )
	{
		// 20150121，支持数组
		$value = serialize( $value );

		$value = $code ? $this->code( $value ) : $value;

		$this->data[ $name ] = $value;
		return setcookie( $name, $value, $lifetime, '/', $this->domain, $secure, $httponly );
	}

	function del( $name )
	{
		setcookie( $name, '', -1, '/', $this->domain );
	}

	function code( $text, $operate = 'encode' )
	{
		$data = '';
		$key = $this->key;
		$key_length = strlen( $key );
		$text = $operate == 'decode' ? base64_decode( $text ) : $text;
		$text_length = strlen( $text );
		for( $i = 0; $i < $text_length; $i += $key_length ) $data .= substr( $text, $i, $key_length ) ^ $key;
		$data = $operate == 'encode' ? str_replace( '=', '', base64_encode( $data ) ) : $data;
		return $data;
	}
}
?>