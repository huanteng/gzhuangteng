<?php
function biz( $name, $temp = false )
{
	return load( 'biz.' . $name, [], ['gray'=>$temp] );
}

/** 根据域名load配置值
 * @param $file：名字名
 * @return array
 */
function load_config( $file )
{
	$file .= '.php';

	global $SITE;
	$host = value( $_SERVER, 'HTTP_HOST', '.' );

	$temp = pathinfo( dirname( __FILE__ ) . '/' );
	$temp = $temp['dirname'] . '/';
	$config_dir = $temp . $SITE['path'] . '/config/';

    $www = $temp . 'www/config/' . $file;
    $root = $config_dir . $file;
    $sub = $config_dir . $host . '/' . $file;

    return
        ( is_file( $sub ) ? require($sub) : [] ) +
        ( is_file( $root ) ? require($root) : [] ) +
        ( $SITE['path'] == 'www' ? [] : require($www) );
}

/** 获取/设置设置值
 * @param string $name: 名字,用半角逗号如果是子命名,如空则返回整个配置值
 * @param null   $value:设置值时有用
 *
 * @return array|string|void
 */
function config( $name = '', $value = null )
{
	static $data = [];

	if( $value !== null )
	{
        $search = &$data;
        foreach (explode('.', $name) as $v){
            $search = &$search[$v];
        }
		$search = $value;
		return;
	}

	if( empty( $data ) )
	{
        global $SITE;
        $data = array(
			'template' => [],
			'domain' => [],
			'header' => [],
			'dir' => [],
			'cache' => [],
			'host' => $_SERVER['HTTP_HOST'],
		);

		//base config
		$data['server'] = & $_SERVER;
		$data['cookie'] = & $_COOKIE;
		$data['get'] = & $_GET;
		$data[ 'request' ] = & $_REQUEST;

		// 规范目录
		$root = pathinfo( dirname( __FILE__ ) . '/' );
        $root = $root['dirname'] . '/';

        $data['dir'] = [
            'root' => $root,
            'project' => $root . $SITE['path'] . '/',
            'module' => $root . $SITE['path'] . '/module/',
            'www' => $root . $SITE['path'] . '/web/',
			'cache' => $root. 'cache/' . $SITE['path'] . '/',
			'library' => $root . 'library/',
			'js' => $root . $SITE['path'] . '/web/js/',
            'base' => ''
		];

        $data = array_merge($data, load_config( 'config' ));
	}

	return $name == '' ? $data : value( $data, $name, '' );
}

function load( $class, $config = array(), $other = [] )
{
	$other += [ 'unique'=>true, 'gray'=>false ];

	static $object = array();

	if ( ! isset( $object[$class] ) || ! $other['unique'] )
	{
		//20160503 引入灰度发布概念, 例如 biz('tips', true), gray 为true 时, 载入
		$forward = $other['gray'] ? '/temp_' : '/' ;
		require_once config( 'dir.library' ) . str_replace( '.', $forward, $class ) . '.php';
		$name = explode( '.', $class );
		$size = count( $name );
		$name = $name[$size-1];

		$temp = new $name( $config );

		if ( $name != 'template' ) $object[$class] = & $temp;
	}
	else
	{
		$temp = $object[$class];
	}

	if ( ! is_object( $temp ) ) {
		trigger_error( 'can not load' . $class . ' object.', E_USER_ERROR );
	}
	return $temp;
}

//检查 $key是否存在，如存在则返回，否则返回$default
// 注：key如果包含"."，则表示取子元素
function value($data, $key, $default = '')
{
	if( isset( $data[ $key ] ) )
	{
		return $data[ $key ];
	}

	$temp = explode( '.', $key );

	foreach( $temp as $v )
	{
		if( isset( $data[ $v ] ) )
		{
			$data = $data[ $v ];
		}
		else
		{
			$data = $default;
			break;
		}
	}
	return $data;
}

/** 确保$data[$key]为数字，常用于前台防SQL注入。（注，函数名写整数，便于使用）
 * @param $data：记录集
 * @param $key：要检查的元素名
 * @param int $default：如果不存在，或非整数，用什么值代替
 * @param bool $is_die：如果非整数，是否中止
 * @return int
 */
function int_value( $data, $key, $default = 0, $is_die = true )
{
	$out = value( $data, $key, $default );

	if( !is_numeric( $out ) )
	{
		if( $is_die )
		{
			die('error 166');
		}
		else
		{
			$out = $default;
		}
	}

	return intval($out);
}

function timetostr( $time )
{
	return date( 'Y-m-d H:i:s', $time );
}

/** 处理box机制
 * @param $file：文件名，不必带后辍名
 * @param array $data：处理函数中所需用到的变量，可选
 * @param array $option：box处理所需的参数处理，可选，特殊参数如下：
 * 	cachetime：缓存时间，0表示不缓存，默认值为300，单位为秒。（注：当url带有参数cache=new时，所有box都不缓存）
 *	注：当传入的option不是数组时，表示本值为cachetime。这是简写用法
 *
 */
function box( $file, $data = [], $option = [] )
{
	global $SITE;
	$url = config('url');

	if( $data == '' )
		$data = [];

	if( !is_array( $option ) && is_integer( $option ) )
	{
		$option = [ 'cachetime' => $option ];
	}
	$cachetime = value( $option, 'cachetime', 300 );
    
    $template = load('template');

    echo $cachetime == 0 ?
        $template->box($file, $data) :
        load('box')->call_or_cache($template, 'box', [
            $cachetime, $file, $data
        ], [$url[ 'language' ], $SITE['path']]);
}

/** 带参数时，输出参数并中止，不带参数时，输出sql语句，便于调试
 * @param $s
 */
function d( $s = '' )
{
	if( $s != '' )
	{
		echo '<pre>';
		print_r( $s );
		echo '</pre>';
		die;
	}
	load('db')->debug();
}

/** 返回可访问的地址，便于整体切换或更新
 * @param $url：文件地址，不含域名；
 * @return string：完整地址
 */
function url( $url )
{
    // 使用本地文件时，请激活本行代码，并忽略版本号
    //return $url;

    if (substr($url, 0, 1) != '/') {
        $path = $_SERVER['REQUEST_URI'];
        $path = pathinfo($path);

        if ($path['dirname'] != '/') {
            $url = substr($path['dirname'], 1) . '/' . $url;
        }
    } else {
        $url = substr($url, 1);
    }

    return biz('url')->get_url($url);
}

/** 分析友好url，返回相关结构体
 * @param string $url：要分析的url，默认值为$_SERVER["REDIRECT_URL"]
 * @param string $querystring：要分析url的参数，默认值为$_SERVER["QUERY_STRING"]
 * @param string $root：统一的根，默认为 "/"
 * @return array：结构如下：
 * 	language：语言，sc|hk|en，默认为hk
 * 	module：模块名，默认为index
 * 	method：方法，默认为home
 * 	get：其它的参数，类似_GET
 */
function parse_url1( $url = '', $querystring = '', $root = '/' )
{
	if( $querystring == '' )
	{
		$querystring = $_SERVER["QUERY_STRING"];
	}
	parse_str( $querystring, $get );

	if( $url == '' )
	{
		$url = $_SERVER["REDIRECT_URL"];
	}
	$url = explode( '?', $url );
	$url = substr( $url[ 0 ], strlen( $root ) );
	$url = explode( '/', $url );

	// url长度
	$len = count( $url );

	// 参数开始序号
	$start = 0;

	// 语言、对象、方法
	$language = '';
	$module = '';
	$method = '';

	if( $len )
	{
		$language = $url[ 0 ];
		if( $language == 'sc' || $language == 'en' )
		{
			++$start;
		}
		else
		{
			// 第一个不是语言标识，交后面处理为默认值
			$language = '';
		}

		if( isset( $url[ $start ] ) )
		{
			$module = $url[ $start++ ];

			// 兼容旧地址，形式为 xxx.php
			if( substr( $module, -4 ) == '.php' )
			{
				$module = substr( $module, 0, -4 );
			}

			if( isset( $url[ $start ] ) )
			{
				$method = $url[ $start++ ];
			}
		}
	}

	for( $i = $start; $i < $len; $i += 2 )
	{
		$get[ $url[ $i ] ] = value( $url, $i + 1, '' );
	}

	if( $language == '' ) $language = 'hk';
	if( $module == '' ) $module = 'index';
	if( $method == '' )
	{
		$method = value( $get, 'method', 'home' );
	}

	return array(
		'root' => $root,
		'language' => $language,
		'module' => $module,
		'method' => $method,
		'get' => $get
	);
}

/** 修改链接，以便于适应多语言版本
 * @param $u
 * @return string
 */
function href( $u )
{
	$url = config('url');
	$dir = config('dir');
	$out = $url[ 'root' ];
	if( $url[ 'language' ] != 'hk' )
	{
		$out .= $url[ 'language' ] . '/';
	}
	if( substr( $u, 0, 1 ) != '/' )
	{
		$u = $dir[ 'base' ] . $u;
	}
	else
	{
		$u = substr( $u, 1 );
	}
	return $out . $u;
}

/** 用户链接
 * @param $uid
 * @return string
 */
function uid_href( $uid, $sport )
{
	return href( 'user/' . $uid . '/' . $sport );
}

/** 显示404页面
 * @param bool $log：是否记录日志？
 */
function go_404()
{
	global $action, $SITE;

	header('HTTP/1.1 404 Not Found');
	header('status: 404 Not Found');

	$referer = value($_SERVER, 'HTTP_REFERER', '');
	$ip = load('http')->get_ip();

	$redis = load('redisDB');
	$ip_key = 'ip_404_'.$ip;
	$result = $redis->incr( $ip_key );

	if( $result >= 10 && !config('develop')) {
		biz('deny')->add_ip($ip, 86400, '频密访问404');
		biz('base')->warning_log($ip . ' 恶意访问，已封一天');
		$redis->del($ip_key);

		die('404 die');
	}

	$redis->expire( $ip_key, 60 );

	//write log
	biz('log')->log( '404', 0, $_SERVER['REQUEST_URI'], [ 'data' => [
		'Host' => $_SERVER['HTTP_HOST'],
		'referer' => $referer,
		'ip' => $ip
	]] );

    config('url', [
		'root' => '/',
		'language'=>'hk',
		'module'=>'404',
		'method'=>'home',
	]);

	config('dir', ['base'=>''] + config('dir'));//子目录时,强制变成到根目录找404文件

	if( !$action ) {
		$action = new frontend();
	}

	if( $SITE['path'] == 'www' ){
		echo $action->render();
	} else {
		die('404 die');
	}

	die;
}

/** 301 方式跳转
 * @param $url："http://" 或非 "/" 开始
 * @param string $domain：如 www|news，默认为本站跳转；$domain=http时，表示url是一个外站，直接跳转
 */
function go_301( $url, $domain = '' )
{
	switch( $domain )
	{
		case '':
			$host = $_SERVER['HTTP_HOST'];
			break;

		case 'http':
			$host = '';
			break;

		default:
			$host = $domain . '.' . config( 'site.domain' );
	}

	if( $host != '' )
	{
		$url = 'http://' . $host . '/' . $url;
	}

	header('HTTP/1.1 301 Moved Permanently');
	header('location: ' . $url);
	exit();
}

/** 如果文件存在,则require
 * @param      $file
 * @param bool [$once]:是否once
 */
function requireIfExists($file, $once = true)
{
	if (file_exists($file))
	{
		if ($once) {
			require_once $file;
		} else {
			require $file;
		}
	}
}
