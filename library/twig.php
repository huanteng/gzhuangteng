<?php
require_once config( 'dir.root' ) . 'library/Twig/Autoloader.php';

class twig
{
	var $twig = null;

	function addFunction( $name, $func )
	{
		$function = new Twig_SimpleFunction( $name, $func );
		$this->twig->addFunction($function);
	}

	/**
	 * @param $path：模板目录，相对于网站根目录
	 * @param $filename
	 * @param $data
	 * @return string
	 */
	function render( $path, $filename, $data )
	{
		$dir = config('dir');
		Twig_Autoloader::register();
		$loader = new Twig_Loader_Filesystem( $path );
		$config = array(
			'cache' => $dir['cache'],
			'debug' => false,
			'auto_reload' => false,
		);

		// 产品环境修改模板后，为了即时生效，请修改本值为true
		$debug = config( 'develop' ) || load('cookie')->get( 'cache' ) == 'new';
		if( $debug )
		{
			$config[ 'debug' ] = true;
			$config[ 'auto_reload' ] = true;
		}

		$this->twig = new Twig_Environment($loader, $config);

		$this->addFunction( 'box', function ( $file, $data = [], $option = [] ) {

			$url = config('url');

			if( !is_array( $option ) && is_integer( $option ) )
			{
				$option = [ 'cachetime' => $option ];
			}
			$cachetime = value( $option, 'cachetime', 300 );

            echo $cachetime == 0 ?
				$this->box($file, $data) :
				load('box')->call_or_cache($this, 'box', [
					$cachetime, $file, $data
				], $url[ 'language' ]);
		} );

		$this->addFunction( 'url', 'url' );
		$this->addFunction( 'href', 'href' );
		$this->addFunction( 'uid_href', 'uid_href' );
		$this->addFunction( 'config', 'config' );

		$this->addFunction( 'img_href', function( $u='' ) {
			if( $u == '' ) return '';
			else
			{
				if( stripos($u, '.css') )
				{
					$img_host = config('css');
				}
				elseif( stripos($u, '.js') )
				{
					$img_host = config('js');
				}
				else
				{
					$img_host = config('img');
				}

				return $img_host != '/' && $img_host != '' ? 'http://' . $img_host . '/' . $u : $img_host . $u;
			}
		} );

		$this->addFunction( 'min_url', function($group_name) {
			require_once config('dir.www') . 'min/utils.php';
			$jsUri = Minify_getUri($group_name,array('rewriteWorks'=>false));
			return $jsUri;
		});

		return $this->twig->render( $filename, $data );
	}

    public function box($file, $data)
    {
        include_once config('dir.www').'../module/box.php';

        $action = new action_box();

        if( method_exists( $action, $file ) )
        {
            $data = $action->{$file}( $data );
        }
        else
        {
            $data = [];
        }

        return $action->render( $data, 'box_' . $file );
    }
}
