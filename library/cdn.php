<?php

class cdn
{
	var $key_id = 'nDk5H50PckTNi1Ig';
	var $key_secret = 'vpag4yjenF9CRK5gTJKuvzeqiiPDcj';
	var $api_url = 'http://cdn.aliyuncs.com/';

	function percentEncode( $str )
	{
		// 使用urlencode编码后，将"+","*","%7E"做替换即满足 API规定的编码规范
		$res = urlencode( $str );
		$res = preg_replace( '/\+/' , '%20' , $res );
		$res = preg_replace( '/\*/' , '%2A' , $res );
		$res = preg_replace( '/%7E/' , '~' , $res );
		return $res;
	}

	function computeSignature( $parameters , $accessKeySecret )
	{
		// 将参数Key按字典顺序排序
		ksort( $parameters );

		// 生成规范化请求字符串
		$canonicalizedQueryString = '';
		foreach ( $parameters as $key => $value ) {
			$canonicalizedQueryString .= '&'.$this->percentEncode( $key ).'='.$this->percentEncode( $value );
		}

		// 生成用于计算签名的字符串 stringToSign
		$stringToSign = 'GET&%2F&'.$this->percentencode( substr( $canonicalizedQueryString , 1 ) );

		// 计算签名，注意accessKeySecret后面要加上字符'&'
		$signature = base64_encode( hash_hmac( 'sha1' , $stringToSign , $accessKeySecret.'&' , true ) );
		return $signature;
	}

	function refresh( $url , $type = 'File' )//可选， 刷新的类型， 其值可以为File | Directory
	{
		// 注意使用GMT时间
		date_default_timezone_set( "GMT" );
		$dateTimeFormat = 'Y-m-d\TH:i:s\Z'; // ISO8601规范

		$accessKeyId = $this->key_id; // 这里填写您的Access Key ID
		$accessKeySecret = $this->key_secret; // 这里填写您的Access Key Secret

		$data = array( // 公共参数
			'Format' => 'json' ,
			'Version' => '2014-11-11' ,
			'AccessKeyId' => $accessKeyId ,
			'SignatureVersion' => '1.0' ,
			'SignatureMethod' => 'HMAC-SHA1' ,
			'SignatureNonce' => uniqid() ,
			'TimeStamp' => date( $dateTimeFormat ) ,
			'partner_id' => '1.0' ,
			//函数
			'Action' => 'RefreshObjectCaches' ,
			'ObjectPath' => $url ,
			'ObjectType' => $type
		);

		// 计算签名并把签名结果加入请求参数
		$data[ 'Signature' ] = $this->computeSignature( $data , $accessKeySecret );

		// 发送请求

		$url = $this->api_url.'?'.http_build_query( $data );
		$data = load( 'http' )->get_or_post( $url );
//		echo json_encode($data);
	}
}

?>