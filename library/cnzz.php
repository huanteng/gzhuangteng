<?php
class cnzz
{
	var $token = '9e15893fadba99948d03';

	/** 产生dplus请求
	 * @param $event：事件名
	 * @param $json：参数，使用json
	 * @param string $option：额外，(空字符串)|img|verbose
	 * @return mixed
	 * 参看：http://help.dplus.cnzz.com/?p=119
	 */
	function track( $event, $json, $option = '' )
	{
		$json[ 'token' ] = $this->token;

		$data = array(
			'event_name' => $event,
			'properties' => $json,
		);

		$data = base64_encode( json_encode( $data ) );
		$data = urlencode( $data );

		if( $option == 'img' )
		{
			$data .= '&img=1';
		}
		elseif( $option == 'verbose' )
		{
			$data .= '&verbose=1';
		}

		return load('http')->get( 'http://a.cnzz.com:80/track/?data=' . $data );
	}


}
?>