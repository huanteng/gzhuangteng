<?php

class file
{
	/*
	 * 删除文件
	 */
	function del($filename)
	{
		return is_file($filename) && unlink($filename);
	}

	/** 删除旧文件树
	 * @param $path ：完整目录
	 * @param $days_ago ：删除多少天前？
	 * @param $del_dir ：删除目录？ 如果开启，则$day_ago设置无效
	 * @param $ignore_list ：排除的文件或目录
	 */
	function del_old($path, $days_ago, $del_dir = false, $ignore_list = array())
	{
		$log = biz('log');
		$now = strtotime("-$days_ago days");
		if (is_dir($path)) {
			$handle = opendir($path);
			$time = load('time');
			$time->timeout();
			$time_err_1 = 0;
			while (($file = readdir($handle)) !== false) {//由于某些服务器禁用了 scandir 可使用readdir
				if($time->timeout(120) && $time_err_1++ == 0){
					$log->log( 1, 0, '删除文件超过了120秒.当前处理中文件夹:'.$path);//警告 级
				}
				//else
				{
					if ($file != '.' && $file != '..') {
						$this->del_old($path . '/' . $file, $days_ago, $del_dir, $ignore_list);
					}
				}
			}
			if (!in_array($path, $ignore_list) && $del_dir && $this->dir_is_empty($path)) {
				if(rmdir($path)){
					$log->log( 9, 0, '删除文件夹:'.$path);
				}
			}
		} else {
			if (!in_array($path, $ignore_list) && $now > filemtime($path)) {
				if(unlink($path)){
					$log->log( 9, 0, '删除文件:'.$path);
				}
			}
		}
	}

	/*
	 * 判断文件夹是否为空
	 */
	function dir_is_empty($dir)
	{
		if ($handle = opendir($dir)) {
			while ($item = readdir($handle)) {
				if ($item != '.' && $item != '..') return false;
			}
		}
		return true;
	}


	/**
	 * 读取文本倒数$n行
	 *
	 * @param string $filename
	 * @param int $n
	 * @return array
	 *
	 * 抄自：http://bbs.phpchina.com/thread-64941-1-1.html
	 */
	function tail($filename, $n)
	{
		$fp = fopen($filename, 'rb');
		$n = (int)$n;
		//if($n>10) trigger_error("取文件倒数N行的时候,不能大于10行", E_USER_ERROR);
		if ($fp) {
			flock($fp, LOCK_SH);
			$i = -2; //从第二个字符开始,因为最后一个是"\n"
			fseek($fp, $i, SEEK_END);
			$buffer = array();
			while ($n >= 0) {
				fseek($fp, $i--, SEEK_END);
				if (fgetc($fp) == "\n") {
					$buffer[] = fgets($fp, 1024);
					$n--;
				}
			}
			flock($fp, LOCK_UN);
			fclose($fp);
			unset($tmp);
			return $buffer;
		} else {
			return false;
		}
	}

	/** 删除目录
	 * @param $path ：完整目录
	 * @param $include =true：是否包含目录本身？
	 */
	function del_path($dir, $include = true)
	{
		if (!empty($dir) && is_dir($dir)) {
			$handle = opendir($dir);
			while (($file = readdir($handle)) !== false) {
				if ($file != '.' && $file != '..') {
					$full = $dir . '/' . $file;
					if (is_dir($full)) {
						$this->del_path($full);
					} else {
						unlink($full);
					}
				}
			}
			closedir($handle);
		}

		if ($include) {
			rmdir($dir);
		}
	}
}