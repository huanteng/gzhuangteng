<?php
// 阿里云消息服务 https://help.aliyun.com/document_detail/27513.html
require_once config( 'dir.root' ) . 'library/aliyun/mns-autoloader.php';

use AliyunMNS\Client;
use AliyunMNS\Requests\SendMessageRequest;
use AliyunMNS\Exception\MnsException;


class mns
{
	private $accessId = "CBpAFp9JeOZSqSgR";
	private $accessKey = "40BnYfSUnGo8g7V7AuO63v6CDDD4VN";
	private $endPoint = "https://1458081714207832.mns.cn-hangzhou.aliyuncs.com";
	private $client;
	private $queueName = 'zq999task';

	function __construct()
	{
		$this->client = new Client($this->endPoint, $this->accessId, $this->accessKey);
	}

    /** 获得队列
     * @param $queueName
     *
     * @return \AliyunMNS\Queue
     */
    function get_queue($queueName) {
        if($queueName == '') {
            $queueName = $this->queueName;
        }
        return $this->client->getQueueRef($queueName);
    }

	/** 发送队列消息
	 * @param $queueName:队列名字
	 * @param $message:消息体,字符串或json
	 * @return bool:成功时返回true,否则false
	 *
	 * 调用方式:
	 * send( '队列名', '消息' );
	 * send( '消息' );
	 */
	function send( $queueName, $message = '' )
	{
		if( $message == '' )
		{
			$message = $queueName;
			$queueName = '';
		}

		$queue = $this->get_queue($queueName);
		if( is_array( $message ) )
		{
			$message = json_encode( $message, JSON_UNESCAPED_UNICODE );
		}

		$request = new SendMessageRequest($message);
		try
		{
//			if ($message['type'] == 102) {
				biz('base')->info_log('MNS发送消息:', 0, $message);
//			}
			$queue->sendMessage($request);
			return TRUE;
		}
		catch (MnsException $e)
		{
			biz('base')->error_log( '发送消息失败:' . $message . ',' . $e->getMessage(), 0 );
			return FALSE;
		}
	}

	/** 接收消息
	 *
     *@param string $queueName:队列名,可略
	 * @param bool $is_del:接收后是否删除,可略,默认为true
     *
     * 返回:
     *  如无消息返回NULL;
     *  有消息时,如 is_del == true,则返回消息,可能是json或字符串,
     *          否则,返回两个元素的数组,分别为[$receiptHandle,消息]
	 *
	 * 调用方式:
	 * get( '队列名', [true|false]接收后是否删除 )
	 */
	function get( $queueName = '', $is_del = true )
	{
        $queue = $this->get_queue($queueName);

		try
		{
			$res = $queue->receiveMessage();
            $receiptHandle = $res->getReceiptHandle();
            $message = json_decode($res->getMessageBody(), true);

			if( $is_del )
			{
				$this->del($queueName, $receiptHandle);
            }

            return $is_del ? $message : [$receiptHandle, $message];
		}
		catch (MnsException $e)
		{
			biz('base')->info_log( "ReceiveMessage Failed: " . $e );
			return;
		}
	}

    /** 删除消息
     * @param $queueName
     * @param $receiptHandle
     *
     * @return bool: 是否成功删除
     */
    function del($queueName, $receiptHandle) {
        $queue = $this->get_queue($queueName);
        try
        {
            $queue->deleteMessage($receiptHandle);
            return true;
        }
        catch (MnsException $e)
        {
            biz('base')->error_log( "DeleteMessage Failed: " . $e );
            return false;
        }
    }

}