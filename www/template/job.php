{% extends "base.php" %}

{% block main %}
<div class="a_banner">
	<img src="/images/job_banner_04.jpg" width="1003" height="180" /> </div>
<div class="current">
	<div class="cont">
		<div class="current_l">首页  >>   精英招聘 </div>
		<div class="current_r">
			<ul>
				<li class="submenu"><a href="job.php">精英招聘 </a></li>
			</ul>
		</div>
	</div>
</div>
<div class="mian">
	<div class="cont con_bg">
		<div id="col">
			<dl class="sliderbox" id="slider2">
				<dt class="sliderbox_ti open">手机UI界面设计师</dt>
				<dd>
					<div class="position_job">
						<h3>手机UI界面设计师</h3>
						<div class="nature"><span class="f_style">工作地点：</span><span>广州</span><span class="f_style">工作性质：</span><span>全职</span><span class="f_style">招聘人数：</span><span>3</span></div>
						<h3>工作职责</h3>
						<p>1、对公司手机客户端产品的UI风格进行整体规划设计；</p>
						<p>2、负责公司所有手机客户端产品界面的UI资源设计，包括图形、图标、动画等；</p>
						<p>3、根据客户需求充分发挥创意，设计出简洁、精致的UI界面，提高产品易用性；</p>
						<p>4、负责输出样式坐标文档和技术用图，配合软件开发人员实现产品；</p>
						<p>5、搜集相关产品的潮流设计信息。</p>
						<p>6、完成上级交给的其它工作；</p>
						<h3>职位要求：</h3>
						<p>1、大专以上学历，美术、平面设计等相关专业；</p>
						<p>2、了解手机UI界面的设计流程，有较强的图标界面设计功底，有较强的色彩把握能力</p>
						<p>3、有独到的设计眼光和对流行趋势的判断力，能快速设计出符合市场主流的UI界面</p>
						<p>4、能够熟练运用Photoshop,illustrator,flash等相关设计软件；</p>
						<p>5、建立UI资源库以满足市场及客户的个性化需求</p>
						<p>6、有强烈的责任感和团队精神；务必性格开朗，能够快速融入整个团队</p>
						<p>7、熟悉Android、Iphone等手机平台特性,做过手机界面设计者优先</p>
						<div class="nature"><span class="f_style">职务待遇:</span><span>工资面议（固定底薪+业绩提成）</span></div>
						<p>有意者请将个人简历与作品E-mail至以下邮箱，合则约见</p>
						<p>联系邮箱：<a href="mailto:hr@gzhuanteng.com">hr@gzhuanteng.com</a></p>

					</div>

				</dd>
				<dt class="sliderbox_ti open">运营/推广专员</dt>
				<dd>
					<div class="position_job">
						<h3>运营/推广专员</h3>
						<div class="nature"><span class="f_style">工作地点：</span><span>广州</span><span class="f_style">工作性质：</span><span>全职</span><span class="f_style">招聘人数：</span><span>3</span></div>
						<h3>工作职责</h3>
						<p>1. 负责参与手机客户端产品运营，负责活动推广策划、用户调研等，提出合理产品需求</p>
						<p>2. 数据分析，能够系统地对数据进行解读，并通过数据分析提炼出相关方案</p>
						<p>3. 能独立规划产品运营计划，独立策划相关活动，并最终推进项目实施、完成</p>
						<p>4. 能根据运营状况，不断调整产品运营策略，并推动协助各部门执行</p>
						<h3>职位要求</h3>
						<p>1. 大专或以上学历，对手机应用具有浓厚的兴趣和热情,关注国内外互联网的新应用新趋势，并有自己的理解</p>
						<p>2. 具备2年以上PC客户端或手机客户端运营经验（活动策划经验、产品推广经验）</p>
						<p>3. 具备很强的文档撰写能力和沟通执行能力</p>
						<p>4. 对数据敏感，能从数据中发现问题、解决问题</p>
						<p>5. 熟悉Android电子市场、App Store的商业模式，并有独到的见解</p>
						<div class="nature"><span class="f_style">职务待遇:</span><span>工资面议（固定底薪+业绩提成）</span></div>
						<p>有意者请将个人简历与作品E-mail至以下邮箱，合则约见</p>
						<p>联系邮箱<a href="mailto:hr@gzhuanteng.com">hr@gzhuanteng.com</a></p>


					</div>

				</dd>
				<dt class="sliderbox_ti open">手机UI高级android开发工程师</dt>
				<dd>
					<div class="position_job">
						<h3>android开发工程师</h3>
						<div class="nature"><span class="f_style">工作地点：</span><span>广州</span><span class="f_style">工作性质：</span><span>全职</span><span class="f_style">招聘人数：</span><span>1</span></div>
						<h3>工作职责</h3>
						<p>负责带领开发工程师主导开发app产品</p>
						<h3>职位要求</h3>
						<p>1、熟悉java开发，具有3年以上开发工作经验和良好的编程风格；</p>
						<p>2、至少曾开发2个以上商业app，熟悉常见品牌机型兼容差异及其处理；</p>
						<p>3、熟悉android平台结构，掌握其界面设计、数据存储、多线程、网络通讯；</p>
						<p>4、对移动终端设计架构有深入理解，熟悉移动技术领域标准和规则；</p>
						<p>5、工作认真负责，具有良好的沟通、执行能力和团队合作精神。</p>
						<div class="nature"><span class="f_style">职务待遇:</span><span>工资面议（固定底薪+业绩提成）</span></div>
						<p>有意者请将个人简历与作品E-mail至以下邮箱，合则约见</p>
						<p>联系邮箱<a href="mailto:hr@gzhuanteng.com">hr@gzhuanteng.com</a></p>
					</div>
				</dd>
				<dt class="sliderbox_ti open">高级php开发工程师</dt>
				<dd>
					<div class="position_job">
						<h3>高级php开发工程师</h3>
						<div class="nature"><span class="f_style">工作地点：</span><span>广州</span><span class="f_style">工作性质：</span><span>全职</span><span class="f_style">招聘人数：</span><span>1</span></div>
						<h3>工作职责</h3>
						<p>负责带领开发工程师主导开发、运维web项目</p>
						<h3>职位要求</h3>
						<p>1、3年以上开发工作经验，曾独立主导开发2个以上中型web项目；</p>
						<p>2、熟悉javascript、php等web技术和linux、mysql等相关系统；</p>
						<p>3、达到一定的架构级水平、具有安全防御和处理高并发高性能的能力；</p>
						<p>4、具有mvc等面向对象编程思维、良好的编程风格和规范习惯；</p>
						<p>5、工作认真负责、具有良好的沟通、执行能力和团队合作精神</p>
						<p>6、有体育网站开发经验者优先。</p>
						<div class="nature"><span class="f_style">职务待遇:</span><span>工资面议（固定底薪+业绩提成）</span></div>
						<p>有意者请将个人简历与作品E-mail至以下邮箱，合则约见</p>
						<p>联系邮箱<a href="mailto:hr@gzhuanteng.com">hr@gzhuanteng.com</a></p>

					</div>
				</dd>
				<dt class="sliderbox_ti open">产品经理</dt>
				<dd>
					<div class="position_job">
						<h3>产品经理</h3>
						<div class="nature"><span class="f_style">工作地点：</span><span>广州</span><span class="f_style">工作性质：</span><span>全职</span><span class="f_style">招聘人数：</span><span>3</span></div>
						<h3>工作职责</h3>
						<p>1、负责体育门户平台策划工作，包括网站产品架构体系规划、功能设计、频道规划、UI风格明确等，以及各需求文档的撰写；</p>
						<p>2、负责公司网站的内容管理以及专题功能版块设计；</p>
						<p>3、负责内容产品合作策划与协调公司各部门开发进度管理。</p>
						<p>4、熟悉网站运营方式，了解网络营销，有策划并组织实施网站的推广， 负责监控网站运营状态、分析运营数据、提供决策依据；</p>
						<h3>职位要求</h3>
						<p>1、熟悉互联网行业，熟悉网站的运营和收益模式；</p>
						<p>2、了解体育类型网站者优先；</p>
						<p>3、具有互联网、新闻媒体、网络公司及成功项目策划案者优先；</p>
						<p>4、具有优秀的计划能力、组织能力和执行能力，能够推动整个团队朝预定的目标快速前进；</p>
						<p>5、具有优秀的统筹、策划能力，文字功底强；</p>
						<p>6、具有很强的团队合作精神，能承受一定的工作压力。</p>
						<div class="nature"><span class="f_style">职务待遇:</span><span>工资面议（固定底薪+业绩提成）</span></div>
						<p>有意者请将个人简历与作品E-mail至以下邮箱，合则约见</p>
						<p>联系邮箱<a href="mailto:hr@gzhuanteng.com">hr@gzhuanteng.com</a></p>


					</div>
				</dd>
				<dt class="sliderbox_ti open">网页设计</dt>
				<dd>
					<div class="position_job">
						<h3>网页设计</h3>
						<div class="nature"><span class="f_style">工作地点：</span><span>广州</span><span class="f_style">工作性质：</span><span>全职</span><span class="f_style">招聘人数：</span><span>3</span></div>
						<h3>工作职责</h3>
						<p>1、负责网站整体创意设计，改版，设计更新网站广告；</p>
						<p>2、保质保量完成上级安排的其他相关工作；</p>
						<h3>职位要求</h3>
						<p>1、专科或以上学历，女性，22～30，专业设计工作1年以上经验优先；</p>
						<p>2、网站整体创意、平面美工能力强，对于色彩的搭配有着独到的见解和体会；</p>
						<p>3、熟练掌握Flash、Corldow、dreamweaver等网页设计软件和Illustrator、Photoshop等平面设计软件；</p>
						<p>4、了解web设计新潮流、能熟练使用div+css制作HTML；</p>
						<p>5、富有责任心，注重工作效率，适应灵活性工作调度，工作细致耐心，责任心强，具有强烈的团队合作精神；</p>
						<p>6、有过android前端设计、手机网站/应用开发经验者优先。</p>
						<div class="nature"><span class="f_style">职务待遇:</span><span>工资面议（固定底薪+业绩提成）</span></div>
						<p>有意者请将个人简历与作品E-mail至以下邮箱，合则约见</p>
						<p>联系邮箱<a href="mailto:hr@gzhuanteng.com">hr@gzhuanteng.com</a></p>



					</div>
				</dd>
				<dt class="sliderbox_ti open">技术经理</dt>
				<dd>
					<div class="position_job">
						<h3>技术经理</h3>
						<div class="nature"><span class="f_style">工作地点：</span><span>广州</span><span class="f_style">工作性质：</span><span>全职</span><span class="f_style">招聘人数：</span><span>1</span></div>
						<h3>工作职责</h3>
						<p>1、负责公司产品新产品的研发；</p>
						<p>2、 参与和客户沟通，负责新系统的技术选型、开发及遗留系统的迁移；</p>
						<p>3、负责技术团队建设和管理，注重人才培养和营造创新的研发文化；</p>
						<p>4、 组织研究行业最新产品的技术发展方向；</p>
						<p>5、负责公司项目的客制化开发。</p>
						<h3>职位要求</h3>
						<p>1、学历：本科及以上学位，5年以上互联网工作经验，3年以上项目和团队管理经验；</p>
						<p>2、有丰富的网站项目开发、管理经验和深厚的软件技术功底；</p>
						<p>3、 精通面向对象设计、设计模式和相关开发语言，熟悉各类关系型数据库开发；</p>
						<p>4、精通HTML、XML、css、php、JavaScript、AJAX、MySQL、JQUERY，熟悉Linux系统和Mysql数据库的管理和优化；</p>
						<p>5、 熟悉网站项目的需求分析、产品设计、功能架构、技术架构和质量保障；</p>
						<p>6、有很强的沟通表达能力和团队合作精神、具有创业激情，能承受巨大的工作压力者优先；</p>
						<p>7、 拥有良好的代码习惯，要求结构清晰，命名规范，逻辑性强，代码冗余率低；</p>
						<p>8、对互联网行业充满兴趣和激情，成熟、坚韧、敬业、诚信，能承受较大的工作压力；</p>
						<p>9、有过创业经验或有创业意欲者优先；</p>
						<p>10、有体育行业、移动互联网、网游、外包等方面工作经验者优先。</p>
						<p>11、有过android前端设计、手机网站/应用开发经验者优先</p>
						<div class="nature"><span class="f_style">职务待遇:</span><span>工资面议（固定底薪+业绩提成）</span></div>
						<p>有意者请将个人简历与作品E-mail至以下邮箱，合则约见</p>
						<p>联系邮箱<a href="mailto:hr@gzhuanteng.com">hr@gzhuanteng.com</a></p>


					</div>
				</dd>
				<dt class="sliderbox_ti open">市场主管</dt>
				<dd>
					<div class="position_job">
						<h3>市场主管</h3>
						<div class="nature"><span class="f_style">工作地点：</span><span>广州</span><span class="f_style">工作性质：</span><span>全职</span><span class="f_style">招聘人数：</span><span>1</span></div>
						<h3>工作职责</h3>
						<p>1、负责网络营销，网络推广策略的指定、实施；</p>
						<p>2、运用对外合作、广告合作/投放、资源交换、流量维护等手段，组织实施各种营销活动的在线推广；</p>
						<p>3、负责网站的各种流量推广方案的执行与跟踪，利用各种网络推广方法和互联网资源提高网站的访问量和人气；</p>
						<p>4、采用网络事件营销、网站内容推广合作等多项策略开展网站对外推广合作；</p>
						<p>5、驱动协调各相关部门实施推广，达到预定目标；</p>
						<p>6、通过数据统计和分析，对推广效果进行分析评估，改进推广手段；</p>
						<h3>职位要求</h3>
						<p>1、熟悉多种网络营销方式（邮件营销，SNS、微博营销，论坛营销等），能够根据公司的需求独立策划网站推广方案并执行；</p>
						<p>2、较为丰富的互联网线上推广活动组织经验及执行力（有实战经验）；</p>
						<p>3、有从事过淘宝网店的网络推广经验者优先考虑；</p>
						<p>4、二年以上市场工作经验；</p>
						<p>5、具有良好沟通能力和敬业精神；</p>
						<p>6、具有良好的团队精神。</p>
						<div class="nature"><span class="f_style">职务待遇:</span><span>工资面议（固定底薪+业绩提成）</span></div>
						<p>有意者请将个人简历与作品E-mail至以下邮箱，合则约见</p>
						<p>联系邮箱<a href="mailto:hr@gzhuanteng.com">hr@gzhuanteng.com</a></p>
					</div>
				</dd>
			</dl>
			<script type="text/javascript">
				var slider2=new accordion.slider("slider2");
				slider2.init("slider2",0,"open");
			</script>
		</div>

	</div>

</div>
{% endblock %}