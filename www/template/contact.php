{% extends "base.php" %}

{% block main %}
<div class="a_banner">
	<img src="/images/contact_banner_04.jpg" width="1003" height="180" /></div>
<div class="current">
	<div class="cont">
		<div class="current_l">首页>>联系我们</div>
		<div class="current_r">
			<ul>
				<li class="submenu"><a href="about.php">联系我们</a></li>
			</ul>
		</div>
	</div>
</div>
<div class="mian">
	<div class="cont con_bg">
		<div class="feedback">
			<ul>
				<li>请留下您的需求，我们尽快与您联系。</li>
			</ul>
			<dl>
				<dd>姓名：</dd>
				<dt><input id="username" type="text" class="text_box"/></dt>
			</dl>
			<dl>
				<dd>邮箱：</dd>
				<dt><input id="email" type="text" class="text_box"/></dt>
			</dl>
			<dl>
				<dd>QQ：</dd>
				<dt><input id="qq" type="text" class="text_box"/></dt>
			</dl>
			<dl>
				<dd>电话：</dd>
				<dt><input id="tel" type="text" class="text_box"/></dt>
			</dl>
			<dl>
				<dd>留言内容：</dd>
				<dt><textarea id="content" cols="" rows="" class="text_box1"></textarea></dt>
			</dl>
			<dl>
				<dd>&nbsp;</dd>
				<dt><input type="submit" class="submit" value="提 交" onclick="contact_submit()"/></dt>
			</dl>
		</div>
		<div class="contact">
			<p><span>欢迎您访问欢腾网络并与我们联系</span></p>
			<p>&nbsp;</p>
			<p>广州市欢腾网络科技有限公司</p>
			<p>地址：广州市海珠区新港中路佳信花园C4栋</p>
			<p>邮箱：cs@gzhuanteng.com      </p>
			<p>网址：<a href="http://www.gzhuanteng.com">http://www.gzhuanteng.com</a></p>
		</div>
	</div>

</div>
<script>

	function contact_submit() {

		if ($('#content').val() == '')
		{
			alert('请输入留言内容');
			$('#content').focus();
		}
		else if ($('#email').val() == '')
		{
			alert('请输入邮箱');
			$('#email').focus();
		}
		else
		{
			$.ajax({
				type: "POST",
				url: "/contact/contact_submit",
				dataType: "json",
				data: "content=" + $('#content').val() + "&email=" + $('#email').val()+ "&tel=" + $('#tel').val(),
				success: function (msg) {

				}
			});

			alert('提交成功');
			$('.input').val("");


		}
	}

</script>
{% endblock %}