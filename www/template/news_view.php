{% extends "base.php" %}

{% block main %}
<div class="a_banner">
	<img src="/images/case_banner_04.jpg" width="1003" height="180" /> </div>
<div class="current">
	<div class="cont con_bg">
		<div class="current_l">首页  >>  新闻动态  >>  新闻内容</div>
		<div class="current_r">
			<ul>

				<li class="submenu"><a href="news.php">新闻动态</a></li>

			</ul>
		</div>
	</div>
</div>
<div class="mian">
	<div class="cont">
		<div class="news_titl">
			<dl>
				<dd>{{news.title}}</dd>
<!--				<dt>{{news.time|date('Y-m-d H:i')}}</dt>-->
			</dl>
		</div>
		{% autoescape false %}
		<div class="news_cont">
			{{news.content}}
		</div>
		{% endautoescape %}
	</div>

</div>
{% endblock %}