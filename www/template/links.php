{% extends "base.php" %}
{% set title = '友情链接' %}
{% block _body %}
{% spaceless %}
<div class="box margin-top">
	<div class="box1000">
		<div class="u-left">
			{{box("help",{style:'links'})}}
		</div>
		<div class="u-right">
			<div class="fb_tab">
				<ul>
					<li><a href="#"  class="am-active">{{title}}</a></li>

				</ul>
			</div>
			<div class="about margin-top10 border">
				{% autoescape false %}
				{{content}}
				{% endautoescape %}
			</div>
		</div>
	</div>
</div>
{% endspaceless %}
{% endblock %}