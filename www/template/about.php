{% extends "base.php" %}

{% block main %}
<div class="a_banner">
	<img src="/images/a_banner_04.jpg" width="1003" height="180" /> </div>
<div class="current">
	<div class="cont">
		<div class="current_l">首页  >>   关于欢腾</div>
		<div class="current_r">
			<ul>
				<li class="submenu"><a href="about.php">关于欢腾</a></li>
			</ul>
		</div>
	</div>
</div>
<div class="mian">
	<div class="cont con_bg">
		<div class="a_about">
			<h1>欢腾是谁？</h1>
			　　广州市欢腾网络科技有限公司成立于2009年，注册资金100万，定位电子商务平台服务提供商。
			　　<br />
			　　四年多的时间的努力，我们已拥有两大自主运营平台，持续为公司发展提供稳定资金：
			　　<br />
			　　<span>推介网</span>　
			<br />
			　　致力于体育竞彩彩民信息分享及交流的体育社区门户建设
			　　<br />
			　　<span>骰   &nbsp;&nbsp;友</span>　
			<br />
			　　移动娱乐互动交友平台，2012手机之家最佳娱乐应用奖得主
			　　<br />
			　　并为包括广州大剧院、温迪传播机构、香港电台、太阳日报、agisl跨国投资集团飞越书城、500W彩票网、天河电脑城、封之神游戏、微疯客等各领域的大型公司提供了基于web或移动互联网的电子商务平台解决方案，并赢得一致好评。

			<br />
			　　 <br />
			　　卓越的成绩来源于高素质技术团队的支撑；欢腾网络凝聚了一批行业内年轻、有活力，在各自的阵地上有着出色表现及多年实战经验的精英成员，我们相信，我们的实力和配合绝对可以胜任任何具有挑战性的工作！ </div>
		<div class="a_bottom">
			<div class="a_program">
				<div class="a_program_ti">
					<dl>
						<dd>欢腾能为您做什么？</dd>
						<dt><img src="/images/about_14.gif" width="346" height="102" /></dt>
					</dl>
				</div>
				<div class="a_program_con">
					<dl>
						<dd><img src="/images/about_18.gif" width="50" height="50" />&nbsp;移动开发</dd>
						<dt><span>移动互联网</span></dt>
						<dt>手机网站制作</dt>
						<dt>移动社交平台</dt>
						<dt>手机游戏开发</dt>
						<dt>生活应用开发</dt>
					</dl>
					<dl>
						<dd><img src="/images/about_20.gif" width="50" height="50" />&nbsp;网站开发</dd>
						<dt><span>整体解决方案</span></dt>
						<dt>大型B2C/C2C商城</dt>
						<dt>大型文化商务网站</dt>
						<dt>体育资讯/数据平台</dt>
						<dt>其他网站解决方案</dt>
					</dl>
					<dl>
						<dd><img src="/images/about_24.gif" width="50" height="50" />&nbsp;网站优化</dd>
						<dt><span>页面重构</span></dt>
						<dt>功能架构优化</dt>
						<dt>页面美化</dt>
						<dt>功能升级</dt>
						<dt>代码重构</dt>
					</dl>
					<dl>
						<dd><img src="/images/about_25.gif" width="50" height="50" />&nbsp;营销推广</dd>
						<dt><span>市场营销推广</span></dt>
						<dt>网站SEO优化</dt>
						<dt>数据拆解/采集</dt>
						<dt>数据监测/分析</dt>
						<dt>整体营销方案策划</dt>
					</dl>
					<dl>
						<dd><img src="/images/about_28.gif" width="50" height="50" />&nbsp;程序开发</dd>
						<dt><span>程序/软件开发</span></dt>
						<dt>网络营销系统</dt>
						<dt>智能在线客服</dt>
						<dt>客户交互系统</dt>
						<dt>软件定制开发</dt>
					</dl>
				</div>
			</div>
			<div class="a_strength">
				<div class="a_program_ti">
					<dl>
						<dd>欢腾凭什么能做到？</dd>
					</dl>
				</div>
				<div class="a_strength_con">
					<dl>
						<dd>丰富的互联网从业经验</dd>
						<dt>
							<span>拥有丰富经验的人员组成</span>
							团队成员皆有在业界拥有多年工作经验的精英组成，对互联网相关项目的开发、应用、策划、运营都有深刻理解和认识

						</dt>
					</dl>
					<dl>
						<dd>高效的默契合作团队</dd>
						<dt>
							<span>丰厚的开发运营及实施经验</span>
							包括网上商城、门户资讯、网络社区运营、彩票代卖、网上支付、电脑城、即时体育、移动互联网在内的丰富项目的开发、运营经验
						</dt>
					</dl>
					<dl>
						<dd>灵活的技术解决方案</dd>
						<dt>
							<span>以用户为中心，高效、灵活运作的团队</span>
							项目团队制只对上司负责、对结果负责、一帮一机制

						</dt>
					</dl>
				</div>
				<div class="a_strength_bot">专业的研发运营团队是我们坚强的后盾</div>
			</div>
		</div>
	</div>

</div>
{% endblock %}