{% extends "base.php" %}

{% block main %}
<div class="a_banner">
	<img src="/images/news_banner_04.jpg" width="1003" height="180" /> </div>
<div class="current">
	<div class="cont">
		<div class="current_l">首页  >>  新闻动态  </div>
		<div class="current_r">
			<ul>

				<li class="submenu"><a href="news.php">新闻动态</a></li>

			</ul>
		</div>
	</div>
</div>
<div class="mian">
	<div class="cont con_bg">
		<div class="news">
			{% for v in news %}
			<dl>
				<dd><a href="{{href('news/'~v.id)}}"><img onerror="this.src='/images/notimg.gif'" src="{{v.img!=''?v.img:'/images/notimg.gif'}}" width="200" height="150" /></a></dd>
				<dt><h1><a href="{{href('news/'~v.id)}}">{{v.title}}</a></h1>
				{{v.content|slice(0,150)}}
			</dl>
			{% endfor %}
		</div>
		{{ box('page', nav, 360) }}
	</div>

</div>
{% endblock %}