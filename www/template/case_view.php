{% extends "base.php" %}

{% block main %}
<div class="a_banner">
	<img src="/images/case_banner_04.jpg" width="1003" height="180" /> </div>
<div class="current">
	<div class="cont">
		<div class="current_l">首页  >>  成功案例  >>  骰友</div>
		<div class="current_r">
			<ul>

				<li class="submenu"><a href="case.php">成功案例</a></li>

			</ul>
		</div>
	</div>
</div>
<div class="mian">
	<div class="cont">
		<div class="news_titl">
			<dl>
				<dd>{{news.title}}</dd>
				<dt>{{news.time|date('Y-m-d H:i')}}</dt>
			</dl>
		</div>
		{% autoescape false %}
		<div class="news_cont">
			{{news.content}}
		</div>
		{% endautoescape %}
	</div>

</div>
{% endblock %}
