<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>404-欢腾网络</title>
    <style>
        body {
            font-size:12px;
            color:#4a4a4a;
            margin:0px;
            padding:0px;
            font-size:12px;
            font-family:Verdana, Geneva, sans-serif;
        }
        body, ul, li, p, dd {
            margin:0px;
            padding:0px;
        }
        img {
            vertical-align:top;
        }
        li, dl {
            list-style-type:none;
        }
        h2, h3, h4, h5, h6 {
            margin:0;
            padding:0;
            font-weight:normal;
        }
        h1 {
            margin:0;
            padding:0;
        }
        div, form, img, ul, ol, li, dl, dt, dd {
            margin:0;
            padding:0;
            border:0;
        }
        .clear {
            height:0px;
            line-height:0px;
            clear:both;
        }
        ul, li, dl, dd, dt {
            list-style:none;
        }
        .box {
            width:1000px;
            margin:0 auto;
            text-align:center;
            padding:10px 0px;
        }
        .size {
            font-size:200px;
            font-family:"微软雅黑";
            color:#08497d;
        }
        .size1 {
            font-size:26px;
            font-family:"微软雅黑";
            color:#08497d;
        }
        a.return {
            background:#08497d;
            height:29px;
            width:79px;
            line-height:29px;
            color:#fff;
            display:inline-block;
            text-decoration:none;-moz-border-radius:3px; -webkit-border-radius:3px; border-radius:3px;
        }
        a.return:hover {
            background:#0073cf;
            height:29px;
            width:79px;
            line-height:29px;
            color:#fff;
            display:inline-block;
            text-decoration:none;
        }
        .menu {
            width:1000px;
            margin:0 auto;
            text-align:center;
            padding:50px 0px 0px;
        }
        .menu ul {
            width:750px;
            margin:0 auto;
        }
        .menu li {
            height:30px;
            line-height:30px;
            float:left;
            padding:0px 5px;
            color:#858585;
        }
        .menu li a {
            color:#858585;
            text-decoration:none;
        }
        .menu li a:hover {
            color:#08497d;
            text-decoration:none;
        }
    </style>
</head>
<body>
<div class="box size">404</div>
<div class="box size1">您访问的页面不存在，但不要着急哦！</div>
<div class="box"><a href="//www.999zq.com/" class="return">返回首页</a></div>
<div class="menu">
    <ul>
        <li><a href="//www.999zq.com/">欢腾网络</a></li>
        <li> | </li>
        <li><a href="//www.999zq.com/expert/football">所有推荐</a></li>
        <li> | </li>
        <li><a href="//www.999zq.com/kill/football">临场绝杀</a></li>
        {#
<!--        <li> | </li>-->
<!--        <li><a href="https://www.999zq.com/confidence/football">重心推荐</a></li>-->
        #}
        <li> | </li>
        <li><a href="//www.999zq.com/hot/football-all">热门赛事</a></li>
        <li> | </li>
        <li><a href="//www.999zq.com/challenge/football-7day">竞猜擂台</a></li>
        <li> | </li>
        <li><a href="//www.999zq.com/big5">五大联赛</a></li>
        <li> | </li>
        <li><a href="//www.999zq.com/promotion">优惠套餐</a></li>
        <li> | </li>
        <li><a href="//www.999zq.com/news">新闻资讯</a></li>
        <li> | </li>
        <li><a href="//www.999zq.com/pic">足球宝贝</a></li>
    </ul>
</div>
</body>
</html>
