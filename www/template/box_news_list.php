<ul class="news-ul">
	{% for v in data %}
	{% if class =='little' %}
	<!--新闻首页列表-->
	<h3><a href="{{href('news/'~v.id)}}" target="_blank">{{v.title}}</a></h3>
	{% else %}
	<!-- 分类列表页-->
	<li>
		<a href="{{href('news/'~v.id)}}" target="_blank">{% if show_time %} <span>{{v.time|date('Y-m-d')}}</span>{% endif %}
			<i class="point"></i>{{v.title}}</a>
	</li>
	{% endif %}
	{% endfor %}
</ul>
{{ box('page', nav, 360) }}
