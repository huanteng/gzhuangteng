{% extends "base.php" %}

{% block main %}
<div class="a_banner">
	<img src="/images/cultural_banner_04.jpg" width="1003" height="180" /> </div>
<div class="current">
	<div class="cont">
		<div class="current_l">首页  >>  企业文化</div>
		<div class="current_r">
			<ul>
				<li class="submenu"><a href="cultural.php">企业文化</a></li>
			</ul>
		</div>
	</div>
</div>
<div class="mian">
	<div class="cont con_bg">
		<div class="cultural_list">
			<dl>
				<dd>公司环境</dd>
				<dt>无论什么样的企业都需要凝聚力，而成功的企业更是这样！这种凝聚力往往以其文化为载体体现在企业的方方面面，尤其是工作环境！因为，我们每天有三分之一的时间在这里度过！</dt>
			</dl>
			<ul>
				<li><a onclick="ShowDiv('MyDiv','fade')" title="办公环境"><img src="/images/cultural_03.jpg" width="160" height="160" /></a></li>
				<li><a onclick="ShowDiv('MyDiv','fade')" title="办公环境"><img src="/images/cultural_05.jpg" width="160" height="160px"/></a></li>
				<li><img src="/images/cultural_07.jpg" width="160" height="160" /></li>
				<li><img src="/images/cultural_09.jpg" width="160" height="160" /></li>
				<li><img src="/images/cultural_11.jpg" width="160" height="160" /></li>
				<li><img src="/images/cultural_03.jpg" width="160" height="160" /></li>
				<li><img src="/images/cultural_05.jpg" width="160" height="160" /></li>
				<li><img src="/images/cultural_07.jpg" width="160" height="160" /></li>
				<li><img src="/images/cultural_09.jpg" width="160" height="160" /></li>
				<li><img src="/images/cultural_11.jpg" width="160" height="160" /></li>
				<li><img src="/images/cultural_03.jpg" width="160" height="160" /></li>
				<li><img src="/images/cultural_05.jpg" width="160" height="160" /></li>
				<li><img src="/images/cultural_07.jpg" width="160" height="160" /></li>
				<li><img src="/images/cultural_09.jpg" width="160" height="160" /></li>
				<li><img src="/images/cultural_11.jpg" width="160" height="160" /></li>
			</ul>

			<div id="MyDiv" class="digPicBox" >
				<div class="close"><a  onclick="CloseDiv('MyDiv','fade')"><img src="/images/fancybox.png" width="22" height="22" /></a></div>

				<div class="rollphotos">
					<div class="blk_29">
						<div class="LeftBotton" id="Leftarr"></div>
						<div class="content" id="ISL_Cont_1">
							<div class="box"><img src="/images/31.jpg" width="800" />


							</div>
							<div class="box"><img src="/images/32.jpg" width="800" />

							</div>
							<div class="box"><img src="/images/32.jpg" />

							</div>
							<div class="box"><img src="/images/31.jpg" width="800" />

							</div>
							<div class="box"><img src="/images/31.jpg" width="800" />

							</div>
							<div class="box"><img src="/images/31.jpg" width="800" />

							</div>
							<div class="box"><img src="/images/31.jpg" width="800" />

							</div>
							<div class="box"><img src="/images/31.jpg" width="800" />

							</div>
							<!-- 图片列表 end --></div>
						<div class="RightBotton" id="Rightarr"></div>
					</div>
					<script language="javascript" type="text/javascript">

						var scrollPic_02 = new ScrollPic();
						scrollPic_02.scrollContId   = "ISL_Cont_1"; //内容容器ID
						scrollPic_02.arrLeftId      = "Leftarr";//左箭头ID
						scrollPic_02.arrRightId     = "Rightarr"; //右箭头ID

						scrollPic_02.frameWidth     = 800;//显示框宽度
						scrollPic_02.pageWidth      = 800; //翻页宽度

						scrollPic_02.speed          = 5; //移动速度(单位毫秒，越小越快)
						scrollPic_02.space          = 20; //每次移动像素(单位px，越大越快)
						scrollPic_02.autoPlay       = false; //自动播放
						scrollPic_02.autoPlayTime   = 3; //自动播放间隔时间(秒)

						scrollPic_02.initialize(); //初始化

					</script>


				</div>

			</div>
		</div>
	</div>

</div>
{% endblock %}