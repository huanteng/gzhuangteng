{% extends "base.php" %}

{% block main %}
<div class="banner" id="full-screen-slider">
	<ul id="slides">
		<li style="background:url(images/01.jpg) no-repeat center top;"><a href="#" target="_blank"></a></li>
		<li style="background:url(images/02.jpg) no-repeat center top;"><a href="#" target="_blank"></a></li>
		<li style="background:url(images/03.jpg) no-repeat center top;"><a href="#" target="_blank"></a></li>
		<li style="background:url(images/04.jpg) no-repeat center top;"><a href="#" target="_blank"></a></li>
		<li style="background:url(images/05.jpg) no-repeat center top;"><a href="#" target="_blank"></a></li>
	</ul>
</div>
<div class="mian">
	<div class="cont">
		<div class="i_con">
			<!---------公司简介开始---------------->
			<div class="i_about">
				<div class="i_about_ti"><img src="/images/i_cont_11.gif"/></div>
				<div class="i_about_con">　　广州市欢腾网络科技有限公司成立于2009年，注册资金100万，定位电子商务平台服务提供商。具有多年IT行业服务经验，拥有雄厚团队实力，共拥有员工100多人，其中开发人员60余人，运营人员40余人...<a href="about.php">【了解欢腾】</a></div>
			</div>
			<!---------公司简介结束---------------->
			<!---------公司新闻开始---------------->
			<div class="i_news">
				<div class="i_news_ti"><img src="/images/i_cont_13.gif"  /></div>
				<div class="i_news_con">
					<dl>
						<dd><a href="#"><img src="/images/i_con_24.gif" width="88" height="88" /></a></dd>
						<dt>
						<ul>
							{% for v in hot_news %}
							<li><a href="{{href('news/'~v.id)}}">{{v.title}}</a></li>
							{% endfor %}

						</ul>
						</dt>
					</dl>
				</div>
			</div>
			<!---------公司简介结束---------------->
			<!---------联系我们开始---------------->
			<div class="i_contact">
				<div class="i_contact_ti"><img src="/images/i_cont_15.gif" width="188" height="59" /></div>
				<div class="i_contact_con">
					<p>广州市欢腾网络科技有限公司 </p>
					<p>地址：广州市海珠区新港中路佳信花园C4栋 </p>
					<p>邮箱：cs@gzhuanteng.com  </p>
					<p>网址：<a href="http://www.gzhuanteng.com">http://www.gzhuanteng.com</a></p>
				</div>
			</div>
		</div>
		<!---------案例展示开始---------------->
		<div class="i_case">
			<div id="tabc13">
				<div class="al" id="leftarr13"></div>
				<div class="ashow2">
					<ul class="list_02" id="isl_cont_13">
						{% for v in case_list %}
						<li><a href="{{href('news/'~v.id)}}" title="骰友"><img src="{{v.img}}" ></a></li>
						{% endfor %}
					</ul>
				</div>
				<div class="ar" id="rightarr13"></div>
			</div>
		</div>
		<!---------案例展示结束---------------->
	</div>
</div>

{% endblock %}