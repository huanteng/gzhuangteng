{% spaceless %}
{%- block top -%}
<!doctype html>
<html class="no-js">
{%- block top1 -%}
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="X-Frame-Options" content="deny">
<meta name="copyright" content="www.gzhuanteng.com">
<meta name="description" content="{{ description == '' ? '欢腾网络' : description }}">
<meta name="keywords" content="{{keywords == '' ? '欢腾网络' : keywords ~ ', 欢腾网络'}}">
<title>{{title==''?'欢腾网络':title~'-欢腾网络'}}</title>

<!-- Set render engine for 360 browser -->
<meta name="renderer" content="webkit">

<!-- No Baidu Siteapp-->
<meta http-equiv="Cache-Control" content="no-siteapp"/>
<link rel="icon" type="image/png" href="/assets/i/favicon.png">

<!-- Add to homescreen for Chrome on Android -->
<meta name="mobile-web-app-capable" content="yes">
<link rel="icon" sizes="192x192" href="/assets/i/app-icon72x72@2x.png">

<!-- Add to homescreen for Safari on iOS -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="apple-mobile-web-app-title" content="Amaze UI"/>
<link rel="apple-touch-icon-precomposed" href="/assets/i/app-icon72x72@2x.png">

<!-- Tile icon for Win8 (144x144 + tile color) -->
<meta name="msapplication-TileImage" content="/assets/i/app-icon72x72@2x.png">
<meta name="msapplication-TileColor" content="#0e90d2">


<link href="/css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/jquery-1.8.0.min.js"></script>
<script src="/js/slideshow.js" type="text/javascript"></script>
<script src="/js/163css.js" type="text/javascript"></script>

</head>
{% endblock %}

<body>
<header>
	{%- block top2 -%}
	{{ box('top', {}, 0)}}
	{% endblock %}
</header>

{% endblock %}

{%- block _body -%}
	{% spaceless %}
		<main>
			<div class="box margin-top">
				<div class="box1000">
					<div class="left">


						{% block main %}


						{% endblock %}
					</div>
				</div>
			</div>
		</main>
	{% endspaceless %}
{% endblock %}

{%- block bottom -%}
	{% spaceless %}
		{{ box( 'bottom', {}, 300 ) }}
	{% endspaceless %}
{% endblock %}
{% endspaceless %}