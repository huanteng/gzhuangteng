{% extends "base.php" %}

{% block main %}
<div class="a_banner">
	<img src="/images/cultural_banner_04.jpg" width="1003" height="180" /> </div>
<div class="current">
	<div class="cont">
		<div class="current_l">首页  >>  企业文化</div>
		<div class="current_r">
			<ul>
				<li class="submenu"><a href="cultural.php">企业文化</a></li>
			</ul>
		</div>
	</div>
</div>
<div class="mian">
	<div class="cont con_bg">
		<div class="cultural">
			<dl>
				<dd>公司环境</dd>
				<dt></dt>
			</dl>

			<ul>
				{% for k,v in company %}
				<li><a href="{{href('cultural/view/'~v.id)}}"><img src="/photo/{{v.file}}" width="160" height="160" /></a></li>
				{% endfor %}
			</ul>
<!--			<dl>-->
<!--				<dd>家庭成员</dd>-->
<!--				<dt></dt>-->
<!--			</dl>-->
<!--			<ul>-->
<!--				{% for k,v in family %}-->
<!--				<li><a href="{{href('cultural/view/'~v.id)}}"><img src="/photo/{{v.file}}" width="160" height="160" /></a></li>-->
<!--				{% endfor %}-->
<!--			</ul>-->
<!--			<dl>-->
<!--				<dd>聚餐旅游</dd>-->
<!--				<dt></dt>-->
<!--			</dl>-->
<!--			<ul>-->
<!--				{% for k,v in travel %}-->
<!--				<li><a href="{{href('cultural/view/'~v.id)}}"><img src="/photo/{{v.file}}" width="160" height="160" /></a></li>-->
<!--				{% endfor %}-->
<!--			</ul>-->
		</div>
	</div>

</div>
{% endblock %}