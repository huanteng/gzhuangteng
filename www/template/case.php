{% extends "base.php" %}

{% block main %}
<div class="a_banner">
	<img src="/images/case_banner_04.jpg" width="1003" height="180" /> </div>
<div class="current">
	<div class="cont">
		<div class="current_l">首页  >>  成功案例  </div>
		<div class="current_r">
			<ul>

				<li class="submenu"><a href="case.php">成功案例</a></li>

			</ul>
		</div>
	</div>
</div>
<div class="main">
	<div class="cont con_bg">
		<div class="case">
			{% for v in case %}
			<dl>
				<dd class="gray" onmouseover="this.className='red'" onmouseout="this.className='gray'"><a  title="{{v.title}}" href="{{href('case/'~v.id)}}"><img src="{{v.img}}" width="220" height="150" /></a></dd>
				<dt>
				<h1><a href="{{href('case/'~v.id)}}">{{v.title}}</a></h1>
				<span><a href="{{href('case/'~v.id)}}"></a></span>
				</dt>
			</dl>
			{% endfor %}

		</div>

	</div>

</div>
{% endblock %}