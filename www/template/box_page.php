
{% if pagecount>1%}
<div class="pagination">
	<ul class="ht-pagination ht-pagination-default">
		{%if page>1%}
		<li class="ht-pagination-last"><a href="{{url}}{{page-1}}">上一页</a></li>
		{% endif %}
		{% for i in start..end %}
		<li {% if i==page%}class="ht-active"{% endif %}><a href="{{url}}{{i}}">{{i}}</a></li>
		{% endfor %}
		{%if page<pagecount%}
		<li class="ht-pagination-last "><a href="{{url}}{{page+1}}">下一页</a></li>	</ul>
	{% endif %}
</div>
{% endif %}