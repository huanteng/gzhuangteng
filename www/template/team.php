{% extends "base.php" %}

{% block main %}
<div class="a_banner"><img src="/images/team_banner_04.jpg" width="1003" height="180" /> </div>
<div class="current">
<div class="cont">
    	<div class="current_l">首页  >>  技术团队  </div>
        <div class="current_r">
        	<ul>
            
           	  <li class="submenu"><a href="#" title="技术团队">技术团队</a></li>
            </ul>
        </div>
        </div>
    </div>
<div class="mian">
<div class="cont con_bg">
	<div class="t_team">
   	  <h1>欢腾团队</h1>
      <span><p><img src="/images/team.png" width="877" height="214" /></p>
      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;欢腾网络公司以移动互联网为起始点，结合电子商务与社会化网络，凝聚了一批行业内年轻、有活力，在各自的阵地上有着出色表现及多年实战经验的精英成员，卓越的成绩来源于高素质技术团队的支撑；我们的实力和配合绝对可以胜任任何具有挑战性的工作！ </p></span>
	</div>
<div class="t_bottom">
	<div class="t_idea">
    	<div class="t_idea_ti">
        	<dl>
            	<dd>高效协作圆环</dd>
                <dt><img src="/images/team_06.jpg" width="385" height="385" /></dt>
            </dl>
        </div>
        <div class="t_idea_con">
       	  <ul>
            	<li><img src="/images/team_12.gif" width="22" height="22" />&nbsp;整体规划/落实执行</li>
                <li><img src="/images/team_14.gif" width="22" height="22" />&nbsp;内容运营、监控、引导</li>
                <li><img src="/images/team_15.gif" width="22" height="22" />&nbsp;产品研发、维护、优化</li>
                <li><img src="/images/team_17.gif" width="22" height="22" />&nbsp;市场、销售、客服</li>
            </ul>
          <dl>
            	<dd>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;区别于大多数企业的金字塔式架构，我们的企业架构为特殊的圆环状架构。
<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;我们力争让每一位同伴在不脱离客户实际需求的前提下，都能够尽量的畅所欲言，实现自我；和谐融洽的气氛，产生更强大的凝聚力，让团队成员之间合作无间，工作效率/执行力自然更高更强！ </dd>
            </dl>
        </div>
    </div>
</div>
 </div>
 
</div>


{% endblock %}