
<!------------头部菜单------------->
<div class="top">
	<div class="head">
		<div class="logo"><a href="index.php" title="欢腾网络"><img src="/images/logo_04.gif" width="217" height="97" /></a></div>
		<div class="nav">
			<ul id="menu" class="menu clearfix">
				<li><a href="/index"><span class="out">首页</span> <span class="over">首页</span> </a></li>
				<li><a href="/about"><span class="out">关于欢腾</span> <span class="over">关于欢腾</span> </a></li>
				<li><a href="/case"><span class="out">成功案例</span><span class="over">成功案例</span> </a></li>
				<li><a href="/team"><span class="out">技术团队</span> <span class="over">技术团队</span> </a></li>
				<li><a href="/news"><span class="out">新闻动态</span> <span class="over">新闻动态</span> </a></li>
				<li><a href="/cultural"><span class="out">企业文化</span> <span class="over">企业文化</span> </a></li>
				<li><a href="/contact" ><span class="out">联系我们</span> <span class="over">联系我们</span> </a></li>
			</ul>
		</div>
	</div>
</div>