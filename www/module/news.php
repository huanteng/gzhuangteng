<?php
class action extends frontend
{
	function cache_rule()
	{
		return array(
			'home' => 600,
			'view' => 600,
		);
	}
	
	function route_rule()
	{
		return array(
			'^(\d+)$' => 'view/id/$1',
			'^page\/(\d+)' => 'home/page/$1',
		);
	}

	function home( $in )
	{

		$news = biz('news');

		$in += array(
			'empty' => '暂无数据',
			'other' => array(),
			'pagesize' => 5,
			'orderby'=>'time desc',
		);
		$news = biz('news');
		$news_content = biz('news_content');


		$other[] = 'type = 1 ';


		$option = array(
			'term' => $other
		);

		$new_list = $news->search( $in, array(), array(), array(), $option );

		foreach($new_list['data'] as $k=>$v)
		{
			$content = $news_content->get1( 'content', ['id'=>$v['id']] );

			$imgs = $news->get_content_imgs($content['content']);
			$new_list['data'][$k]['content'] = strip_tags($content['content']);
			$new_list['data'][$k]['content'] = str_replace('&nbsp;','',$new_list['data'][$k]['content']);
			$new_list['data'][$k]['img'] = $imgs[0];
		}


		$data = array(
			'title' => $this->title(basename(__FILE__,'.php')),
			'news' => $new_list['data'],
			'nav'=> $new_list['nav']
		);
		return $this->render( $data );
	}

	function view($in)
	{
		$id = is_numeric($in['id']) ? $in['id'] : 0;
		if($id == 0) header('location:index.php');

		$news = biz('news');
		$news_content = $news->info( $id );

        if(empty($news_content)){
            go_404();
        }


		$type_array = $news->type_dict();
		$type_name = $type_array[$news_content['type']];


		$news_content['content'] = $news_content['content'];

		if($news_content['from']!='')
		{
			$news_content['content'] = str_replace(array("\r\n", "\r", "\n"), "<br/>", $news_content['content']);
		}

		$news_content['content']=preg_replace("/&lt;.*?&gt;/si","",$news_content['content']); //注释

		$pattern = "/(<br\/>){2,}/i";
		$replace = '<br />';
		$news_content['content'] = preg_replace($pattern, $replace, $news_content['content']);


		$data = array(
			'news' => $news_content,
			'type_name'=>$type_name,
			'type'=>$news_content['type'],
		);
		return $this->render( $data );
	}



}

?>