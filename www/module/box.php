<?php
class action_box extends frontend
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'box';
	}

    function news_list($in){
        return $in;
    }

    // 页头
	function top( $in )
	{
		$url = config('url');

        $old = $_SERVER[ 'REQUEST_URI' ];
        $old = substr( $old, count( $url[ 'root' ] ) );

        $old_language = substr( $old, 0, 3 );
        if( $old_language == 'sc/' || $old_language == 'en/' )
        {
            $old = substr( $old, 3 );
        }

        $language = $url[ 'language' ];
        $root = $url[ 'root' ];

        $out = array( 'sc' => '', 'hk' => '', 'en' => '' );
        if( $language != 'sc' )
        {
            $out[ 'sc' ] = $root . 'sc/' . $old;
        }
        if( $language != 'hk' )
        {
            $out[ 'hk' ] = $root . $old;
        }
        if( $language != 'en' )
        {
            $out[ 'en' ] = $root . 'en/' . $old;
        }

        $in['ln']=$out;


        $in = load('arr')->set_default( $in, array(
			'title' => '欢腾网络',
			'keyword' => '欢腾网络'
		) );
		$in['title'] = $in['title'] != '欢腾网络' ? $in['title'] . '-' . '欢腾网络' : $in['title'];
		$in['keyword'] = $in['keyword'] != '' ? $in['keyword'] . ',欢腾网络' : $in['keyword'];
        $in['url'] = $url;

		return $in;
	}

	// 页尾
	function bottom( $in )
	{
		return $in;
	}


	// 翻页页码列表
	function page( $in )
	{
		$page = $in[ 'page' ];
		$start = $page-2 > 0 ? $page-2 : 1;
		$end = $start+4 < 5 ? 5 : $start+4;
		$end = min( $end, $in[ 'pagecount' ] );

		$url = $_SERVER["REQUEST_URI"];

		$route = array(
			'-page-\d+$' => '',
			'\/page-\d+' => '',
			'&page=\d+$' => '',
			'\/page=\d+' => '',
			'page=\d+$' => '',
			'page=\d+&'=>''
		);

		foreach ($route as $k => $v) {
			$pattern = '/' . $k . '/';

			if( preg_match( $pattern, $url, $matches, PREG_OFFSET_CAPTURE ) === 1 )
			{
				$url = preg_replace( $pattern, $v, $url );
				break;
			}
		}

		if(strpos($url,"=")>0){
			$url .= '&page=';
		}else{
			if(strpos($url,"?")>0) {
				$url .= 'page=';
			}else{
				$url .= '?page=';
			}
		}

		$in[ 'start' ] = $start;
		$in[ 'end' ] = $end;
		$in[ 'url' ] = $url;

		return $in;
	}

	/*
	 * 新闻列表
	 * mode ： 普通列表，第一个不同的列表，第一个带内容的列表...
	 */
	function news2( $in )
	{
		$in += array(
			'mode' => 1,
			'show_page' => false,
			'empty' => '暂无数据',
			'class' => '',
			'show_time' => false,
			'other' => array(),
			'type' => '',
			'sport' => 1,
			'orderby'=>'hot desc,time desc'
		);

		$news = biz('news');
		$other = $news->to_array( $in['other'] );

		if( !empty($in['type']) && empty($other) ) {
			$other[] = 'type in ('.$in['type'].')';
		}

		$option = array(
			'term' => $other
		);

		$data = $news->search( $in, array(), array(), array(), $option );

		$in[ 'data' ] = &$data[ 'data' ];
		$in[ 'nav' ] = &$data[ 'nav' ];

		return $in;
	}

	function help( $in )
	{
		return $in;
	}


	/** 广告
	 * @param $id
	 * @return array
	 */
	function ad( $id )
	{
		$out = biz('position')->get_from_id( $id );
		if( empty( $out ) )
		{
			return array();
		}

		$position_id = $out[ 'id' ];
		$ad = biz( 'ad' );

		$term = array( 'position_id=' . $position_id, 'expires=0 or expires>=' . time() );
		$info = $ad->get1( 'max(level) as level',  $term );

		if( empty( $info ) )
		{
			return array();
		}

		$term[ 'level' ] = $info[ 'level' ];
		$info_ad = $ad->get1( '*', $term, 'rand()' );

		$info=$info_ad+$out;

		if( !empty( $info_ad ) )
		{
			$content = '';
			if( $info['url'] != '' )
			{
				$content = '<a href="' . $info['url'] . '"';

				if( $info['follow'] == 0 )
				{
					$content .= ' rel="nofollow"';
				}
				if( $info['title'] != '' )
				{
					$content .= ' title="' . $info['title'] . '"';
				}
				if( $info['target'] != '' )
				{
					$content .= ' target="' .  $info['target'] . '"';
				}
				$content .= '>';
			}

			$content .= '<img src="' . ( '/banner/'.$info['filename'] ) . '"';
			if( $info['width'] != 0 )
			{
				$content .= ' width="' . $info['width'] . '"';
			}
			if( $info['height'] != 0 )
			{
				$content .= ' height="' . $info['height'] . '"';
			}
			if( $info['title'] != '' && $info['url'] == '' )
			{
				$content .= ' alt="' . $info['title'] . '"';
			}
			$content .= '>';

			if( $info['url'] != '' )
			{
				$content .= '</a>';
			}

			return array('content'=>$content);
		}else{
			biz('task')->set_alert( true, 0, '有广告图过期', '', 'ad.php?position_id=' . $id );
			return array();
		}
	}

	/*
 * 焦点图
 */
	function focus()
	{
		$out['focus_photo'] = biz('ad')->get( '*',  array('position_id=3', 'expires=0 or expires>=' . time()), 'level desc' );
		return $out;
	}


}
?>