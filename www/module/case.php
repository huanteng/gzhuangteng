<?php
class action extends frontend
{
	function __construct()
	{
		parent::__construct();

	}
	function route_rule()
	{
		return array(
			'^(\d+)$' => 'view/id/$1',
			'^page\/(\d+)' => 'home/page/$1',
		);
	}


	//成功案列 新闻类型为2
	function home( $in )
	{
		$news = biz('news');
		$new_list= $news->get( 'id,title', array('`type` = 2 '), 'id desc', 9, 1800 );
		foreach($new_list as $k=>$v)
		{
			$info = biz('news_content')->get_from_id($v['id']);
			$new_list[$k]['content'] = $info['content'];
		}

		foreach($new_list as &$v){
			$imgs = $news->get_content_imgs($v['content']);

			$txt = preg_replace("/<img.*?>/si","",$v['content']);
			$v['content']=$txt;
			$v['img']=$imgs[0];
		}

		$data = array(
			'case'=>$new_list,
		);


		return $this->render( $data );
	}


	function view($in)
	{
		$id = is_numeric($in['id']) ? $in['id'] : 0;
		if($id == 0) header('location:index.php');

		$news = biz('news');
		$news_content = $news->info( $id );

		if(empty($news_content)){
			go_404();
		}


		$type_array = $news->type_dict();
		$type_name = $type_array[$news_content['type']];


		$news_content['content'] = $news_content['content'];

		if($news_content['from']!='')
		{
			$news_content['content'] = str_replace(array("\r\n", "\r", "\n"), "<br/>", $news_content['content']);
		}

		$news_content['content']=preg_replace("/&lt;.*?&gt;/si","",$news_content['content']); //注释

		$pattern = "/(<br\/>){2,}/i";
		$replace = '<br />';
		$news_content['content'] = preg_replace($pattern, $replace, $news_content['content']);


		$data = array(
			'news' => $news_content,
			'type_name'=>$type_name,
			'type'=>$news_content['type'],
		);
		return $this->render( $data );
	}
}
?>