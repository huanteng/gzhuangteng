<?php
class action extends frontend
{
	function __construct()
	{
		parent::__construct();

	}

	function home( $in )
	{
		//公司环境
		$in['company'] = biz('focus_photo')->get( '*',  array( 'type = 1' ));
		//家庭成员
		$in['family'] = biz('focus_photo')->get( '*',  array( 'type = 2' ));
		//聚餐旅游
		$in['travel'] = biz('focus_photo')->get( '*',  array( 'type = 3' ));

		return $this->render($in);

	}
	function view( $in )
	{

		return $this->render($in);

	}
}
?>