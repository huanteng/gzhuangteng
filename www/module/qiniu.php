<?php
class action extends frontend
{
	// 后台焦点图
	function focus( $in )
	{
		$qiniu = biz( 'qiniu' );
		$qiniu->log( '', 0, 'log', array( 'data' => $in ) );

		$focus_photo = biz( 'focus_photo' );

		$info = $focus_photo->get_from_id( $in[ 'id' ] );

		// 七牛可能多次回调，所以要加判断
		if( $info[ 'file' ] != $in[ 'key' ] )
		{
			$qiniu->log( '', 0, '删除文件：' . $info[ 'file' ] );
			$qiniu->delete( $info[ 'file' ] );

			$focus_photo->set( array( 'id' => $in[ 'id' ], 'file' => $in[ 'key' ] ) );
		}

		$url = $qiniu->url( $in[ 'key' ], 0 );
		return $this->out( array( 'url' => $url ), 'qiniu_focus' );
	}

	// 广告
	function ad( $in )
	{
		$qiniu = biz( 'qiniu' );
		$qiniu->log( '', 0, 'log', array( 'data' => $in ) );

		$ad = biz( 'ad' );

		$info = $ad->get_from_id( $in[ 'id' ] );

		// 七牛可能多次回调，所以要加判断
		if( $info[ 'filename' ] != $in[ 'key' ] )
		{
			if( $info[ 'filename' ] != '' )
			{
				$qiniu->delete( $info[ 'filename' ] );
			}

			$ad->set( array( 'id' => $in[ 'id' ], 'filename' => $in[ 'key' ] ) );
		}

		$url = "https://www.999zq.com/";

		if( config( 'develop' ) )
		{
			$url = "http://test.999zq.com/";
		}


		$content = '<script type="text/javascript">alert("上传成功");location="'.$url.'_back2/ad.php?method=edit&id=' .
			$in[ 'id' ] . '";</script>';

		return $content;
	}

	// 用户头像
	function logo( $in )
	{
		$qiniu = biz( 'qiniu' );
		$qiniu->log( '', 0, 'log', array( 'data' => $in ) );

		$info = $this->login_info();
		$uid = $info[ 'uid' ];

		$field = biz('uchome_spacefield');
		$key = $field->get_field_from_id( $uid, 'logo', '' );

		if( $key != $in[ 'key' ] )
		{
			$qiniu->log( '', 0, '删除文件：' . $key );
			$qiniu->delete( $key );

			$field->set( array( 'uid' => $uid, 'logo' => $in[ 'key' ] ) );
		}

		$url = "https://www.999zq.com/";

		if( config( 'develop' ) )
		{
			$url = "http://test.999zq.com/";
		}

		if($in['type']=='mobile')
		{

			echo( '<script type="text/javascript">alert("上传成功");location="'.$url.'center/logo";</script>' );
		}
		else
		{
			echo( '<script type="text/javascript">alert("上传成功");location="/center/logo";</script>' );
		}

	}

	function home($in){
		biz('log')->info_log( '7牛上传', 0, $in);

        if (isset($in['error'])) {
            return '<script type="text/javascript">alert("上传失败:' . $in['error'] .
            '");history.back()</script>';
            die;
        }

		$retStr = $_GET["upload_ret"];
		//$errCode = $_GET["code"];
		//$errMsg = urldecode($_GET["error"]);

		if ($retStr)
			$decodedRet = base64_decode($retStr);
		$retObj = json_decode( $decodedRet, true );

		foreach( $retObj as $k => &$v )
		{
			$k_array = explode( 'x:', $k );
			if( count( $k_array ) == 2 )
			{
				unset( $retObj[ $k ] );
				$retObj[ $k_array[ 1 ] ] = $v;
			}
		}
		return $this->{$retObj['method']}($retObj);
	}
}
?>