<?php
class action extends frontend
{
	function __construct()
	{
		parent::__construct();

	}

	function home( $in )
	{

		return $this->render($in);

	}
	function contact_submit( $in )
	{
		$log = biz('log');
		if(!$log->check_email($in['email']))
		{
			return $this->ajax_out( 0, ' 亲，输入的邮箱格式不正确哦！' );
		}
		$content = "意见反馈，姓名：".$in['name'].",邮箱：".$in['email'].",QQ：".$in['QQ'].",电话：".$in['tel'].",留言内容：".$in['content'];

		if( !$log->exists( array('content'=>$content ) ) )
		{
			$log->add(array('content' => $content));
			return $this->ajax_out(1, '我们已经收到你的 我们尽快与您联系');

		}else
		{
			return $this->ajax_out(-1, '意见反馈失败');
		}


	}
}
?>