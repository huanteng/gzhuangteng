<?php
class action extends frontend
{
	function __construct()
	{
		parent::__construct();

	}

	function home( $in )
	{


		$news = biz('news');

		$in['hot_news']= $news->get('*', array('`type`=1'), '`time` desc', 5 );

		$news_content = biz('news_content');
		$case = $news->get( 'id,title', array('`type` = 2 '), 'id desc', 12 );
		foreach($case as $k=>$v)
		{
			$info = $news_content->get_from_id($v['id']);
			$case_list[$k]['content'] = $info['content'];
		}

		foreach($case_list as &$v){
			$imgs = $news->get_content_imgs($v['content']);

			$txt = preg_replace("/<img.*?>/si","",$v['content']);
			$v['content']=$txt;
			$v['img']=$imgs[0];
		}

		$in['case_list'] = $case_list;

		return $this->render($in);

	}
}
?>