<? box( 'top',  array( 'title' => '增加广告位' ), 86400 ); ?>
<?php include_once('_edit.php'); ?>

<div class="row-fluid">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th-list"></i> 增加广告位</h2>
			<div class="box-icon"><a href="javascript:history.back()" class="btn btn-round"><i class="icon-remove"></i></a></div>
		</div>
	</div>
</div>

<form id="main">
	<table class="table table-striped table-bordered">
		<tr>
			<td>名字</td>
			<td><input type="text" name="name" class="validate[required,maxSize[20]]"></td>
			<td>*不可重复，建议使用页面+位置名。</td>
		</tr>
		<tr>
			<td>宽</td>
			<td><input type="text" name="width" class="validate[custom[integer]]"></td>
			<td>广告位的宽度，0表示不输入</td>
		</tr>
		<tr>
			<td>高</td>
			<td><input type="text" name="height" class="validate[custom[integer]]"/> </td>
			<td>广告位的高度，0表示不输入</td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td>窗口</td>
			<td><input type="text" name="short_hk"></td>
			<td>链接中什么窗口打开？空表示本窗口</td>
		</tr>
		<tr>
			<td>follow</td>
			<td><?=$follow ?></td>
			<td>设置folow，将影响SEO</td>
		</tr>
	</table>
	<div id="cmd">
		<a do="add_save">保存</a>
		<a href="back">返回</a>
	</div>
</form>

<fieldset>
	<legend>&nbsp;</legend>
	<li>删除位置前，必须先删除对应位置里的广告；</li>
</fieldset>

<?php box( 'bottom', '', 86400 ); ?>