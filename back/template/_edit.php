<link rel="stylesheet" href="css/validationEngine.jquery.css">
<script src="js/jquery.validationEngine-zh-CN.js"></script>
<script src="js/jquery.validationEngine.min.js"></script>

<script type="text/javascript">
var table = '<?= $_table ?>';
$.fn.serializeObject = function()
{
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

/**
 * submit form
 * @param frm：form对象
 * file：处理文件名，不带后辍
 * method：方法
 * @param success：服务器提示成功时，回调函数，可略
 * @param fail：服务器提示成功时，回调函数，可略
 * @returns false，用来取消form提交
 */
function submitit( frm, file, method, success, fail )
{
	var hook = function( data )
	{
		if( !success )
		{
			success = function()
			{
				location = file + '.php';
			}
		}

		if( !fail )
		{
			fail = function( data )
			{
				alert( '{memo}(代码：{code})'.format_key( data ) );
			}
		}

		if( data.code == 1 )
		{
			success( data );
		}
		else
		{
			fail( data );
		}
	};

	if($(frm).validationEngine("validate"))
	{
		var data = $(frm).serializeObject();
		post( file, method, data, hook );
	}

	return false;
}

$(function(){
	var main = $('#main');

	main.find('a[href="back"]').addClass('btn').click(function(){
		history.back();
		return false;
	});

	main.find('a[do]').addClass('btn btn-primary').click(function(){
		submitit( $(this).closest('form'), table, $(this).attr('do') );

		return false;
	});

	$("form").validationEngine();
});
</script>