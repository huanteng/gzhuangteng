<? box( 'top',  array( 'title' => '编辑焦点图片' ), 86400 ); ?>
<?php include_once('_edit.php'); ?>
	<div class="row-fluid sortable">
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<h2><i class="icon-th-list"></i> 图片管理</h2>
				<div class="box-icon"><a href="#" onclick="history.back();" class="btn btn-round"><i class="icon-remove"></i></a></div>
			</div>
			<div class="box-content">
				<form onsubmit="return submitit(this, 'focus_photo', 'edit_save' );" >
					<table class="table table-striped table-bordered">
						<tr><td>id</td><td><input type="text" name="id" value="<?=$id?>" /></td><td></td></tr>
						<tr><td>标题</td><td><input type="text" name="title" value="<?=$title?>" /></td><td></td></tr>
						<tr><td>排序</td><td><input type="text" name="seq" value="<?=$seq?>" /></td><td></td></tr>
						<tr><td>类型</td><td>
								<select name="type">
									<?php foreach ($typename as $k => $v) { ?>
										<option  value="<?= $k ?>" <?php if ($type == $k) echo "selected=selected"; ?>><?= $v ?></option>
									<?php } ?>
								</select>
							</td><td></td></tr>
<!--						<tr><td>内容</td><td><input type="text" name="content" value="--><?//=$content?><!--"/></td><td></td></tr>-->
<!--						<tr><td>链接</td><td><input type="text" name="url" value="--><?//=$url?><!--"  /></td><td></td></tr>-->
					</table>
					<div class="form-actions">
						<button type="submit" class="btn btn-primary">保存</button>
						<a href="focus_photo.php" onclick="history.back()" class="btn">返回</a>
					</div>
				</form>
			</div>
		</div><!--/span-->

	</div><!--/row-->
	<link rel="stylesheet" href="css/validationEngine.jquery.css">
	<script src="js/jquery.validationEngine-zh-CN.js"></script>
	<script src="js/jquery.validationEngine.min.js"></script>
	<script>$("form").validationEngine();</script>
<?php box( 'bottom', '', 86400 ); ?>