<? box( 'top',  array( 'title' => '快捷方式' ), 86400 ); ?>
<?php include_once('_list.php'); ?>

<script type="text/javascript">
	function update()
	{
		alert( '更新成功。今后所访问的所有页面，所涉及的缓存均得以更新。直至关闭本浏览器为止。' );
		open( '/?cache=new' );

		return false;
	}
	function clear_cache()
	{
		if( confirm( '清除缓存后，所有缓存需要重建，开始时会导致访问缓存，继续么？' ) )
		{
			post('data', 'clear',{});
		}

		return false;
	}
</script>

<div class="row-fluid sortable">
	<div class="box span12">
	  <div class="box-header well" data-original-title>
			<h2><i class="icon-th-list"></i>缓存管理</h2>
	    </div>
		</div><!--/span-->

	<fieldset>

		<legend>说明：清理CND缓存。</legend>
		<li>
			<form  method="post" name="edit_form">
				<p>
<textarea cols="9" rows="8" style="width: 400px" id="url_list" name="url_list">
http://www.999zq.com/banner/
http://www.999zq.com/assets/
</textarea>
				</p>
				<input type="hidden" name="method" value="update_cdn">
				<button type="submit"  class="btn btn-primary">更新CDN</button>
			</form>
		</li>

		<legend>说明：本页面用来更新或清空缓存。</legend>
<!--		<li><a href="#" onclick="return update();">更新缓存</a></li>-->
		<li><a href="#" onclick="return clear_cache();">清空缓存：</a>将缓存目录清空，以节省空间，也达到更新缓存的目的</li>
	</fieldset>
</div><!--/row-->
	<link rel="stylesheet" href="css/validationEngine.jquery.css">
	<script src="js/jquery.validationEngine-zh-CN.js"></script>
	<script src="js/jquery.validationEngine.min.js"></script>
	<script>$("form").validationEngine();</script>
<?php box( 'bottom', array("back2"), 86400 ); ?>