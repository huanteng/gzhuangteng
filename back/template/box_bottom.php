
</div><!--/fluid-row-->
</div><!--/.fluid-container-->

<script type="text/javascript">
	// 20150331，在前台单入口改过结构，在此恢复。以后单入口时，取消本函数
	/* 使用ajax方式post，将json结果交回调函数处理
	 * file：接受post的文件名
	 * data：postdata，为key=value数组
	 * success, error：回调函数
	 */
	function post(file, method, data, success, error)
	{
	var url = file + '.php';

	if( !data ) data = {};
	data.method = method;

		if( !data ) data = {};

		$.ajax
		({
			type : "POST",
			url : url,
			dataType : 'json',
			async : false,
			data : data,
			success : function(data) {
				if( success )
				{
					success(data);
				}
				else
				{
					alert( data.memo + '(代码=' + data.code + ')' );
				}
			},
			error : function (jqXHR, textStatus, errorThrown) {
				if( error )
				{
					error (jqXHR, textStatus, errorThrown);
				}
				else
				{
					//alert(jqXHR.responseText);
				}
			}
		});

	}
</script>
		
</body>
</html>