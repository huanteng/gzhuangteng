<? box( 'top',  array( 'title' => '日志管理' ), 86400 ); ?>
<?php include_once('_list.php'); ?>

<script src="<?=href( 'js/jquery.contextmenu.r2.packed.js' )?>"></script>
<script type="text/javascript">
/**
 * 服务器处理后函数
 */
function after_post( log_id, data )
{
	if( data.code == 1 )
	{
		$('#tr' + log_id).hide();
		$('#tr' + log_id + '_2').hide();

		if( $('table').find('tbody').children(':visible').length == 0 )
		{
			reload();
		}
	}
	else
	{
		alert( '{memo}(code={code})'.format_key( data ) );
	}
}

/*统一的log post函数
 * 参数：
 * 	btn：当前按钮对象，由本对象获得log id
 * 	method：方法
 * 	data：发送数据，不必传入 log id，本函数会自动取
 */
function do_post_log( btn, data )
{
	var id = $(btn).closest('tr[log_id]').attr('log_id');
	data.id = id;
	var func = function( data )
	{
		after_post( id, data );
	}

	post( 'log', 'do_module', data, func )
}

// 右键时，当前行id
var context_menu_id = 0;

function banch_del( field )
{
	if( confirm( '将会执行批量删除，并且不可恢复。\n真的确认么？' ) )
	{
		post( 'log', 'banch_del', { field: field, id: context_menu_id }, reload );
	}
}

function banch_modify()
{
	//参考批量删除函数，但暂时只做 批量修改错误log为警告log
	if( confirm( '将会执行批量修改，并且不可恢复。\n真的确认么？' ) )
	{
		post( 'log', 'banch_modify', { id: context_menu_id }, reload );
	}
}

$( document ).ready( function() {
	$( "td[context_menu_type='log']" ).contextMenu( 'context_menu_log', {
		onContextMenu : function( e )
		{
			context_menu_id = $(e.target).closest('tr').attr('val');
			return true;
		},
		bindings :
		{
			'del_by_module' : function( t )
			{
				banch_del( 'module' );
			},
			'del_by_content' : function( t )
			{
				banch_del( 'content' );
			},

			'del_by_type' : function( t )
			{
				banch_del( 'type' );
			},
			'del_by_code' : function( t )
			{
				banch_del( 'code' );
			},
			'del_by_ip' : function( t )
			{
				banch_del( 'ip' );
			},
			'del_by_all' : function( t )
			{
				banch_del( 'all' );
			},
			'modify_by_module' : function( t )
			{
				banch_modify();

			}
		}
	});
});
</script>

<div class="contextMenu hide" id="context_menu_log">
	<ul>
		<li id="del_by_module">按模块删除</li>
		<li id="del_by_content">按内容删除</li>
		<li id="del_by_type">按类型删除</li>
		<li id="del_by_code">按代码删除</li>
		<li id="del_by_ip">按IP删除</li>
<!--		<li id="del_by_all">全部删除</li>-->
		<li id="modify_by_module">错误log改成警告</li>
	</ul>
</div>

<div class="row-fluid">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th-list"></i> log管理</h2>
			<div class="box-icon"><a href="javascript:location.reload();" class="btn btn-round"><i class="icon-refresh"></i></a></div>
		</div>
	</div>
</div>

<form id="search">
		<div class="input-append">
			模块：<?=$module?>
			类型：<?=$type?>
			相关id：
			<input name="relate_id" type="text" id="relate_id" style="width:90px;" />
			代码：
			<input name="code" type="text" id="code" style="width:90px;" />
		  IP：
		  <input name="ip" type="text" id="ip" size="16" style="width:90px;" />
		  关键字：
		  <input name="q" size="16" type="text" style="width:90px;">
			<? if( isset($hide) ) echo '<input name="hide" type="hidden">'; ?>

			日期：<input type="text" id="begin_time" name="begin_time" style="width:90px;">
			到：<input type="text" id="end_time" name="end_time" style="width:90px;" >

			<button class="btn btn-success" type="submit">搜索</button>
		 </div>
</form>

<table class="table table-striped table-bordered" id="main">
			  <thead>
				  <tr>
			<th width="5"><input type="checkbox" /></th>
					  <th><? up_down( 'id', 'ID' ) ?></th>
					  <th><? up_down( 'module', '模块' ) ?></th>
					  <th><? up_down( 'content', '内容' ) ?></th>
					  <th><? up_down( 'type', '类型' ) ?></th>
					  <th><? up_down( 'relate_id', '相关id' ) ?></th>
					  <th><? up_down( 'code', '代码' ) ?></th>
					  <th><? up_down( 'ip', 'IP' ) ?></th>
					  <th><? up_down( 'time', '时间' ) ?></th>
					  <th>操作</th>
				  </tr>
			  </thead>
			  <tbody>
				<?php foreach($data as $k=>$v){?>
		<tr val="<?= $v['id'] ?>">
			<td><input type="checkbox" /></td>
					<td><?=$v['id']?></td>
					<td><?=$v['module']?></td>
			        <td context_menu_type="log"><?php
						echo $v['content'];

						if( !empty( $v['table'] ) )
						{
							echo '&nbsp;<a href="#" onclick="$(\'#tr' . $v[ 'id' ] . '_2\').toggle();return false">详情</a>';
						}
						?>
					</td>
					<td><?=$v['type']?></td>
					<td><a href="<?=$v['relate_url']?>" target="_blank"><?=$v['relate_id']?></a></td>
					<td><?=$v['code']?></td>
					<td><a href="http://ip138.com/ips138.asp?ip=<?=$v['ip']?>&action=2" target="_blank"><?=$v['ip']?></a></td>
					<td><?=$v['time']?></td>
		            <td context_menu_type="log"></td>
				</tr>
				<?php if( isset( $v['table'] ) ) { ?>
				<tr<? if( isset($hide) ) echo ' class="hide"'; ?> id="tr<?= $v[ 'id' ] ?>_2" log_id="<?= $v[ 'id' ] ?>">
					<td colspan="9"><?= $v[ 'table' ] ?></pre></td>
				</tr>
				<?php } ?>
				<?php }?>
			  </tbody>
</table>
<?php box( 'page', $nav, 0 ); ?>
	
<div id="cmd">
	<a do="delit" >删除</a>
</div>

<fieldset>
	<legend>说明：</legend>
	<li>在“操作”列右键，可以执行批量删除。</li>
	<li>查看日志信息,<a href="?method=sls">请点击此处</a>。</li>
</fieldset>

<?php box( 'bottom', array("back2"), 86400 ); ?>