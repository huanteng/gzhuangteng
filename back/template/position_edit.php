<? box( 'top',  array( 'title' => '编辑广告位' ), 86400 ); ?>
<?php include_once('_edit.php'); ?>

<div class="row-fluid">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th-list"></i> 编辑广告位</h2>
			<div class="box-icon"><a href="javascript:location.reload();" class="btn btn-round"><i class="icon-refresh"></i></a><a href="javascript:history.back()" class="btn btn-round"><i class="icon-remove"></i></a></div>
		</div>
	</div>
</div>

<form id="main">
	<table class="table table-striped table-bordered">
		<tr>
			<td>ID</td>
			<td><?= $id ?></td>
			<td></td>
		</tr>
		<tr>
			<td>名字</td>
			<td><input type="text" name="name" value="<?= $name ?>" class="validate[required,maxSize[20]]"></td>
			<td>*不可重复，建议使用页面+位置名。</td>
		</tr>
		<tr>
			<td>宽</td>
			<td><input type="text" name="width" value="<?= $width ?>" class="validate[custom[integer]]"></td>
			<td>广告位的宽度，0表示不输入</td>
		</tr>
		<tr>
			<td>高</td>
			<td><input type="text" name="height" value="<?= $height ?>" class="validate[custom[integer]]"/> </td>
			<td>广告位的高度，0表示不输入</td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td>窗口</td>
			<td><input type="text" name="target" value="<?=$target ?>" class="validate[maxSize[20]]"></td>
			<td>链接中什么窗口打开？空表示本窗口</td>
		</tr>
		<tr>
			<td>follow</td>
			<td><?=$follow ?></td>
			<td>设置folow，将影响SEO</td>
		</tr>
	</table>
	<div id="cmd">
		<input type="hidden" name="id" value="<?= $id ?>">
		<a do="edit_save">保存</a>
		<a href="back">返回</a>
	</div>
</form>

<!-- 本广告位的广告列表 -->
<div class="row-fluid">
<?php foreach($ad['data'] as $k=>$v){?>

	<li id="image-<?=$k?>" class="thumbnail"><a style="background:url(banner/<?=$v['filename']?>)" title="<?=$v['title']?>" href="banner/<?=$v['filename']?>"><img height="50%" src="banner/<?=$v['filename']?>" title="ID: <?=$v['id']?>, <?=$v['title']?>, <?=$v['url']?>, <?=$v['time']>0?date('Y-m-d H:i',$v['time']):'不限期'?>"/></a></li>
	<a href="ad.php?method=edit&id=<?= $v['id'] ?>">编辑广告</a>&nbsp;&nbsp;<a href="ad.php?id=<?= $v['id'] ?>">跳转到列表删除此广告</a>

<?php }?>
	<?php box( 'page', $ad['nav'], 0 ); ?>
</div>
<!-- 本广告位的广告列表end -->

<fieldset>
	<legend>&nbsp;</legend>
	<li>删除位置前，必须先删除对应位置里的广告；</li>
</fieldset>

<?php box( 'bottom', '', 86400 ); ?>