<? box( 'top',  array( 'title' => '增加广告' ), 86400 ); ?>
<?php include_once('_edit.php'); ?>

<div class="row-fluid">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th-list"></i> 增加广告</h2>
			<div class="box-icon"><a href="javascript:history.back()" class="btn btn-round"><i class="icon-remove"></i></a></div>
		</div>
	</div>
</div>

<form id="main">
	<table class="table table-striped table-bordered">
	  <tr>
        <td>广告位</td>
	    <td><select name="position_id" style="width:200px;" class="validate[required]">
            <option value="">-</option>
            <?
					$content = '';
					foreach( $position as $k => $v )
					{
						$content .= '<option value="' . $k . '">' . $v[ 'name' ] . $v[ 'width' ] . '*' . $v[ 'height' ] . '</option>';
					}
					echo $content;
					?>
          </select>        </td>
	    <td>*如需管理广告位，<a href="position.php" target="_blank">请点击这里</a></td>
      </tr>
	  <tr>
        <td>地址</td>
	    <td><input type="text" name="url" class="validate[maxSize[200]]" /></td>
	    <td>空表示不跳转</td>
      </tr>
	  <tr>
        <td>标题</td>
	    <td><input type="text" name="title" class="validate[maxSize[200]]" />        </td>
	    <td>为了用户体验及SEO，建议输入标题</td>
      </tr>
	  <tr>
        <td colspan="3">&nbsp;</td>
      </tr>
	  <tr>
        <td>优先级</td>
	    <td><?=$level ?></td>
	    <td>同一广告位下，仅显示最高优先级的广告。如果存在多个，会随机显示。</td>
      </tr>
	  <tr>
        <td>备注</td>
	    <td><input type="text" name="remark" class="validate[maxSize[200]]" /></td>
	    <td>本备注仅在后台显示，便于管理</td>
      </tr>
	  <tr>
        <td>过期时间</td>
	    <td><input type="text" class="input-xlarge datetimepick" name="expires" id="expires" style="width:210px;"/></td>
	    <td>过期后的广告会被自动删除，空表示不过期</td>
      </tr>
	</table>
<div id="cmd">
		<a do="add_save">保存</a>
		<a href="back">返回</a>
	</div>
</form>

<fieldset>
	<legend>&nbsp;</legend>
	<li>删除位置前，必须先删除对应位置里的广告；</li>
</fieldset>

<?php box( 'bottom', '', 86400 ); ?>