<!DOCTYPE html>
<html lang="en">
<head>
<!--
	Charisma v1.0.0

	Copyright 2012 Muhammad Usman
	Licensed under the Apache License v2.0
	http://www.apache.org/licenses/LICENSE-2.0

	http://usman.it
	http://twitter.com/halalit_usman
-->
<meta charset="utf-8">
<title><?= $title ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
<meta name="author" content="Muhammad Usman">

<!-- The styles -->
<link id="bs-css" href="<?= href('css/bootstrap-cerulean.css') ?>" rel="stylesheet">
<style type="text/css">
  body {
	padding-bottom: 40px;
  }
  .sidebar-nav {
	padding: 9px 0;
  }
</style>
<link href="<?= href('css/bootstrap-responsive.css') ?>" rel="stylesheet">
<link href="<?= href('css/charisma-app.css') ?>" rel="stylesheet">
<link href="<?= href('css/jquery-ui-1.8.21.custom.css') ?>" rel="stylesheet">
<link href='<?= href('css/fullcalendar.css') ?>' rel='stylesheet'>
<link href='<?= href('css/fullcalendar.print.css') ?>' rel='stylesheet'  media='print'>
<link href='<?= href('css/chosen.css') ?>' rel='stylesheet'>
<link href='<?= href('css/uniform.default.css') ?>' rel='stylesheet'>
<link href='<?= href('css/colorbox.css') ?>' rel='stylesheet'>
<link href='<?= href('css/jquery.cleditor.css') ?>' rel='stylesheet'>
<link href='<?= href('css/jquery.noty.css') ?>' rel='stylesheet'>
<link href='<?= href('css/noty_theme_default.css') ?>' rel='stylesheet'>
<link href='<?= href('css/elfinder.min.css') ?>' rel='stylesheet'>
<link href='<?= href('css/elfinder.theme.css') ?>' rel='stylesheet'>
<link href='<?= href('css/jquery.iphone.toggle.css') ?>' rel='stylesheet'>
<link href='<?= href('css/opa-icons.css') ?>' rel='stylesheet'>
<link href='<?= href('css/uploadify.css') ?>' rel='stylesheet'>

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- The fav icon -->
<link rel="shortcut icon" href="<?= href('img/back.ico') ?>">

<!-- jQuery -->
<script src="<?= href('js/jquery-1.7.2.min.js') ?>"></script>
<script src="<?= href('/js/common.js') ?>?20160114"></script>
<!-- jQuery UI -->
<script src="<?= href('js/jquery-ui-1.8.21.custom.min.js') ?>"></script>
<!-- transition / effect library -->
<script src="<?= href('js/bootstrap-transition.js') ?>"></script>
<!-- alert enhancer library -->
<script src="<?= href('js/bootstrap-alert.js') ?>"></script>
<!-- modal / dialog library -->
<script src="<?= href('js/bootstrap-modal.js') ?>"></script>
<!-- custom dropdown library -->
<script src="<?= href('js/bootstrap-dropdown.js') ?>"></script>
<!-- scrolspy library -->
<script src="<?= href('js/bootstrap-scrollspy.js') ?>"></script>
<!-- library for creating tabs -->
<script src="<?= href('js/bootstrap-tab.js') ?>"></script>
<!-- library for advanced tooltip -->
<script src="<?= href('js/bootstrap-tooltip.js') ?>"></script>
<!-- popover effect library -->
<script src="<?= href('js/bootstrap-popover.js') ?>"></script>
<!-- button enhancer library -->
<script src="<?= href('js/bootstrap-button.js') ?>"></script>
<!-- accordion library (optional, not used in demo) -->
<script src="<?= href('js/bootstrap-collapse.js') ?>"></script>
<!-- carousel slideshow library (optional, not used in demo) -->
<script src="<?= href('js/bootstrap-carousel.js') ?>"></script>
<!-- autocomplete library -->
<script src="<?= href('js/bootstrap-typeahead.js') ?>"></script>
<!-- tour library -->
<script src="<?= href('js/bootstrap-tour.js') ?>"></script>
<!-- library for cookie management -->
<script src="<?= href('js/jquery.cookie.js') ?>"></script>
<!-- calander plugin -->
<script src='<?= href('js/fullcalendar.min.js') ?>'></script>
<!-- data table plugin -->
<script src='<?= href('js/jquery.dataTables.min.js') ?>'></script>

<!-- chart libraries start -->
<script src="<?= href('js/excanvas.js') ?>"></script>
<script src="<?= href('js/jquery.flot.min.js') ?>"></script>
<script src="<?= href('js/jquery.flot.pie.min.js') ?>"></script>
<script src="<?= href('js/jquery.flot.stack.js') ?>"></script>
<script src="<?= href('js/jquery.flot.resize.min.js') ?>"></script>
<!-- chart libraries end -->

<!-- select or dropdown enhancer -->
<script src="<?= href('js/jquery.chosen.min.js') ?>"></script>
<!-- checkbox, radio, and file input styler -->
<script src="<?= href('js/jquery.uniform.min.js') ?>"></script>
<!-- plugin for gallery image view -->
<script src="<?= href('js/jquery.colorbox.min.js') ?>"></script>
<!-- rich text editor library -->
<script src="<?= href('js/jquery.cleditor.min.js') ?>"></script>
<!-- notification plugin -->
<script src="<?= href('js/jquery.noty.js') ?>"></script>
<!-- file manager library -->
<script src="<?= href('js/jquery.elfinder.min.js') ?>"></script>
<!-- star rating plugin -->
<script src="<?= href('js/jquery.raty.min.js') ?>"></script>
<!-- for iOS style toggle switch -->
<script src="<?= href('js/jquery.iphone.toggle.js') ?>"></script>
<!-- autogrowing textarea plugin -->
<script src="<?= href('js/jquery.autogrow-textarea.js') ?>"></script>
<!-- multiple file upload plugin -->
<script src="<?= href('js/jquery.uploadify-3.1.min.js') ?>"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="<?= href('js/jquery.history.js') ?>"></script>
<!-- application script for Charisma demo -->
<script src="<?= href('js/charisma.js') ?>"></script>
<script type="text/javascript">
	var URL = <?= json_encode( $url ) ?>;
</script>
</head>

<body>

<div class="container-fluid">
	<div class="row-fluid">
				
