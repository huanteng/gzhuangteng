<? box( 'top',  array( 'title' => '增加文章' ), 86400 ); ?>

<div class="row-fluid">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-th-list"></i> 文章管理--添加新闻</h2>
            <div class="box-icon">
                <a href="news.php" class="btn btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>

            
           

        </div>
    </div><!--/span-->

 <script charset="utf-8" src="../kindeditor/kindeditor.js"></script>
            <script charset="utf-8" src="../kindeditor/lang/zh_CN.js"></script>
            <script charset="utf-8" src="../kindeditor/plugins/code/prettify.js"></script>
            <script>
                KindEditor.ready(function(K) {
                    var editor1 = K.create('textarea[name="content"]', {
                        cssPath : '../kindeditor/plugins/code/prettify.css',
                        uploadJson : '../kindeditor/php/upload_json.php',
                        fileManagerJson : '../kindeditor/php/file_manager_json.php',
                        allowFileManager : true,
                        afterCreate : function() {
                            var self = this;
                            K.ctrl(document, 13, function() {
                                self.sync();
                                K('form[name=edit_form]')[0].submit();
                            });
                            K.ctrl(self.edit.doc, 13, function() {
                                self.sync();
                                K('form[name=edit_form]')[0].submit();
                            });
                        }
                    });
                    prettyPrint();
                });
            </script>
            <form action="news.php" method="post" name="edit_form">
                <table class="table table-striped table-bordered">
                    <tr><td>标题</td><td><input type="text" name="title" value="" class="validate[required]"></td></tr>
                    <tr><td>栏目</td><td><?= $type_select?></td></tr>
                    <tr><td>内容</td><td>
                            <textarea name="content" style="width:680px;height:400px;visibility:hidden;" class="validate[required]"></textarea>
                        </td></tr>
                    <tr><td>置顶</td><td><input type="text" name="hot"></td><td></td></tr>
                    <tr><td>来源地址</td><td><input type="text" name="from" value=""></td></tr>

                    <tr><td colspan="2" class="form-actions">
                            <input type="hidden" name="method" value="add_save">
                            <button type="submit" class="btn btn-primary">保存</button> <a href="news.php" class="btn">返回</a></td></tr>
                </table> 
            </form>
<link rel="stylesheet" href="css/validationEngine.jquery.css">
<script src="js/jquery.validationEngine-zh-CN.js"></script> 
<script src="js/jquery.validationEngine.min.js"></script>
<script>$("form").validationEngine();</script>
<?php box( 'bottom', array("back2"), 86400 ); ?>