<?php
/** 在表头显示向上向下箭头
 * @param $field：字段名
 * @param $text：显示文字
 */
function up_down( $field, $text )
{
	// 除orderby参数外的url
	static $base_url = '';

	if( $base_url == '' )
	{
		$temp = $_GET;
		$s = array();
		foreach( $temp as $k => $v )
		{
			if( $k != 'orderby' )
			{
				if( is_array( $v ) )
				{
					foreach( $v as $v2 )
					{
						$s[] = $k . '[]=' . $v2;
					}
				}
				else
				{
					$s[] = $k . '=' . $v;
				}
			}
		}
		$base_url = '?' . ( count( $s ) ? implode( '&', $s ) . '&' : '' ) . 'orderby=' ;
	}

	// 旧排序
	$old = value( $_GET, 'orderby' );
	$old = explode( ' ', $old );

	// 新排序
	$new1 = explode( ' ', $field );

	if( $old[ 0 ] == $new1[ 0 ] )
	{
		if( isset( $old[ 1 ] ) )
		{
			$class = 'icon-arrow-down';
			$field = $old[ 0 ];
		}
		else
		{
			$class = 'icon-arrow-up';
			$field = $old[ 0 ] . ' desc';
		}
	}
	else
	{
		//$class = 'icon-resize-vertical';
		$class = '';
	}

	echo load('str')->format_key( '<a href="{base_url}{field}">{text}<i class="{class}"></i></a>',
		array( 'base_url' => $base_url, 'field' => $field, 'text' => $text, 'class' => $class )
	);
}
?>

<script type="text/javascript">
var table = '<?= $_table ?>';
$(function(){
	<?php
	$s = 'var frm=$("form:first");';
	foreach( $_GET as $k => $v )
	{
		if( is_array( $v ) )
		{
			foreach( $v as $v2 )
			{
				$s .= "frm.find('input[name=\"" . $k . "[]\"][value=\"" . $v2 . "\"]').attr('checked', true).parent('span').toggleClass('checked');";
			}
		}
		else
		{
			$s .= "frm.find('[name=\"$k\"]').val('" . addslashes( $v ) ."');";
		}
	}
	echo $s;
	?>

	var main = $('#main');
	var key = get_closet_attr( this, 'table', 'key', 'id' );

	//全选
    main.find(':checkbox:first').click(function(){
        if( $(this).attr('checked') ){
        	main.find('tbody :checkbox').attr('checked', true).parent('span').toggleClass('checked');
        } else {
			main.find('tbody :checkbox').attr('checked', false).parent('span').removeClass('checked');
        }
    });

	main.find('a[do]').each(function(){
		var func = $(this).attr('do');
		var method = $(this).attr('method');
		var id = get_closet_attr( this, 'tr', 'val', '' );

		$(this).attr( 'href', func + ' ' + id ).click( function() {
			if( method )
			{
				tr_post( method, id );
			}
			else
			{
				window[ func ]( id );
			}

			return false;
		});
	});

	main.find('a[href="edit"]').each(function(){
		var id = get_closet_attr( this, 'tr', 'val', '' );
		this.href = table + '.php?method=edit&' + key + '=' + id;
	});

	main.find('a[link]').each(function(){
		var id = get_closet_attr( this, 'tr', 'val', '' );
		this.href = '{0}.php?method={1}&{2}={3}'.format( table, $(this).attr('link'), key, id );
	});

	var cmd = $('#cmd');
	cmd.find('a[do]').each(function(){
		var method = $(this).attr('do');

		var class1 = method == 'delit' ? 'btn btn-danger' : 'btn btn-info';

		$(this).addClass(class1).click(function(){
			var selected = main.find('tbody :checkbox:checked');

			if( selected.length == 0 )
			{
				alert('请先选择要处理的行');
				return false;
			}

			var val = [];
			selected.each(function(){
				val.push( $(this).closest('tr').attr('val') );
			});

			var func = window[ method ] || function( count, id ) {
					var msg = $(event.srcElement).attr( 'confirm' );
					if( msg )
					{
						msg = msg.format_key( {count: count, id: id} );

						if( !confirm( msg ) )
						{
							return;
						}
					}

					tr_post( method, id );
				};
			func( selected.length, val.join(',') );
		});
	});

	cmd.find('a[href]').addClass('btn btn-info').each(function(){
		this.href = '?method=' + $(this).attr( 'href' );
	});
});

/**
 * 行提交
 * @param method：方法
 * @param value：行id值
 * @param callback：回调，默认为reload
 */
function tr_post( method, value, callback )
{
	var key = get_closet_attr( $('#main'), 'table', 'key', 'id' );
	var data = {};
	data[ key ] = value;
	if( !callback )
	{
		callback = function( data )
		{
			alert( data.memo );
			reload();
		}
	}
	post( table, method, data, callback );
}

function ghost( id )
{
	if( confirm('真要克隆本行吗？') )
	{
		tr_post( 'ghost', id );
	}
}

/**
 * （单个或多个）删除。
 * 20141127，因del()被占用，使用delit，以后请将旧写法删除，并将本函数改叫del
 * @param count
 * @param id：如果是多个，用半角逗号隔开
 */
function delit( count, id )
{
	if( confirm( '真的删除已选择的 ' + count + ' 行吗？' ) )
	{
		tr_post( 'del', id );
	}
}

function del( file, id )
{
	var func = function( data ) {
		if( data.code == 1 )
		{
			reload();
		}
		else
		{
			alert( '{memo}(代码：{code})'.format_key( data ) );
		}
	};

	if( confirm('真的删除吗？') )
	{
		post( file, 'del', { id: id }, func );
	}
}

function reload()
{
	location.reload();
}
</script>