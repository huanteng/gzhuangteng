<? box( 'top',  array( 'title' => '文章管理' ), 86400 ); ?>
<?php include_once('_list.php'); ?>

<div class="row-fluid">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-th-list"></i> 文章管理</h2>
			<div class="box-icon"><a href="news.php" class="btn btn-success"><i class="icon-refresh"></i></a></div>
        </div>

        </div>
    </div><!--/span-->

<form action="" method="get" id="transForm" name="transForm">
                <div class="input-append">
                    标题关键字：
                    <input id="appendedInputButton" name="q" size="16" type="text" value="<?= $q ?>" style="width:90px;">
                    <?= isset($type_select) ? $type_select : '' ?>
                    <button class="btn btn-success" type="submit" onclick="return checkkey();">搜索</button>
                    <a href="news.php?method=add" class="btn btn-info">增加</a>
                </div>
            </form>


            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th><? up_down( 'id', 'ID' ) ?></th>
                        <th><? up_down( 'title', '标题' ) ?></th>
                        <th><? up_down( 'type', '栏目' ) ?></th>
                        <th><? up_down( 'time', '时间' ) ?></th>
                        <th>操作</th>
                    </tr>
                </thead>   
                <tbody>
					<?php foreach ($data as $k => $v) { ?>  
						<tr>
							<td><?= $v['id'] ?></td>
							<td class="center"><a href="http://www.999zq.com/news/<?= $v['id'] ?>" target="_blank"><?= $v['title'] ?></a></td>
							<td class="center"><?php echo $type[$v['type']]; ?></td>
							<td class="center"><?php echo date("Y-m-d", $v['time']); ?></td>
							<td class="center">
								<a href="?method=edit&id=<?= $v['id'] ?>">

									编辑
								</a>
								<a  href="?method=del&id=<?= $v['id'] ?>" onclick="return confirm('真的删除吗？')">

									删除
								</a>
							</td>
						</tr>
					<?php } ?>  
                </tbody>
            </table> 
			<?php box( 'page', $nav, 0 ); ?>

<?php box( 'bottom', array("back2"), 86400 ); ?>