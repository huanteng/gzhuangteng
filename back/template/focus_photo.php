<? box( 'top',  array( 'title' => '焦点图片' ), 86400 ); ?>
<?php include_once('_list.php'); ?>
<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th-list"></i> 焦点图片管理</h2>
			<div class="box-icon"></div>
		</div>
		<div class="box-content">
			<form id="search">
				<div class="input-append">
					关键字：
					<input name="q" type="text" style="width:80px;">
					<?= $typename ?>
					<button class="btn btn-success" type="submit">搜索</button>
				</div>
			</form>
			<table class="table table-striped table-bordered"  id="main">
				<thead>
				<tr>
					<th width="5"><input type="checkbox" /></th>
					<th><? up_down( 'id', 'ID' ) ?></th>
					<th><? up_down( 'title', '标题' ) ?></th>
					<th><? up_down( 'file', '图片' ) ?></th>
					<th><? up_down( 'seq', '排序' ) ?></th>
					<th><? up_down( 'type', '类型' ) ?></th>
					<th>操作</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($data as $k => $v) { ?>
					<tr val="<?= $v['id'] ?>">
						<td><input type="checkbox" /></td>
						<td><?= $v['id'] ?></td>
						<td class="center"><a href="<?= $v['url'] ?>" target="_blank"><?=$v['title']  ?></a></td>
						<td id="image-<?=$k?>" class="thumbnail"><a style="background:url(/photo/<?=$v['file']?>)" title="<?=$v['title']?>" href="/photo/<?=$v['file']?>"><img  height="50"
																						  src="/photo/<?=$v['file']?>"/></a></td>
						<td class="center"><?=$v['seq']?></td>
						<td class="center"><?=$v['typename']?></td>
						<td width="150">
							<a href="edit">编辑</a>
							<a href="focus_photo.php?method=upload&id=<?= $v['id'] ?>" >上传</a>
						</td>
					</tr>
				<?php }?>
				</tbody>
			</table>
			<?php box( 'page', $nav, 0 ); ?>
			<div id="cmd">
				<a href="add">增加</a>
				<a do="delit" title="删除所选图片" >删除</a>
			</div>

		</div>
	</div><!--/span-->
	<?php box( 'bottom', '', 86400 ); ?>
