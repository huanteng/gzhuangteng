<? box( 'top',  array( 'title' => '编辑广告' ), 86400 ); ?>
<?php include_once('_edit.php'); ?>

<div class="row-fluid">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th-list"></i> 编辑广告</h2>
			<div class="box-icon"><a href="javascript:history.back()" class="btn btn-round"><i class="icon-remove"></i></a></div>
		</div>
	</div>
</div>

<form method="post" action="ad_pic_upload.php" enctype="multipart/form-data">
	<table class="table table-striped table-bordered">
		<tr>
			<td>
				<? if (empty($_GET['img'])){?>
					<img id="preview" src="/banner/<?=$filename?>?r=<?=time()?>">
				<? }else{?>
					<img id="preview" src="/banner/<?=$_GET['img'] ?>?r=<?=time()?>">
				<? }?>


				<br />
				<input name="file" id="file" type="file" ><br />
				<input name="token" type="hidden" value="<?=$upToken?>">
				<input type="hidden" name="x:method" value="ad" >
				<input type="hidden" name="x:id" value="<?= $id ?>" >
				<input type="hidden" name="key" value="<?= $key ?>">
				<button type="submit" class="btn btn-primary"  type="submit"  name='submit' value='保存'>保存</button>
			</td>
		</tr>
	</table>
</form>
<form id="main">
	<table class="table table-striped table-bordered">
		<tr>
			<td>ID</td>
			<td><?= $id ?></td>
			<td></td>
		</tr>
		<tr>
			<td>广告位</td>
			<td>
				<select name="position_id" style="width:200px;" class="validate[required]">
					<option value="">-</option>
					<?
					$content = '';
					foreach( $position as $k => $v )
					{
						$content .= '<option value="' . $k . '"';
						if( $k == $position_id )
						{
							$content .= ' selected="selected"';
						}

						$content .= '>' . $v[ 'name' ] . $v[ 'width' ] . '*' . $v[ 'height' ] . '</option>';
					}
					echo $content;
					?>
				</select>
			</td>
			<td>*如需管理广告位，<a href="position.php" target="_blank">请点击这里</a></td>
		</tr>
		<tr>
			<td>地址</td>
			<td><input type="text" name="url" value="<?= $url ?>" class="validate[maxSize[200]]" /></td>
			<td>空表示不跳转</td>
		</tr>
		<tr>
			<td>标题</td>
			<td><input type="text" name="title" value="<?= $title ?>" class="validate[maxSize[200]]" /> </td>
			<td>为了用户体验及SEO，建议输入标题</td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td>优先级</td>
			<td><?=$level ?></td>
			<td>同一广告位下，仅显示最高优先级的广告。如果存在多个，会随机显示。</td>
		</tr>
		<tr>
			<td>备注</td>
			<td><input type="text" name="remark" value="<?=$remark ?>" class="validate[maxSize[200]]" ></td>
			<td>本备注仅在后台显示，便于管理</td>
		</tr>
		<tr>
			<td>过期时间</td>
			<td>
				<input type="text" class="input-xlarge datetimepick" name="expires" id="expires" style="width:210px;"/>
			</td>
			<td>过期后的广告会被自动删除，空表示不过期</td>
		</tr>
		<tr>
			<td>时间</td>
			<td><?= $time ?></td>
			<td>&nbsp;</td>
		</tr>
	</table>
	<div id="cmd">
		<? if (empty($_GET['img'])){?>
		<input type="hidden" name="filename" value="<?= $filename ?>" class="validate[maxSize[200]]" />
		<? }else{?>
		<input type="hidden" name="filename" value="<?= $_GET['img'] ?>" class="validate[maxSize[200]]" />
		<? }?>

		<input type="hidden" name="id" value="<?= $id ?>">
		<a do="edit_save">保存</a>
		<a href="back">返回</a>
	</div>
</form>

<fieldset>
	<legend>&nbsp;</legend>
	<li></li>
</fieldset>

<script src="<?= url( '/js/jquery-ui-timepicker-addon.js' ) ?>"></script>
<script>
	$(function(){
		$('#expires').datetimepicker({
			dateFormat: "yy-mm-dd",
			timeFormat: "HH:mm"
		});

		/*
		实现预览图，但没生效，暂注释
		$("#file").change(function(){
			$("#preview").attr("src","file:///"+$("#file").val());
		})
		*/
	});
</script>

<?php box( 'bottom', '', 86400 ); ?>