<? box( 'top',  array( 'title' => '增加图片' ), 86400 ); ?>
<?php include_once('_edit.php'); ?>
	<div class="row-fluid sortable">
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<h2><i class="icon-th-list"></i>增加图片</h2>
				<div class="box-icon"><a href="?" class="btn btn-round"><i class="icon-remove"></i></a></div>
			</div>
			<div class="box-content">
				<form onsubmit="return submitit(this, 'focus_photo', 'add_save' );" id="main">
					<table class="table table-striped table-bordered">
						<tr>
							<td>类型</td><td>
								<select name="type">
									<?php foreach ($typename as $k => $v) { ?>
										<option  value="<?= $k ?>"><?= $v ?></option>
									<?php } ?>
								</select></td>
							<td></td>
						</tr>
						<tr><td>标题</td><td><input type="text" name="title" value="" class="validate[required]"/></td><td></td></tr>
						<tr><td>排序</td><td><input type="text" name="seq" value=""/></td><td></td></tr>
<!--						<tr><td>内容</td><td><input type="text" name="content" value="" /></td><td></td></tr>-->
<!--						<tr><td>链接</td><td><input type="text" name="url" value=""/></td><td></td></tr>-->
						<input type="hidden" name="id" value="<?=$id?>">
					</table>
					<div class="form-actions" id="cmd">
						<input type="submit" class="btn btn-primary" value="保存" />
						<a href="back"  onclick="history.back()" >返回</a>
					</div>
				</form>
			</div>
		</div><!--/span-->

	</div><!--/row-->
	<link rel="stylesheet" href="css/validationEngine.jquery.css">
	<script src="js/jquery.validationEngine-zh-CN.js"></script>
	<script src="js/jquery.validationEngine.min.js"></script>
	<script>$("form").validationEngine();</script>
<?php box( 'bottom', '', 86400 ); ?>