<? box( 'top',  array( 'title' => '上传焦点图片' ), 86400 ); ?>
<?php include_once('_edit.php'); ?>
<?php $id=$_GET['id'];?>
<div class="row-fluid">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th-list"></i> 图片上传</h2>
			<div class="box-icon"><a href="#" onclick="history.back();" class="btn btn-round"><i class="icon-remove"></i></a></div>
		</div>
	</div>
</div>
<!--/span-->

<form  method="post" action="focus_photo_upload.php"  enctype="multipart/form-data">
	<table class="table table-striped table-bordered">
		<tr>
			<td>图片名称</td>
			<td><?=$info['title']?><input  type="hidden" name="x:id" id="x:id" value="<?=$info['id']?>"></td>
			<td>*图片名称不可修改</td>
		</tr>
		<tr>
			<td>图片</td>

			<td><img src="/photo/<?=$info['file']?>?d=<?=time()?>" width="100px"/><input type="file" name="file"   /></td>
			<td>*选择上传图片</td>
		</tr>
		<input type="hidden" name="id" value="<?=$id?>">
	</table>
	<div class="form-actions">
		<input type="submit"  name='submit'class="btn btn-primary" value='保存' />
		<a href="focus_photo.php" class="btn">返回</a> </div>
</form>
<link rel="stylesheet" href="css/validationEngine.jquery.css">
<script src="js/jquery.validationEngine-zh-CN.js"></script>
<script src="js/jquery.validationEngine.min.js"></script>
<script>$("form").validationEngine();</script>
<?php box( 'bottom', array("back2"), 86400 ); ?>
