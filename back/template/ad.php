<? box( 'top',  array( 'title' => '广告' ), 86400 ); ?>
<?php include_once('_list.php'); ?>

<div class="row-fluid">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th-list"></i> 广告</h2>
			<div class="box-icon"></div>
		</div>
	</div>
</div>

<form id="search">
	<div class="input-append">
		位置：<select name="position_id" style="width:200px;">
			<option value="">-</option>
			<?
			$content = '';
			foreach( $position as $k => $v )
			{
				$content .= '<option value="' . $k . '">' . $v[ 'name' ] .' ('. $v[ 'width' ] . '*' . $v[ 'height' ] .
					')</option>';
			}
			echo $content;
			?>
		</select>
		关键字：
		<input name="q" size="16" type="text" style="width:120px;">
		开始：
		<input type="text" class="input-xlarge datepicker" name="start" id="start" style="width:90px;">
		结束：
		<input type="text" class="input-xlarge datepicker" name="end" id="end" style="width:90px;">

		<button class="btn btn-success" type="submit">搜索</button>
	</div>
</form>

<table class="table table-striped table-bordered" id="main">
	<thead>
		<tr>
			<th width="5"><input type="checkbox"/></th>
			<th><? up_down( 'id', 'ID' ) ?></th>
			<th><? up_down( 'position_id', '广告位' ) ?></th>
			<th><? up_down( 'title', '标题' ) ?></th>
			<th><? up_down( 'url', '地址' ) ?></th>
			<th><? up_down( 'filename', '图片' ) ?></th>
			<th><? up_down( 'remark', '备注' ) ?></th>
			<th><? up_down( 'level', '优先级' ) ?></th>
			<th><? up_down( 'expires', '过期时间' ) ?></th>
			<th><? up_down( 'time', '时间' ) ?></th>
			<th>操作</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($data as $v){?>
		<tr val="<?= $v['id'] ?>">
			<td><input type="checkbox" /></td>
			<td><?=$v['id']?></td>
			<td><a href="position.php?method=edit&id=<?=$v['position_id']?>" target="_blank"><?=$position[$v['position_id']]['name']?> [ 广告位ID: <?=$v['position_id']?> ] </a></td>
			<td><?=$v['title']?></td>
			<td id="image-<?=$k?>" class="thumbnail"><a style="background:url(banner/<?=$v['filename']?>)" title="<?=$v['title']?>" href="banner/<?=$v['filename']?>"><img height="50" src="banner/<?=$v['filename']?>"/></td>
			<td><?=$v['url']?></td>
			<td><?=$v['remark']?></td>
			<td><?=$v['level']?></td>
			<td><?=$v['expires']?></td>
			<td><?=$v['time']?></td>
			<td>
				<a do="ghost">克隆</a>
				<a href="edit">编辑</a>
			</td>
		</tr>
		<?php }?>
	</tbody>
</table>
<?php box( 'page', $nav, 0 ); ?>

<div id="cmd">
	<a href="add">增加</a>
	<a do="delit" title="删除所选图片" >删除</a>
</div>

<fieldset>
	<legend>&nbsp;</legend>
	<ul>
		<li><span style="color: blue"><a href="position.php">广告位列表</a></span> </li>
		<li><span style="color: #eb0000">后台上传图片,需5分钟左右才能同步到前台.</span> </li>
		<li>如果10分钟后图片还没有更新请点击<a href="data.php">更新CDN</a>!</li>
		<li>之后点击此连接，<a href="http://www.999zq.com/cookie/set" target="_blank">访问本链接后</a>，你访问过的网页，广告将即时更新；</li>
		<li><a href="ad.php?method=create_week">生成周榜图</a></li>
	</ul>
</fieldset>

<?php box( 'bottom', '', 86400 ); ?>
