<? box( 'top',  array( 'title' => '广告位' ), 86400 ); ?>
<?php include_once('_list.php'); ?>

<div class="row-fluid">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th-list"></i> 广告位</h2>
			<div class="box-icon"></div>
		</div>
	</div>
</div>

<form id="search">
	<div class="input-append">
		<?= $follow ?>
		关键字：
		<input name="q" size="16" type="text" style="width:120px;">
		<button class="btn btn-success" type="submit">搜索</button>
	</div>
</form>

<table class="table table-striped table-bordered" id="main">
	<thead>
		<tr>
			<th width="5"><input type="checkbox" /></th>
			<th width="60"><? up_down( 'id', 'ID' ) ?></th>
			<th width="200"><? up_down( 'name', '名字' ) ?></th>
			<th width="50"><? up_down( 'width', '宽' ) ?></th>
			<th width="50"><? up_down( 'height', '高' ) ?></th>
			<th width="80"><? up_down( 'target', '窗口' ) ?></th>
			<th width="20"><? up_down( 'follow', 'follow' ) ?></th>
			<th>操作</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($data as $v){?>
		<tr val="<?= $v['id'] ?>">
			<td><input type="checkbox" /></td>
			<td><?=$v['id']?></td>
			<td><?=$v['name']?></td>
			<td><?=$v['width']?></td>
			<td><?=$v['height']?></td>
			<td><?=$v['target']?></td>
			<td><?=$v['follow']?></td>
			<td>
				<a do="ghost">克隆</a>
				<a href="edit">编辑</a>
			</td>
		</tr>
		<?php }?>
	</tbody>
</table>
<?php box( 'page', $nav, 0 ); ?>

<div id="cmd">
	<a href="add">增加</a>
	<a do="delit" title="删除所选位置" >删除</a>
</div>

<fieldset>
	<legend>&nbsp;</legend>
	<li>删除位置前，必须先删除对应位置里的广告；</li>
</fieldset>

<?php box( 'bottom', '', 86400 ); ?>
