<? box( 'top',  array( 'title' => '增加管理员' ), 86400 ); ?>
<?php include_once('_edit.php'); ?>

<div class="row-fluid">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-th-list"></i> 增加管理员</h2>
            <div class="box-icon"><a href="#" onclick="history.back()" class="btn btn-round"><i class="icon-remove"></i></a></div>
        </div>
			
        </div>
    </div><!--/span-->

<form onsubmit="return submitit(this, 'admin', 'add_save' );">
				<table class="table table-striped table-bordered">
					<tr><td>用 户 名：</td><td><input type="text" name="username" class="validate[required]"></td><td>*</td></tr>
					<tr><td>登录密码：</td><td><input type="password" name="password" class="text-input validate[required,minSize[6],maxSize[20]]" id='pwd'></td><td>*</td></tr>
					<tr><td>重复密码：</td><td><input name="password1" class="text-input validate[condRequired[pwd],equals[pwd]]" type="password"></td><td></td></tr>
					<tr><td>备    注：</td><td><input type="text" name="remark" /> </td><td></td></tr>
				</table>
				<div class="form-actions">
				  <button type="submit" class="btn btn-primary">保存</button>
					<a href="#" onclick="history.back()" class="btn">返回</a>
			  </div>
			</form>
<link rel="stylesheet" href="css/validationEngine.jquery.css">
<script src="js/jquery.validationEngine-zh-CN.js"></script> 
<script src="js/jquery.validationEngine.min.js"></script>
<script>$("form").validationEngine();</script>
<?php box( 'bottom', array("back2"), 86400 ); ?>