<?php
$SITE = array(
    // 网站目录名
    'path' => 'back',
    // 要初始化的类名
    'action' => 'action',
    // url友好化，非文件对应的路由设置：规则（ 匹配符 => 交给哪一个module处理？）
    'url_route' => []
);

require dirname( __FILE__ ) . '/../../_index.php';
