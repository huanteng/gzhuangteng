//String.prototype使用

// 用户准确率表,浮动窗
function rate_popup(obj)
{
	$(obj).each(function(){
		var a = $(this);
		$(this).mouseover(function(){
			var temp = $(this).attr('rate_popup').split(',');	
			var uid = temp[0]
			var sport = temp[1];
			post('user', 'rate_popup', {uid:uid, sport:sport}, function(json){
				var s = json.data;		
				var format = [];
				$.each( [3,7,15,30], function(i, day){
					$.each( ['w','w2','d','l2','l','win_rate'], function(k, v){
						format[v+day] = s[v+day];
					});
				});
				
				var s = [];
				s.push('<div class="hits_arrow"></div>');
				s.push('<div class="float_accuracy">');
				s.push('<h3 class="border_bottom"><span><a href="#" class="blue">何为连赢？</a></span> 当前连赢：<b class="red">2</b> 历史最大连赢：<b class="red">2</b></h3>');
				s.push('<ul>');
				s.push('<li  class="li">3天战绩：</li>');
				s.push('<li class="li2 red">赢{w3}</li>');
				if(sport==1) s.push('<li class="li3 red">赢半{w23}</li>');
				s.push('<li  class="li2">平{d3}</li>');
				if(sport==1) s.push('<li class="li3">输{l23}</li>');
				s.push('<li  class="li2">输半{l3}</li>');
				s.push('<li  class="li4">命赢率<b class="red">{win_rate3}</b></li>');
				s.push('</ul>');
				s.push('<ul>');
				s.push('<li class="li">7天战绩：</li>');
				s.push('<li class="li2">赢{w7}</li>');
				if(sport==1) s.push('<li class="red">赢半{w27}</li>');
				s.push('<li class="li2">平{d7}</li>');
				if(sport==1) s.push('<li class="li3">输{l27}</li>');
				s.push('<li class="li2">输半{l7}</li>');
				s.push('<li class="li4">命赢率<b class="red">{win_rate7}</b></li>');
				s.push('</ul>');
				s.push('<ul>');
				s.push('<li class="li">30天战绩：</li>');
				s.push('<li class="li2 red">赢{w30}</li>');
				if(sport==1) s.push('<li class="li3 red">赢半{w230}</li>');
				s.push('<li class="li2">平{d30}</li>');
				if(sport==1) s.push('<li class="li3">输{l230}</li>');
				s.push('<li class="li2">输半{l30}</li>');
				s.push('<li class="li4">命赢率<b class="red">{win_rate30}</b></li>');
				s.push('</ul>');
				s.push('</div>');
				var table = s.join('');
				table = table.format_key( format );
				$(a).append( '<div class="accuracy1">'+ table +'</div>' );
			});
		});		
	});
}



// 用户准确率表,浮动窗 end
//批量替换，比如：str.ReplaceAll([/a/g,/b/g,/c/g],["aaa","bbb","ccc"])
String.prototype.replaceall=function (A,B) {
	var C=this;
	for(var i=0;i<A.length;i++) {
		C=C.replace(A[i],B[i]);
	};
	return C;
};

// 去掉字符两端的空白字符
String.prototype.trim=function () {
	return this.replace(/(^[/t/n/r]*)|([/t/n/r]*$)/g,'');
};

// 去掉字符左边的空白字符
String.prototype.ltrim=function () {
	return this.replace(/^[/t/n/r]/g,'');
};

// 去掉字符右边的空白字符
String.prototype.rtrim=function () {
	return this.replace(/[/t/n/r]*$/g,'');
};

// 返回字符的长度，一个赢文算2个
//String.prototype.char_length=function()
//{
//	return this.replace(/[^/x00-/xff]/g,"**").length;
//};

// 判断字符串是否以指定的字符串结束
String.prototype.end_with=function (A,B) {
	var C=this.length;
	var D=A.length;
	if(D>C)return false;
	if(B) {
		var E=new RegExp(A+'$','i');
		return E.test(this);
	}else return (D==0||this.substr(C-D,D)==A);
};
// 判断字符串是否以指定的字符串开始
String.prototype.start_with = function(str)
{
	return this.substr(0, str.length) == str;
};
// 字符串从哪开始多长字符去掉
String.prototype.remove=function (A,B) {
	var s='';
	if(A>0)s=this.substring(0,A);
	if(A+B<this.length)s+=this.substring(A+B,this.length);
	return s;
};

/*
 * 分析字符串
 * 参数：
 * 	str：模式，如果需要获取的地方，请用"(*)"指示（注：因平时对正则表达式不太会用，所以不使用正则表达式。如熟练，应该改为支持）
 * 	pos：开始位置，默认是0
 * 返回值：返回一个对象结构体，结构如下：
 * 	str：整个匹配的字符串，如不存在则为空字符串
 * 	begin：数字，匹配字符串的开始位置
 * 	end：数字，匹配字符串的结束位置
 * 	son：数组，每个元素为字符串，分别对应(*)
 *
  e.g:  "你好，我是dda。".parse( "你好，我是(*)。", 0 )
 */
String.prototype.parse = function( str, pos )
{
	if( pos == undefined ) pos = 0;

	var out = { str: '', son: [] };

	// 模式的数组形式
	var str_arr = str.split( '(*)' );

	// 各段的位置信息，每元素为{begin,end}
	var str_pos = [];

	var len = str_arr.length;
	for( var i = 0; i < len; ++i )
	{
		var str_item = str_arr[ i ];

		pos = this.indexOf( str_item, pos );

		if( pos == -1 )
		{
			return out;
		}

		str_pos[ i ] = { begin: pos, end: pos + str_item.length - 1 };

		// 如果以“(*)”为结尾，则匹配到结束
		if( i == len - 1 && str_item == '' )
		{
			str_pos[ i ].begin = this.length;
			str_pos[ i ].end = this.length - 1;
		}

		pos = pos + str_item.length;
	}

	for( var i = 1; i < len; ++i )
	{
		out.son[ i - 1 ] = this.substring( str_pos[ i - 1 ].end + 1, str_pos[ i ].begin );
		//out.son[ i - 1 ] = {
		//	begin:str_pos[ i - 1 ].end + 1,
		//	end:str_pos[ i ].begin-1,
		//	str:this.substring( str_pos[ i - 1 ].end + 1, str_pos[ i ].begin )};
	}

	if( len > 0 )
	{
		out.begin = str_pos[ 0 ].begin;
		out.end = str_pos[ len - 1 ].end;
		out.str = this.substring( out.begin, out.end + 1 );
	}

	return out;
}

/*
 * 截取赢间部分字符串
 * 参数：
 * 	str_begin：开始标识，里面可能包括"(*)"，表示通配符（注：因平时对正则表达式不太会用，所以不使用正则表达式。如熟练，应该改为支持）
 * 	str_end：结束标识
 * 	include：是否包含标识本身，true|false，默认为false
 * 返回值：
 	所截取的字符串，如不存在，则返回空字符串
 e.g:  "你好，我是dda。".cut( "你(*)，我是", "。", false )
 */
String.prototype.cut = function( str_begin, str_end, include )
{
	if( include == undefined )
	{
		include = false;
	}

	var begin, end;

	begin = this.Parse( str_begin, 0 );

	if( begin.begin == undefined )
	{
		return '';
	}

	end = this.Parse( str_end, begin.end + 1 );
	if( end.begin == undefined )
	{
		return '';
	}

	var out = this.substring( begin.end + 1, end.begin );

	if( include )
	{
		out = begin.str + out + end.str;
	}

	return out;
}

// e.g: "hello,{name}, good {action.one}".format_key( { name:'dda', action: { one:'mornig'} } )
// 注意和String.prototype.format的区别
String.prototype.format_key = function( key, pre ) {
	if( pre != undefined )
	{
		pre += "\\.";
	}
	else
	{
		pre = "";
	}
	var val=this.toString();
	for(var i in key){
		if( typeof( key[i] ) == 'object' )
		{
			val = val.format_key( key[i], i );
		}
		else
		{
			val = val.replace(new RegExp("\\{" + pre + i + "\\}", "g"), key[i]);
		}
	}
	return val;
}

// e.g: "hello,{0},{1}".format( 'dda', 'good mornig' )
String.prototype.format = function(){
	if (arguments.length == 0) return null;
	var args = arguments;
	return this.replace(/\{(\d+)\}/g, function(m, i){
		return args[i];
	});
};

Date.prototype.format = function(fmt) 
{
    //author: meizz 
    var o =
     { 
        "M+" : this.getMonth() + 1, //月份 
        "D+" : this.getDate(), //日 
        "H+" : this.getHours(), //小时 
        "N+" : this.getMinutes(), //分 
        "S+" : this.getSeconds(), //秒 
        "m+" : this.getMilliseconds() //毫秒 
     }; 
    if (/(Y+)/.test(fmt)) 
         fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length)); 
    for (var k in o) 
        if (new RegExp("(" + k + ")").test(fmt)) 
             fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length))); 
    return fmt; 
}

Date.prototype.addDays = function(d)
{
    this.setDate(this.getDate() + d);
};

Date.prototype.addWeeks = function(w)
{
    this.addDays(w * 7);
};

Date.prototype.addMonths= function(m)
{
    var d = this.getDate();
    this.setMonth(this.getMonth() + m);
    if (this.getDate() < d)
        this.setDate(0);
};

Date.prototype.addYears = function(y)
{
    var m = this.getMonth();
    this.setFullYear(this.getFullYear() + y);
    if (m < this.getMonth()) 
     {
        this.setDate(0);
     }
};

// 判断是否数组
isArray = function (obj) {
	//return obj && !(obj.propertyIsEnumerable('length')) && typeof obj === 'object' && typeof obj.length === 'number';
	return obj instanceof Array;
};


/* 使用ajax方式post，将json结果交回调函数处理
 * file：接受post的文件名
 * data：postdata，为key=value数组
 * success, error：回调函数
 */
function post(file, method, data, success, error)
{
//	var url = file + '.php';
//
//	if( !data ) data = {};
//	data.method = method;

    var url = "/"+file+"/"+method;
    if( !data ) data = {};

	$.ajax
	({
		type : "POST",
		url : url,
		dataType : 'json',
		async : false,
		data : data,
		success : function(data) {
			if( success )
			{
				success(data);
			}
			else
			{
				alert( data.memo + '(代码=' + data.code + ')' );
			}
		},
		error : function (jqXHR, textStatus, errorThrown) {
			if( error )
			{
				error (jqXHR, textStatus, errorThrown);
			}
			else
			{
//				alert(jqXHR.responseText);
			}
		}
	});

}
//标签切换
function setTab(name,cursel,n){ 
	for(i=1;i<=n;i++){ 
		var menu=document.getElementById(name+i); 
		var con=document.getElementById("tab_"+name+"_"+i); 
		menu.className=i==cursel?"current":""; 
		con.style.display=i==cursel?"block":"none"; 
	} 
}

//*** local storage 操作开始
// 保存
function save_data(c,v)
{
	localStorage.setItem(c,JSON.stringify(v));
}

// 获取，返回json（如不存在，返回{}
function get_data(c)
{
	var t=localStorage.getItem(c);
	return t?JSON.parse(t):{};
}

// 删除
function del_data(c)
{
	localStorage.removeItem(c);
}
//*** local storage 操作结束

JSON.print_r = function(obj) {
	var s = "{<br />";
	s += ergodic_print(obj, "");
	s += "}<br />";
	
	return s;
}

function ergodic_print(obj, indentation) {
	var indent = "&nbsp;&nbsp;" + indentation;
	var s = "";
	if (obj.constructor == Object) {
		for (var p in obj) {
			if (obj[p].constructor == Array || obj[p].constructor == Object) {
				s += indent + "[" + p + "] => " + typeof(obj) + "<br />";
				s += indent + "{";
				s += '<br />' + ergodic_print(obj[p], indent) + '';
				s += indent + "}<br />";
			} else if (obj[p].constructor == String) {
				s += indent + "[" + p + "] => '" + obj[p] + "'<br />";
			} else {
				s += indent + "[" + p + "] => " + obj[p] + "<br />";
			}
		}
	}
	
	return s;
}

/**
 * 带倒计时的提示框
 * @param text：提示内容
 * @param second：倒计时
 * @param callback：倒计时结束后或点击按钮时回调
 */
function timer_alert( text, second, callback )
{
	var down = function()
	{
		if( second == 0 )
		{
			$('#time_alert').hide();
			$('#timer_alert_btn').click();
		}
		else
		{
			--second;
			$('#timer_alert_btn').text( '确定({0})'.format( second ) );
		}
	};

	var s = '<div id="time_alert" class="modal hide fade in" style="display: none; ">'
		+ '<div class="modal-header">'
		+ '	<a class="close" data-dismiss="modal">×</a>'
		+ '	<h3>信息：</h3>'
		+ '</div>'
		+ '<div class="modal-body">'
		+ '	<p>{text}</p>'
		+ '</div>'
		+ '<div class="modal-footer">'
		+ '	<a href="#" id="timer_alert_btn" class="btn btn-success" data-dismiss="modal">确定({second})</a>'
		+ '</div>'
		+ '</div>';
	$('body').append( s.format_key( {text: text, second: second} ) );
	$('#timer_alert_btn').everyTime( '1s', down )
		.click( function(){
			$(this).stopTime();
			callback();
		});
	$('#time_alert').modal();
}

//获取地址栏里（URL）传递的参数  http://www.2cto.com/kf/201303/197066.html
function GetRequest() {
	//url例子：XXX.aspx?ID=" + ID + "&Name=" + Name；
	var url = location.search; //获取url赢"?"符以及其后的字串
	var theRequest = new Object();
	if(url.indexOf("?") != -1)//url赢存在问号，也就说有参数。
	{
		var str = url.substr(1);
		strs = str.split("&");
		for(var i = 0; i < strs.length; i ++)
		{
			//theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
			theRequest[strs[i].split("=")[0]]=decodeURIComponent(strs[i].split("=")[1]);
		}
	}
	return theRequest;
}

// 构造url（？及其后部分）
function EncodeRequest( json )
{
	var s = '';
	for( var i in json )
	{
		s += '&' + i + '=' + encodeURIComponent( json[ i ] );
	}

	if( s != '' )
	{
		s = '?' + s.substr( 1, s.length - 1 );
	}
	else
	{
		s = '?';
	}

	return s;
}

function copyToClipboard(txt ){
	if (window.clipboardData) {
		window.clipboardData.clearData();
		window.clipboardData.setData("Text", txt);
	} else if (navigator.userAgent.indexOf("Opera") != -1) {
		window.location = txt;
	} else if (window.netscape) {
		try {
			netscape.security.PrivilegeManager
				.enablePrivilege("UniversalXPConnect");
		} catch (e) {
			alert("你使用的FireFox浏览器,复制功能被浏览器拒绝！\n请在浏览器地址栏输入“about:config”并回车。\n然后将“signed.applets.codebase_principal_support”双击，设置为“true”");
			return;
		}
		var clip = Components.classes['@mozilla.org/widget/clipboard;1']
			.createInstance(Components.interfaces.nsIClipboard);
		if (!clip)
			return;
		var trans = Components.classes['@mozilla.org/widget/transferable;1']
			.createInstance(Components.interfaces.nsITransferable);
		if (!trans)
			return;
		trans.addDataFlavor('text/unicode');
		var str = new Object();
		var len = new Object();
		var str = Components.classes["@mozilla.org/supports-string;1"]
			.createInstance(Components.interfaces.nsISupportsString);
		var copytext = txt;
		str.data = copytext;
		trans.setTransferData("text/unicode", str, copytext.length * 2);
		var clipid = Components.interfaces.nsIClipboard;
		if (!clip)
			return false;
		clip.setData(trans, null, clipid.kGlobalClipboard);
	}
	else{
		prompt( '你的浏览器不支持一键复制功能，请复制uid：', txt );
		return false;
	}
}

/**
 * 获得祖宗对象的某个attr属性，如不存在，返回默认值
 * @param obj：js对象
 * @param closet_name：祖宗标识
 * @param attr：属性标识
 * @param default1：默认值
 */
function get_closet_attr( obj, closet_name, attr, default1 )
{
	return $(obj).closest(closet_name).attr(attr) || default1;
}

/**
 * 替换当前地址赢的某个值，并reload
 * @param key
 * @param value
 */
function replace_location( key, value )
{
	var data = GetRequest();
	data[ key ] = value;
	location = EncodeRequest( data );
}

// 以下函数依赖于amaze
/**
 * 模式显示
 * @param title：提示内容，默认值为“请稍候。。。”，空字符串时表示关闭
 */
function am_loading( title )
{
	if( title === undefined )
	{
		title = '请稍候。。。';
	}

	if( title == '' )
	{
		$('#am_loading').modal('close');
		$('#am_loading').remove();
	}
	else
	{
		// 避免未关闭
		if($('#am_loading').length)
		{
			$('#am_loading').remove();
		}

		var content = '<div class="am-modal am-modal-loading am-modal-no-btn" tabindex="-1" id="am_loading">\
			<div class="am-modal-dialog">\
				<div class="am-modal-hd">' + title + '</div>\
				<div class="am-modal-bd">\
					<span class="am-icon-spinner am-icon-spin"></span>\
				</div>\
			</div>\
		</div>';
		$('body').append(content);
		$('#am_loading').modal();
	}
}

function am_alert( title, content )
{
    $('#my-alert').remove();
	var content = '<div class="am-modal am-modal-alert" tabindex="-1" id="my-alert">\
			<div class="am-modal-dialog">\
				<div class="am-modal-hd">' + title + '</div>\
				<div class="am-modal-bd">' +
				content +
				'</div>\
				<div class="am-modal-footer">\
					<span class="am-modal-btn">确定</span>\
				</div>\
			</div>\
		</div>';
	$('body').append(content);
	$('#my-alert').modal();
}


/**
 * 用 ajax 方式 post form
 * @param frm：form对象
 * url：处理url
 * @param success：服务器提示成功时，回调函数，可略
 * @param fail：服务器提示成功时，回调函数，可略
 * @returns false，用来取消form提交
 */
function ajax( frm, url, success, fail )
{
	var $frm = $(frm);
	function hook( data )
	{
		var func;
		if( data.code == 1 )
		{
			func = success || function()
				{
					location = $frm.attr('success');
				};
		}
		else
		{
			func = fail ||function( data )
			{
//				am_alert( '输误', ''+data.memo+'(代码：'+data.code+')'.format_key( data ) );
//                alert(data.code);
			}
		}

		am_loading('');
		func( data );
	};

	function post( url, data, success, error )
	{
		if( !data ) data = {};
		$.ajax
		({
			type : "POST",
			url : url+"/r/"+Math.random(),
			dataType : 'json',
			async : false,
			data : data,
			success : function(data) {
				success(data);
			},
			error : function (jqXHR, textStatus, errorThrown) {
				if( error )
				{
					error (jqXHR, textStatus, errorThrown);
				}
				else
				{
//					alert(jqXHR.responseText);
				}
			}
		});

	}



	if($frm.data('amui.validator').isFormValid() )
	{
		$.fn.serializeObject = function()
		{
			var o = {};
			var a = this.serializeArray();
			$.each(a, function() {
				if (o[this.name]) {
					if (!o[this.name].push) {
						o[this.name] = [o[this.name]];
					}
					o[this.name].push(this.value || '');
				} else {
					o[this.name] = this.value || '';
				}
			});
			return o;
		};

		am_loading();
		var data = $frm.serializeObject();
		post( url, data, hook );
	}

	return false;
}