$( document ).ready( function()
{
	var content = '<div class="contextMenu" id="context_menu_user"><ul><li id="copy_uid_1">复制uid</li>' +
		'<li id="user_index">用户首页</li>' +
		'<li id="list_tips">推荐列表</li>' +
		'<li id="report_tips">推荐报表</li>' +
		'<li id="modify_info">修改信息</li>' +
		'<li id="sell_tips">查看卖出的推荐</li>' +
		'<li id="buy_tips">查看买入的推荐</li>' +
		'<li id="gold">金币记录</li>' +
		'<li id="send_magic">赠送道具</li>' +
		'<li id="magic">道具记录</li>' +
		'<li id="tips_fake">自动发布</li>' +
		'<li id="tips_adjust">胜率调整</li>' +
		'<li id="daily_award_log">轮盘抽奖</li>' +
		'</ul></div>';
	$( document.body ).append( content );
	$( "#context_menu_user" ).hide();

	$( "a[context_menu_type='user'],td[context_menu_type='user']" ).each
	(
		function( t )
		{
			$( this ).contextMenu
			(
				'context_menu_user',
				{
					onContextMenu : function( e )
					{
						var uid = $( e.target ).closest('[uid]').attr( 'uid' );
						window.click_uid_context = function( url, new_window )
						{
							url = url.format( uid );
							if( new_window )
							{
								open( url );
							}
							else
							{
								location = url;
							}
						};

						$( '#copy_uid_1' ).html( '复制uid（' + uid + '）').unbind('click').click(function(){
							$(this).parent().parent().hide();
							$(this).parent().parent().next().hide();
							if( copyToClipboard( $( e.target ).attr( 'uid' ) ) )
							{
								alert( '复制成功' );
							}
						});
						return true;
					},

					onShowMenu : function( e, menu )
					{
						return menu;
					},
                  
					bindings :
					{
						'user_index' : function( t )
						{
							click_uid_context( 'http://www.999zq.com/user/{0}', true );
						},

						'list_tips' : function( t )
						{
							click_uid_context( 'tips.php?search_type=t.uid&q={0}&type=&start=&end=' );
						},

						'sell_tips' : function( t )
						{
							click_uid_context( 'sell_log.php?search_type=s.uid&q={0}' );
						},

						'buy_tips' : function( t )
						{
							click_uid_context( 'sell_log.php?search_type=s.buyer_uid&q={0}' );
						},

						'report_tips' : function( t )
						{
							click_uid_context( 'report_tips.php?method=user&uid={0}' );
						},

						'modify_info' : function( t )
						{
							click_uid_context( 'users.php?method=edit&uid={0}' );
						},

						'gold' : function( t )
						{
							click_uid_context( 'balance.php?search_type=uid&q={0}&type=&start=&end=' );
						}

						, 'send_magic' : function( t )
						{
							click_uid_context( 'magic_send.php?uid={0}' );
						}

						, 'magic' : function( t )
						{
							click_uid_context( 'magic_log.php?uid={0}&start=&end=' );
						}

						, 'tips_fake' : function( t )
						{
							click_uid_context( 'tips_fake.php?type=&basketball_type=&uid={0}&q=' );
						}

						, 'tips_adjust' : function( t )
						{
							click_uid_context( 'tips_adjust.php?uid={0}&type=&basketball_type=' );
						}

						, 'daily_award_log' : function( t )
					{
						click_uid_context( 'daily_login_award.php?method=add&uid={0}' );
					}
					}
				}
			);
		}
	);

}
);