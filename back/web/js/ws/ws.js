var webim = {
//	'server' : 'ws://127.0.0.1:9503',
	'server' : 'ws://115.29.187.212:9503',
	// 用于断线重连
	timeid: 0,
	session: null
//    flash_websocket:true
};
var ws = {};

function ws_init()
{

	//使用原生WebSocket
	if (window.WebSocket || window.MozWebSocket)
	{
		ws = new WebSocket(webim.server);
	}
	//使用flash websocket
	else if (webim.flash_websocket)
	{

		WEB_SOCKET_SWF_LOCATION = "/js/static/WebSocketMain.swf";
		$.getScript("/js/static/swfobject.js", function () {
			$.getScript("/js/static/web_socket.js", function () {
				ws = new WebSocket(webim.server);
			});
		});
	}
	//使用http xhr长轮循
	else
	{
		ws = new Comet(webim.server);
	}
	listenEvent();
}

function ws_heart()
{
	send_ws( 'users', 'heart' );
}

function send_ws( file, method, data )
{
	var msg = { f: file, m: method, d: data };
	if( webim.session )
	{
		msg.s = webim.session;
	}
	msg = $.toJSON(msg);
	console.log( '发消息:' + msg );
	ws.send( msg );

	window.clearInterval(webim.timeid);
	webim.timeid = window.setInterval(ws_heart, 1000 * 60 * 5 );
}

// 更新或初始化session
function update_session()
{
	var data = {};
	var username = '';
	var password = '';

	password = sessionStorage.getItem( 'password' );

	var login_info;
	login_info = get_data('login_info');

	for( var i in login_info )
	{
		username = i;
	}

	if( username && password )
	{
		data.username = username;
		data.password = password;
	}

	send_ws( 'users', 'login', data )
}

function on_users_login( msg )
{
	var session = msg.d.session;
	sessionStorage.setItem( 'session', session );
	webim.session = session;
	ws_users_url();
}

function on_system_alert( data )
{
	alert( data.data );
}

//function listenEvent废止本函数() {
//	/**
//	 * 连接建立时触发
//	 */
//	ws.onopen = function(e){
//		if( webim.timeid )
//		{
//			window.clearInterval(webim.timeid);
//		}
//
//		ws_open();
//	};
//
//    //有消息到来时触发
//    ws.onmessage = function (e) {
//		var mydate = new Date();
//		var t=mydate.toLocaleString();
//		console.log( t + ' 收到消息：' + e.data );
//		var message = $.evalJSON(e.data);
//		var method = 'on_' + message.m;
//
//		if( window[ method ] )
//		{
//			delete message.m;
//			window[ method ]( message );
//		}
//		else
//		{
//			console.log("消息被忽略：" + method);
//		}
//    };
//
//    /**
//     * 连接关闭事件
//     */
//    ws.onclose = function (e) {
//		console.log( '已断线' );
//		window.clearInterval(webim.timeid);
//		webim.timeid = window.setInterval(ws_init, 10000);
//	};
//
//    /**
//     * 异常事件
//     */
//    ws.onerror = function (e) {
//    //    alert("异常:" + e.data);
//        console.log("onerror");
//    };
//}

function GetDateT(time_stamp) {
    var d;
    d = new Date();

    if (time_stamp) {
        d.setTime(time_stamp * 1000);
    }
    var h, i, s;
    h = d.getHours();
    i = d.getMinutes();
    s = d.getSeconds();

    h = ( h < 10 ) ? '0' + h : h;
    i = ( i < 10 ) ? '0' + i : i;
    s = ( s < 10 ) ? '0' + s : s;
    return h + ":" + i + ":" + s;
}

function ws_users_url()
{
	send_ws( 'users', 'log', {url: window.location.href, title: document.title } );
}

function on_system_login( $msg )
{
	update_session();
}


function ws_open(e) {
	// connect ws server success.
	console.log("onOpen");

	var session = sessionStorage.getItem( 'session' );

	if( session )
	{
		webim.session = session;
		ws_users_url();
	}
	else
	{
		update_session();
	}
}

$(document).ready(function () {
	ws_init();
});