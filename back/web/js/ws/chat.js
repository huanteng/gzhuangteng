var ws = {};
var client_id = 0;

var webim = {
  'server' : 'ws://115.29.187.212:9503'
//  'server' : 'ws://127.0.0.1:9503'
};

$(document).ready(function () {
    //使用原生WebSocket
    if (window.WebSocket || window.MozWebSocket)
    {
        ws = new WebSocket(webim.server);
    }
    //使用flash websocket
    else if (webim.flash_websocket)
    {
        WEB_SOCKET_SWF_LOCATION = "/assets/flash-websocket/WebSocketMain.swf";
        $.getScript("/assets/flash-websocket/swfobject.js", function () {
            $.getScript("/assets/flash-websocket/web_socket.js", function () {
                ws = new WebSocket(webim.server);
            });
        });
    }
    //使用http xhr长轮循
    else
    {
        ws = new Comet(webim.server);
    }
    listenEvent();
});

function listenEvent() {
    /**
     * 连接建立时触发
     */
    ws.onopen = function (e) {
      //连接成功
      console.log("connect ws server success.");

      var account = get_data('account');

      var msg = {};
      msg.f='user';
      msg.m='track';
      msg.s = dplus.cookie.props.distinct_id;
      msg.d={url: document.location.href, title: (document.title).replace("欢腾网络","").replace("-",""), uid: account.uid};

      ws.send($.toJSON(msg));
    };

    //有消息到来时触发
    ws.onmessage = function (e) {
//        var message = $.evalJSON(e.data);
//        var cmd = message.cmd;

    };

    /**
     * 连接关闭事件
     */
    ws.onclose = function (e) {
      console.log("disconnect from ws server.");
    };

    /**
     * 异常事件
     */
    ws.onerror = function (e) {
        console.log("ws onerror");
    };
}

