<?php
class action extends backend
{
	function home($in)
	{
		return $this->out(array());
	}

	function login($in)
	{
		if (!isset($in['username']) || $in['username'] == '') {
			$this->jump('用户名不能空');
		} else if (!isset($in['password']) || $in['password'] == '') {
			$this->jump('密码不能空');
		} else if (!isset($in['code']) || $in['code'] == '') {
			$this->jump('验证码不能空');
		} else {
			$result = biz('admin')->login($in);

			if( $result[ 'code' ] == 1 )
			{
				$this->jump(null, 'main.php', 0, 0);
			}
			else
			{
				$this->jump( $result[ 'memo' ] );
			}
		}
	}

	// 后台跨站登录
	function site_login( $in )
	{
		$this->check_sign( $in );
				
		$site_login = true;
		$result = biz('admin')->login($in, $site_login);//如果site_login 也通过 $in 传入,会有安全隐患,所以 分开参数 2017 4 8

		if( $result[ 'code' ] == 1 )
		{
			$url = value($in,'url','');
			$url = 'main.php'.(strlen($url)>0?'?url='.$url:'');
			header( 'location:'.$url );
		}
		else
		{
			echo $result[ 'memo' ];
		}
	}

}
?>