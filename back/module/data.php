<?php
class action extends backend
{

	function home( $in )
	{
		$this->check_privilege( '129' );

		return $this->out();
	}

	function clear( $in )
	{
		$this->check_privilege( '129' );

		//清除cache 目录
		$days_ago = value( $in, 'data.days_ago', 0 );
		$path = config('dir.root')."cache";
		$file = load('file');
		$ignore_list = array($path . '/error.php');//不删错误日志
		$file->del_old($path, $days_ago, true, $ignore_list);

		return $this->ajax_out( '成功操作' );
	}

	function update_cdn( $in )
	{
		$url_list = $in[ 'url_list' ];
		$array_url_list = explode( "\n" , $url_list );

		foreach ( $array_url_list as $k => $v )
		{
			if ( $v !== "" )
			{
				load( 'cdn' )->refresh( trim( $v ) , 'Directory' );
			}
		}

		$this->jump( '清除成功' , "/data.php" );
	}

}

?>
