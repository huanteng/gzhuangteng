<?php
class action extends backend
{

	function home( $in )
	{
		$in[ 'q' ] = urldecode($in[ 'q' ]);
		$this->check_privilege( '254' );
		$news = biz( 'news' );
		$in['term']=array("title"=>$in[ 'q' ]);
		$in[ 'pagesize' ] = 15;
		$in[ 'type' ] = value( $in, 'type' );
		$in[ 'orderby' ] = 'id desc';
		$equal = array( 'type' );
		$like = array( );
		$q = array( 'title' );

		$data = $news->search( $in, $equal, $like, $q ,['field'=>'id,title,type,hot,time,`from`']);
		$data[ 'q' ] = value( $in, 'q' );


		$type_dict = $news->type_dict();
		$data[ 'type_select' ] = load( 'form' )->select( 'type', $in[ 'type' ], $type_dict, array('empty'=>true, 'attr'=>'style="width:90px;" ') );
		$data[ 'type' ] = $type_dict;
		$data[ 'searchtype' ] = $in[ 'type' ];

		return $this->out( $data );
	}

	function format( $value )
	{
		$this->check_privilege( '254' );
		return $value;
	}

	function add( $in )
	{
		$this->check_privilege( '254' );
		$news = biz( 'news' );
		$type_dict = $news->type_dict();
		$data[ 'type_select' ] = load( 'form' )->select( 'type', '', $type_dict, array( 'empty' => false, 'attr' => ' class="validate[required]"' ) );
		return $this->out( $data );
	}

	function add_save( $in )
	{
		$this->check_privilege( '254' );

		$news = biz( 'news' );
		$news_content = biz( 'news_content' );
//		$in['content'] = $news->add_keyword($in['content']);

		if($in['type']<99)//自已手工发的文章不过滤
		{
			$in['content']= str_replace('&lt;', '<',$in['content']);
			$in['content']= str_replace('&gt;', '>',$in['content']);
			$in['content']= str_replace('<p>', '[p]',$in['content']);
			$in['content']= str_replace('</p>', '[/p]', $in['content']);
			$in['content']= str_replace('<br>', '[br]',$in['content']);
			$in['content']= str_replace('<br />', '[br]',$in['content']);
			$in['content']= str_replace('<br/>', '[br]',$in['content']);
			$in['content']= strip_tags($in['content']);

			$in['content']= str_replace( '[p]','<p>',$in['content']);
			$in['content']= str_replace('[/p]','</p>',  $in['content']);
			$in['content']= str_replace( '[br]','<br/>',$in['content']);
		}

		$in['time'] = time();

		$id = $news->add( $in );

		if (isset($_POST['content']))
		{
			$in['content'] = $_POST['content'];
		}

		$result = $news_content->add(array('id'=>$id, 'content'=>$in['content']) );


		if( $result == -100 )
		{
			$msg = "添加失败,请检查是否重复！";
		}
		else
		{
			$msg = "添加成功";
		}
		$this->jump( $msg, 'news.php' );
		return false;
	}

	function edit( $in )
	{
		$this->check_privilege( '254' );
		$news = biz( 'news' );
		$data[ 'type' ] = $news->type_dict();
		$data[ 'info' ] = $news->info( $in[ 'id' ] );

		return $this->out( $data );
	}

	function edit_save( $in )
	{
		$this->check_privilege( '254' );

		$news_content = biz( 'news_content' );
		$news  = biz( 'news' );

		if (isset($_POST['content']))
		{
			$in['content'] = $_POST['content'];
		}

		$news->set( $in );
		$result = $news_content->set( $in );

		if( $result == 0 )
		{
			$msg = "修改失败！";
		}
		else
		{
			$msg = "修改成功";
		}
		$this->jump( $msg, 'news.php' );
		return false;
	}

	function del( $in )
	{
		$this->check_privilege( '254' );
		$result = biz( 'news' )->del( $in[ 'id' ] );
		if( $result == 0 )
		{
			$msg = "删除失败！";
		}
		else
		{
			$msg = "删除成功";
		}
		$this->jump( $msg, '/news.php' );
		return false;
	}

	function about_us( $in )
	{
		$this->check_privilege( '254' );
		//指定类型,内部文章类型；如关于我们，帮助中心
		$in[ 'type' ] = 99;
		$news = biz( 'news' );
		$in[ 'pagesize' ] = 15;
		$equal = array( 'type' );
		$like = array( );
		$q = array( 'title', 'id' );
		$data = $news->search( $in, $equal, $like, $q );
		$data[ 'q' ] = value( $in, 'q' );
		
		$type_dict = $news->type_dict();
		$data[ 'type_select' ] = load( 'form' )->select( 'type', $in[ 'type' ], $type_dict, array('empty'=>true, 'attr'=>'style="width:90px;" ') );
		$data[ 'type' ] = $type_dict;
		
		return $this->out( $data, 'news' );
	}



}

?>