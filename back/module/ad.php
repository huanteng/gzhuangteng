<?php
class action extends backend
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'ad';
		$this->check_privilege( '265' );
	}

	function home( $in )
	{
		$in += array(
		);

		$ad = biz( 'ad' );

		$equal = array( 'id', 'position_id' );
		$like = array( 'name' );
		$q = array( 'id', 'title', 'remark' );
		$other = array();
		if( value( $in, 'start' ) != '' )
		{
			$other[] = 'time>= ' . strtotime( $in[ 'start' ] );
		}
		if( value( $in, 'end' ) )
		{
			$other[] = 'time < ' . ( strtotime( $in[ 'end' ] ) + 86400 );
		}

		$option = array( 'term'=>$other );
		$data = $ad->search( $in, $equal, $like, $q, $option );

		$time = load( 'time' );
		foreach( $data[ 'data' ] as &$v )
		{
			$v[ 'expires' ] = $v[ 'expires' ] == 0 ? '' : $time->format( $v[ 'expires' ] );

			$v[ 'time' ] = $time->format( $v[ 'time' ] );
		}

		$position = biz( 'position' )->get( 'id,name,width,height', array(), 'name' );
		$data[ 'position' ] = load('dataset')->key_by_id( $position, 'id' );

		return $this->out( $data );
	}

	function add( $in )
	{

		$position = biz('position')->get( 'id,name,width,height', array(), 'name' );
		$info[ 'position' ] = load('dataset')->key_by_id( $position, 'id' );
		$info[ 'level' ] = load('form')->number_select( 'level', 0 );

		return $this->out( $info );
	}

	function add_save( $in )
	{

		$in[ 'expires' ] = load( 'time' )->str2time( $in['expires'] );
		biz( 'ad' )->add( $in );
		return $this->ajax_out( 1, '添加成功' );
	}

	function edit( $in )
	{

		$ad = biz( 'ad' );
		$info = $ad->get_from_id( $in[ 'id' ] );

		$position = biz('position')->get( 'id,name,width,height', array(), 'name' );
		$info[ 'position' ] = load('dataset')->key_by_id( $position, 'id' );


		$info['level'] = intval( $info['level'] );
		$info[ 'level' ] = load('form')->number_select( 'level', $info['level'] );
		$info[ 'time' ] = load('time')->format( $info[ 'time' ] );


		return $this->out( $info );
	}

	function edit_save( $in )
	{

		$in[ 'expires' ] = load( 'time' )->str2time( $in['expires'] );
		$result = biz( 'ad' )->set( $in );

		return $result == 1 ? $this->ajax_out( 1, '修改成功' ) : $this->ajax_out( -1, '修改失败,请检查！' );
	}

	function del( $in )
	{

		$result = biz( 'ad' )->del( $in[ 'id' ] );
		return $result == 1 ? $this->ajax_out( 1, '删除成功' ) : $this->ajax_out( -1, '删除失败,请检查！' );
	}

	function update($in){

	}

	function create_week($in)
	{
		biz('ad')->createWeekImage(1,'pc');
		biz('ad')->createWeekImage(1,'mobile');
		biz('ad')->createWeekImage(2,'pc');
		biz('ad')->createWeekImage(2,'mobile');

		biz('ad')->createMonthImage('pc');
		biz('ad')->createMonthImage('mobile');

		echo '足球周榜图<a href="/banner/export_week_1_pc'.'_'.date("Ymd").'.jpg" target="_blank">[下载]</a><br/>';
		echo '篮球周榜图<a href="/banner/export_week_2_pc'.'_'.date("Ymd").'.jpg" target="_blank">[下载]</a><br/>';
		echo '足球(手机)周榜图<a href="/banner/export_week_1_mobile'.'_'.date("Ymd").'.jpg" target="_blank">[下载]</a><br/>';
		echo '篮球(手机)周榜图<a href="/banner/export_week_2_mobile'.'_'.date("Ymd").'.jpg" target="_blank">[下载]</a><br/>';

		echo '足球月榜图<a href="/banner/export_month_pc'.'_'.date("Ymd").'.jpg" target="_blank">[下载]</a><br/>';
		echo '足球(手机)月榜图<a href="/banner/export_month_mobile'.'_'.date("Ymd").'.jpg" target="_blank">[下载]</a><br/>';

		exit;
	}

}
?>
