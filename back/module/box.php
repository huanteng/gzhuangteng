<?php
class action_box extends backend
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'box';
	}
    // 页头
	function top( $in )
	{
		$in += array(
			'title' => ''
		);

		if( $in[ 'title' ] != '' )
		{
			$in['title'] = $in['title'] . '_999后台';
		}
		else
		{
			$in['title'] = '999后台';
		}

		$url = config('url');
		$in[ 'url' ] = $url;

		return $in;
	}

	// 翻页页码列表
	function page( $in )
	{
		$page = $in[ 'page' ];
		$start = $page-2 > 0 ? $page-2 : 1;
		$end = $start+4 < 5 ? 5 : $start+4;
		$end = min( $end, $in[ 'pagecount' ] );

		$url = $_SERVER["REQUEST_URI"];

		$route = array(
			'-page-\d+$' => '',
			'\/page-\d+' => '',
			'&page=\d+$' => '',
			'\/page=\d+' => '',
			'page=\d+$' => '',
			'page=\d+&'=>''
		);

		foreach ($route as $k => $v) {
			$pattern = '/' . $k . '/';

			if( preg_match( $pattern, $url, $matches, PREG_OFFSET_CAPTURE ) === 1 )
			{
				$url = preg_replace( $pattern, $v, $url );
				break;
			}
		}

		if(strpos($url,"=")>0){
			$url .= '&page=';
		}else{
			if(strpos($url,"?")>0) {
				$url .= 'page=';
			}else{
				$url .= '?page=';
			}
		}

		$in[ 'start' ] = $start;
		$in[ 'end' ] = $end;
		$in[ 'url' ] = $url;

		return $in;
	}

}
?>