<?php
class action extends backend
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'focus_photo';
		$this->check_privilege();
	}

	function home($in)
	{
		$in[ 'pagesize' ] = 15;
		$in[ 'id' ] = value( $in, 'id' );
		$in[ 'orderby' ] = 'id desc';
		$equal = array( 'type' );
		$like = array('title' );
		$q = array('type','title','id' );
		$other = array();
		$focus_photo =  biz('focus_photo');
		$data = $focus_photo->search( $in, $equal, $like, $q, array( 'term' => $other ));
		foreach( $data[ 'data' ] as &$v )
		{
			$v['typename'] = $focus_photo->type_dict($v['type']);
		}
		$typename = $focus_photo->type_dict();
		$data[ 'typename' ] = load('form')->select( 'id', '', $typename, array( 'title' => '类型', 'attr'=>'style="width:90px;" '));
		return $this->out( $data );
	}

	function add( $in )
	{
		$this->check_privilege();
		$in['typename'] = biz('focus_photo')->type_dict();
		return $this->out( $in );
	}

	function add_save( $in )
	{
		$this->check_privilege();
		biz('focus_photo')->add( $in );
		return $this->ajax_out( 1, '添加成功' );
	}

	function edit( $in )
	{
		$this->check_privilege( );
		$id = isset($in['id']) && is_numeric($in['id']) ? $in['id'] : 0;
		$focus_photo = biz('focus_photo');
		$pic = $focus_photo->get1('*', array( 'id' => $id ));
		$pic['typename'] = $focus_photo->type_dict();
		$pic['type'] = value($pic, 'type');
		return $this->out( $in + $pic);
	}

	function edit_save( $in )
	{
		$this->check_privilege( );
		$result=biz('focus_photo')->set( $in );
		return $result == 1 ? $this->ajax_out( 1, '修改成功' ) : $this->ajax_out( -1, '修改失败,请检查！' );
	}

	function del( $in )
	{
		$this->check_privilege();
		$result = biz('focus_photo')->del_by_term( array('id' => $in[ 'id' ]) );
		return $result == 1 ? $this->ajax_out( 1, '删除成功' ) : $this->ajax_out( -1, '删除失败,请检查！' );
	}

	function upload($in)
	{
		$this->check_privilege();

		if(isset($in['file']))
		{
			biz('focus_photo')->set( $in );
		}

		$id = isset( $in['id'] ) && is_numeric($in['id']) ? $in['id'] : 0;
		$focus_photo = biz('focus_photo');
		$info = $focus_photo->get1('*' , array( 'id' => $id ));
		$in['info'] = $info;



		return $this->out($in);
	}
}
?>
