<?php
class action extends backend
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'position';
		$this->check_privilege( '265' );

	}

	function home( $in )
	{
		$in += array(
		);

		$position = biz( 'position' );

		$equal = array( 'id', 'follow' );
		$like = array( 'name' );
		$q = array( 'id','name' );
		$data = $position->search( $in, $equal, $like, $q );

		$follow = $position->follow_dict();
		foreach( $data[ 'data' ] as &$v )
		{
			$v[ 'follow' ] = $follow[ $v[ 'follow' ] ];
		}

		$data[ 'follow' ] = load( 'form' )->select( 'follow', '', $follow, array( 'title' => '是否follow', 'attr' => 'style="width:120px;" ' ) );

		return $this->out( $data );
	}

	function add( $in )
	{
		$position = biz( 'position' );
		$info[ 'follow' ] = load('form')->radio( 'follow', '1', $position->follow_dict() );
		return $this->out( $info );
	}

	function add_save( $in )
	{
		$position = biz( 'position' );

		if( $position->exists( array( 'name' => $in[ 'name' ] ) ) )
		{
			return $this->ajax_out( -1, '名字重复' );
		}
		else
		{
			$position->add( $in );
			return $this->ajax_out( 1, '添加成功' );
		}
	}

	function edit( $in )
	{
		$position = biz( 'position' );
		$info = $position->get_from_id( $in[ 'id' ] );
		$info[ 'follow' ] = load('form')->radio( 'follow', $info[ 'follow' ], $position->follow_dict() );

		//列出这个广告位的广告列表
		$info['ad'] = biz('ad')->search(['position_id'=>$in['id']], ['position_id']);

		return $this->out( $info );
	}

	function edit_save( $in )
	{
		$result = biz( 'position' )->set( $in );

		return $result == 1 ? $this->ajax_out( 1, '修改成功' ) : $this->ajax_out( -1, '修改失败,请检查！' );
	}

	function del( $in )
	{

		if( biz('ad')->exists( 'position_id in(' . $in[ 'id' ] . ')' ) )
		{
			return $this->ajax_out( -1, '该位置中含有广告，删除失败' );
		}

		$position = biz( 'position' );
		$result = $position->del( $in[ 'id' ] );

		return $result == 1 ? $this->ajax_out( 1, '删除成功' ) : $this->ajax_out( -1, '删除失败,请检查！' );
	}

}
?>
