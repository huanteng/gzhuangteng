<?php
class backend extends frontend
{
	function run( $in )
	{
		$url = config('url');
		$method = &$url[ 'method' ];

		// 有可能通过post提交
		if( isset( $in[ 'method' ] ) )
		{
			$method = $in[ 'method' ];
			unset( $in[ 'method' ] );
		}

		if( !method_exists( $this, $method ) )
		{
			$method ="home";
		}

        $in = $this->clean( $in );
        $this->content = $this->$method( $in );
	}

	// 默认要检查的权限列表
	var $privilege = '0';

	// 检查当前身份，是否拥有该功能权限，当前身份来自cookie
	// 参数：$privilege：功能id，对应于 admin_function.id；用半角逗号隔开
	// 返回值：无
	function check_privilege( $privilege = '0' )
	{
		if( $privilege == '0' )
		{
			$privilege = $this->privilege;
		}

		$cookie = load('cookie');

		$have = $cookie->get( 'privilege',true);
		if( !$have )
		{
			echo '未登录';
			die;
		}

		$privilege = explode( ',', $privilege );

		foreach( $privilege as $p )
		{
			if( !isset( $have[ $p ] ) )
			{
				echo '权限不足';
				die;
			}
		}
	}

	/** 检查当前用户是否拥有某功能的权限
	 * @param $privilege
	 * @return bool
	 */
	function have_privilege( $privilege )
	{
		$have = load('cookie')->get( 'privilege',true);
		if( !$have )
		{
			return false;
		}

		return isset( $have[ $privilege ] );
	}

	function jump($text = '请不要乱打链接访问哦', $url = '', $time = 3) {
		if ($url == '') {
			$url = $this->table . '.php';
		}

		echo $this->out( array( 'title' => config( 'site.name') . '管理后台', 'text' => $text, 'time' => $time, 'url' => $url ), 'jump' );
	}

	// 返回当前登录用户名或“未登录”
	function login_username()
	{
		$admin_cookie = biz('admin')->login_info();
		return value( $admin_cookie, 'username', '未登陆' );
	}

	/*
	 * 做后台log
	 * in参数：和log字段一致
	 */
	function log( $module, $relate_id, $content, $data = array() )
	{
		$content = $this->login_username() . ' ' . $content;
		biz('base')->log( $module, $relate_id, $content, $data );
	}

	/*
	 * 后台输出
	 * 参数：
	 * 	data：传入模板的变量对
	 */
	function out($data = array(), $tpl = '')
	{
		$url = config('url');

		$table = $url[ 'module' ];
		$template = load('template');
		$template->path = config('dir.project') . 'template/';
		$template->appoint($data + array( '_table' => $table ) );
		if ($tpl == '') {
			$method = $url[ 'method' ];
			if ($method != 'home')
				$table .= '_' . $method;
		}else {
			$table = $tpl;
		}
		return $template->parse($table . '.php');
	}

	// 通用列表
	function home( $in )
	{
		$this->check_privilege();

		$in += array(
			'pagesize' => 15
		);

		$table = $this->table;
		$equal = array();
		$like = array();
		$q = array();
		$data = biz($table)->search( $in, $equal, $like, $q );

		$data[ 'table' ] = $table;

		return $this->out( $data, 'sample' );
	}

	// 通用增加
	function add( $in )
	{
		$this->check_privilege();

		$table = $this->table;
		$data = load('db')->select( 'desc ' . $table );
		$info = array();
		foreach( $data as $v )
		{
			if( $v[ 'Field' ] != 'id' )
			{
				$info[] = $v[ 'Field' ];
			}
		}
		$info = array( 'info' => $info, 'table' => $table );
		return $this->out( $info, 'sample_add' );
	}

	// 通用增加处理
	function add_save( $in )
	{
		$this->check_privilege();

		$result = biz( $this->table )->add( $in );

		return $result > 1 ? $this->ajax_out( 1, '增加成功' ) : $this->ajax_out( -1, '增加失败,请检查！' );
	}

	// 通用修改
	function edit( $in )
	{
		$this->check_privilege();

		$table = $this->table;
		$info = biz($table)->get_from_id( $in[ 'id' ] );
		$info = array( 'info' => $info, 'table' => $table );
		return $this->out( $info, 'sample_edit' );
	}

	// 通用修改处理
	function edit_save( $in )
	{
		$this->check_privilege();

		$result = biz( $this->table )->set( $in );

		return $result == 1 ? $this->ajax_out( 1, '修改成功' ) : $this->ajax_out( -1, '修改失败,请检查！' );
	}

	// 通用删除处理
	function del( $in )
	{
		$this->check_privilege();

		$result = biz( $this->table )->del( $in[ 'id' ] );

		return $result == 1 ? $this->ajax_out( 1, '删除成功' ) : $this->ajax_out( -1, '删除失败,请检查！' );
	}

	// 通用克隆处理
	function ghost( $in )
	{
		$this->check_privilege();

		$obj = biz( $this->table );
		$info = $obj->get_from_id( $in[ $obj->id ] );
		unset( $info[$obj->id ] );

		if( $obj->time_field )
		{
			unset( $info[$obj->time_field ] );
		}

		$obj->add( $info );
		return $this->ajax_out( 1, '克隆成功' );
	}

}
?>