<?php
class action extends backend
{
	function home( $in )
	{
		$this->check_privilege( '105' );
		$admin = biz('admin');
		$result['admin'] = $admin->login_info();
		if( !empty($result['admin']) )
		{
			$result['menu'] = $admin->menu();
			return $this->out($result);
		}

	}
}
?>